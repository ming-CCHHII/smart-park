const mongoose = require('../connect');
const Schema = mongoose.Schema;

const levelSchema = new Schema({
    level:String,
    state:{
        type:Boolean,
        default:false
    },
    describe:String,
    date:{
        type:Date,
        default:Date.now()
    }
})

const carSchema = new Schema({
    name:String,
    phone:String,
    position:String,
    purpose:String,
    company:String,
    flag:{
        type:Boolean,
        default:false
    },
    plate:{
        type:String,
        default:''
    },
    date:String,
    face:String,
    pid:{
        type:mongoose.Types.ObjectId,
        ref:'level',
        default:'672ae7d67eb09556158885dd'
    },
    state:{
        type:Number,
        default:0
    },
    address:{
        type:Array,
        default:''
    },
    fulfill:{
        type:String,
        default:''
    },
    block:{
        type:String,
        default:''
    }
})

const staffSchema = new Schema({
    name:String,
    phone:String,
    position:String,
    company:String,
    face:String,
    address:Array,
    type:{
        type:Boolean,
        default:false
    },
    date:{
        type:Date,
        default:Date.now()
    }
})

const driverSchema = new Schema({
    plate:String,
    carType:String,
    purpose:String,
    name:String,
    phone:String,
    img:String,
    date:{
        type:Date,
        default:Date.now()
    }
})

const personSchema = new Schema({
    name:String,
    phone:String,
    position:String,
    company:String,
    type:String,
    access:String,
    address:String,
    img:String,
    pass:String,
    inOut:Boolean,
    date:String
})

const recordSchema = new Schema({
    plate:String,
    message:String,
    purpose:String,
    name:String,
    phone:String,
    lockage:String,
    inOut:Boolean,
    img:String,
    date:String
})

const carModel = mongoose.model('people',carSchema,'people');
const levelModel = mongoose.model('level',levelSchema,'level');
const staffModel = mongoose.model('staff',staffSchema,'staff');
const driverModel = mongoose.model('driver',driverSchema,'driver');
const personModel = mongoose.model('person',personSchema,'person');
const recordModel = mongoose.model('record',recordSchema,'record');

module.exports = {
    carModel,
    levelModel,
    staffModel,
    driverModel,
    personModel,
    recordModel
};