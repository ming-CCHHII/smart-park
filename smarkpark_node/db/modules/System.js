const { text } = require('express');
const mongoose = require('../connect');
const Schema = mongoose.Schema;


// 用户
const System_user = new Schema({
    name: String,
    zhanghao: String,
    phone: Number,
    sex: Boolean,
    position: String,
    department:Array,
    role:String,
    picture:String,
    createdata: Date,
})
// 部门
const System_department = new Schema({
    name: String,
    weight:String,
    updatadate: Date,
    children:Array,
    key:String
})
// 角色
const System_role= new Schema({
    name: String,
    updatadate: Date,
    fan:Array,
    model:Array,
    flag:Boolean,
    key:Array
})
// 空间
const System_space=new Schema({
    name: String,
    address: String,
    weight:String,
    updatadate: Date,
    children:Array,
    key:String

})
// 设备
const System_device=new Schema({
    bian:String,
    name: String,
    type:String,
    brand:String,
    address:String,
    position:String,
    updatadate: Date,
    flag:Number,
})
System_device.index({
    name:'text'
})
// 日志
const System_log=new Schema({
    xuid:String,
    type:String,
    xiang:Array,
    name: String,
    content:String,
    zhanghao:String,
    updatadate:Date,
})
const System_device_type=new Schema({
    name: String,
    describe: String,
    flag:Boolean,
    type_id:String,
    updatadate: Date,
})
const UserModel = mongoose.model('System_user',System_user,'System_user');
const Departmentodel = mongoose.model('System_department',System_department,'System_department');
const Rolemodel = mongoose.model('System_role',System_role,'System_role');
const SpaceModel = mongoose.model('System_space',System_space,'System_space');
const DeviceModel = mongoose.model('System_device',System_device,'System_device');
const LogModel = mongoose.model('System_log',System_log,'System_log');
const Device_typeModel= mongoose.model('System_device_type',System_device_type,'System_device_type');


module.exports = {
    UserModel,
    Rolemodel,
    Departmentodel,
    SpaceModel,
    DeviceModel,
    LogModel,
    Device_typeModel
};




// module.exports = {
//     personnelTypeModel,
//     personnelModel,
//     patrolSizeModel,
//     patrolTaskModel
// };
