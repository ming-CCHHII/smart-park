const mongoose = require('../connect');
const Schema = mongoose.Schema;

function setCode(length) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        result += characters[randomIndex];
    }
    return result;
}

// 部门
const personnelSchemaType = new Schema({
    type: String
})
const personnelTypeModel = mongoose.model('personnelType', personnelSchemaType, 'personnelType');

// 职位
const position= new Schema({
    position: String
})
const positionModel = mongoose.model('position', position, 'position');

// 人员
const personnelSchema = new Schema({
    name: String,
    age: Number,
    gender: Number,
    phone: String,
    address: String,
    typeid: {
        ref: 'personnelType',
        type: mongoose.Types.ObjectId
    },
    positionid: {
        ref: 'position',
        type: mongoose.Types.ObjectId
    }
})
const personnelModel = mongoose.model('personnel', personnelSchema, 'personnel');

// 巡查点
const patrolSize = new Schema({
    code: {
        type: String,
        default: '#' + setCode(6)
    }, // 编号
    name: String, // 名称
    position: String, // 位置
    object: String, // 对象
    inspect: String, // 检查
    createtime: {
        type: Date,
        default: Date.now
    }, // 创建时间
    state: {
        type: Number,
        default: 0
    }, // 状态
    img: String // 二维码
})
const patrolSizeModel = mongoose.model('patrolSize', patrolSize, 'patrolSize');

// 巡查任务
const patrolTask = new Schema({
    name: String, // 名称
    cycle: {
        type: String,
        default: "一周一次"
    }, // 周期
    Taskpersonnel: String, // 执行人员
    patrols: {
        type: Array,
        default: []
    }, // 巡查点
    startime: String, // 执行时间
    source: String, // 来源
    state: {
        type: Number,
        default: 0
    } // 状态
})
const patrolTaskModel = mongoose.model('patrolTask', patrolTask, 'patrolTask');

// 巡检计划
const patrolPlan = new Schema({
    name: String, // 名称
    cycle: String, // 周期
    Taskpersonnel: String, // 执行人员
    startimeout: String, // 计划有效期
    time: String, // 执行时间
    state: {
        type: Number,
        default: 0
    }, // 状态
    patrols: {
        type: Array,
        default: []
    }, // 巡查点
})
const patrolPlanModel = mongoose.model('patrolPlan', patrolPlan, 'patrolPlan');

// 隐患
const patrolHidden = new Schema({
    title: String, // 隐患描述
    object: String, // 对象
    level: {
        type: Number,
        default: 0
    },// 0:一般 1:严重 2:提醒
    position: String, // 位置
    reporter: String, // 上报人
    reporterphone: String, // 上报人联系电话
    troubleimg: String, // 隐患图片
    createtime: String, // 上报时间

    rectify: String, // 整改人
    rectifyphone: String, // 整改人电话
    rectifystate: {
        type: Number,
        default: 0
    }, // 状态0:未整改 1:已整改
    rectifytitle: String, // 整改描述
    rectifyimg: String, // 整改图片
    rectifytime: String, // 整改时间
})
const patrolHiddenModel = mongoose.model('patrolHidden', patrolHidden, 'patrolHidden');

// 模板
const template = new Schema({
    code: {
        type: String,
        default: '#' + setCode(4)
    }, // 编号
    name: String, // 名称
    content: {
        type: Array,
        default: []
    }, // 检查项目
    createtime: {
        type: Date,
        default: Date.now
    }, // 创建时间
    state: {
        type: Number,
        default: 0
    }, // 是否有效
})
const templateModel = mongoose.model('template', template, 'template');

module.exports = {
    personnelTypeModel,
    personnelModel,
    patrolSizeModel,
    patrolTaskModel,
    patrolPlanModel,
    patrolHiddenModel,
    positionModel,
    templateModel
};
