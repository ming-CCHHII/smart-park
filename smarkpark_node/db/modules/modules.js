const mongoose = require('../connect');
const Schema = mongoose.Schema;

//用户集合
const users= new Schema({
    uname:String,
    pwd:String,
    roleid:{
        ref:'role',
        type:mongoose.Types.ObjectId
    }
})

const userModel=mongoose.model('user',users,'user');

//角色集合
const roles= new Schema({
    rname:String,
    mids:{
        ref:'menu',
        type:[mongoose.Types.ObjectId]
    }
})

const roleModel=mongoose.model('role',roles,'role');

//菜单集合
const menus= new Schema({
    mname:String,
    path:String,
    icon:String,
    pid:mongoose.Types.ObjectId
})

const menuModel=mongoose.model('menu',menus,'menu');


module.exports = {
    userModel,
    roleModel,
    menuModel
};