const mongoose = require('../connect');
const Schema = mongoose.Schema;

// 摄像头管理
const  securityModel = new Schema({
    // 设备编号
    securityId: {
        type: String,
        required: true
    },
    // 设备名称
    name: {
        type: String,
        required: true
    }
}, {
    timestamps: {
        createdAt: 'createTime',
        updatedAt: 'updateTime'
    }
})
const Security = mongoose.model('securityModel', securityModel, 'securityModel');


// 预警管理
const securitySchema = new Schema({
    // 设备关联
    securityId: {
        ref: 'securityModel',
        type: mongoose.Types.ObjectId,
    },
    // 预警级别关联
    securityLevelId: {
        ref: 'securityLevelSchema',
        type: mongoose.Types.ObjectId,
    },
    // 区域管理关联
    areaId: {
        ref: 'areaSchema',
        type: mongoose.Types.ObjectId,
    },
    // 处理状态
    state: {
        type: Boolean,
        default: true
    },
    // 处理结果
    result: String,
}, {
    timestamps: {
        createdAt: 'createTime',
        updatedAt: 'updateTime'
    }
})
const securitys = mongoose.model('securitySchema', securitySchema, 'securitySchema');


// 预警级别管理
const securityLevelSchema = new Schema({
    name: String,
    cate: String,
    desc: String,
    level: String,
    state: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'createTime',
        updatedAt: 'updateTime'
    }
})
const securityLevel = mongoose.model('securityLevelSchema', securityLevelSchema, 'securityLevelSchema');


// 区域管理
const areaSchema = new Schema({
    name: String
}, {
    timestamps: {
        createdAt: 'createTime',
        updatedAt: 'updateTime'
    }
})
const area = mongoose.model('areaSchema', areaSchema, 'areaSchema');

module.exports = {
    Security,
    securitys,
    securityLevel,
    area
};