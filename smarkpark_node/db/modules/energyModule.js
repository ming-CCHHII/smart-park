const mongoose = require('../connect');
const Schema = mongoose.Schema;


// 能耗类型
const energyType = new Schema({
    name: String // 类型名称
})

const energyTypeModel = mongoose.model('energyType', energyType, 'energyType');

//预警周期
const energyCycle = new Schema({
    name: String // 周期名称
})

const energyCycleModel = mongoose.model('energyCycle', energyCycle, 'energyCycle');

//能耗设备
const energyDevice = new Schema({
    name: String,//设备名称
    image: {
        type: String,
        default: 'https://cdn7.axureshop.com/demo2024/2291721/images/%E8%83%BD%E8%80%97%E5%88%86%E6%9E%90/u7160.png'
    }
})

const energyDeviceModel = mongoose.model('energyDevice', energyDevice, 'energyDevice');

//水表设备
const waterDevice = new Schema({
    name: String,//设备名称
    image: {
        type: String,
        default: '	https://cdn7.axureshop.com/demo2024/2291721/images/%E8%83%BD%E8%80%97%E5%88%86%E6%9E%90/u7216.png'
    }
})
const waterDeviceModel = mongoose.model('waterDevice', waterDevice, 'waterDevice');

//能耗预警
const energyWarn = new Schema({
    name: String, //预警名称
    describe: String, //预警描述
    time: Date, //预警时间
    state: {
        type: Number,
        default: 0
    }, //状态 // 0:未处理 1:已处理
    result: String, //处理结果
    resultTime: Date,//处理时间
    threshold: Number, //阈值
    //设备id
    deviceid: {
        ref: 'energyDevice',
        type: [mongoose.Types.ObjectId]
    },
    //能耗类型
    etid: {
        ref: 'energyType',
        type: mongoose.Types.ObjectId
    },
    //预警周期
    ecid: {
        ref: 'energyCycle',
        type: mongoose.Types.ObjectId
    }
})

const energyWarnModel = mongoose.model('energyWarn', energyWarn, 'energyWarn');

//用途分组
const purposeGroup = new Schema({
    name: String, //用途名称,
    weight: Number, //权重
    //电表设备id
    deviceid: {
        ref: 'energyDevice',
        type: [mongoose.Types.ObjectId]
    },
    //水表设备id
    waterid:{
        ref: 'waterDevice',
        type: [mongoose.Types.ObjectId]
    },
    //用途类型
    typeid:{
        ref: 'purposeType',
        type: mongoose.Types.ObjectId
    },
    showFlag:{
        type:Boolean,
        default:false
    },
    realTime:Number,//实时读数
    recordTime:Date,//记录时间
    pid:mongoose.Types.ObjectId
    
})
const purposeGroupModel = mongoose.model('purposeGroup', purposeGroup, 'purposeGroup');

//用途类型
const purposeType = new Schema({
    name: String, //用途名称
})

const purposeTypeModel = mongoose.model('purposeType', purposeType, 'purposeType');


//能源价格
const energyPrice = new Schema({
    typeid:{
        ref: 'energyType',
        type: mongoose.Types.ObjectId
    },
    stairs:String,
    stairsPrice:Number,
    stairsThreshold:Number,
    stairsCondition:Number,//0 小于阈值 1 大于阈值
    up_time:Date,
    pid:mongoose.Types.ObjectId
})

const energyPriceModel = mongoose.model('energyPrice', energyPrice, 'energyPrice');


module.exports = {
    energyTypeModel,
    energyCycleModel,
    energyDeviceModel,
    waterDeviceModel,
    energyWarnModel,
    purposeGroupModel,
    purposeTypeModel,
    energyPriceModel
}