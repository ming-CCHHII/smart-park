
var express = require('express');
var router = express.Router();
const {
    positionModel,
    personnelModel,
    personnelTypeModel,
    patrolSizeModel,
    patrolTaskModel,
    patrolPlanModel,
    patrolHiddenModel,
    templateModel } = require('../db/modules/patrolModule');

// 添加人员
router.get('/addpersonnelModel', async (req, res, next) => {
    await personnelModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
});

// 添加部门
router.get('/addpersonnelTypeModel', async (req, res, next) => {
    await personnelTypeModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

// 添加职位
router.post("/addpositionModel", async (req, res, next) => {
    await positionModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

// 获取职位
router.get('/getpositionModel', async (req, res, next) => {
    let position = await positionModel.find()
    let data = [];
    position.map(item => {
        data.push({
            value: item._id,
            label: item.position
        })
    })
    res.send({
        code: 200,
        msg: '获取成功',
        data
    })
})

// 获取部门
router.get('/getpersonnelTypeModel', async (req, res, next) => {
    let personnelType = await personnelTypeModel.find()
    let data = [];
    personnelType.map(item => {
        data.push({
            value: item._id,
            label: item.type
        })
    })
    res.send({
        code: 200,
        msg: '获取成功',
        data
    })
})

// 查询人员类型
router.get('/getpersonnelTypeModel', async (req, res, next) => {
    let users = await personnelModel.find()
    let classes = await personnelTypeModel.find()

    const mergeData = (users, classes) => {
        // 创建一个以 classId 为键的用户映射
        const classUsersMap = {};

        // 遍历用户列表
        users.forEach(user => {
            const { typeid, name, _id } = user;

            // 如果 classUsersMap 中还没有该 typeid，则初始化一个空数组
            if (!classUsersMap[typeid]) {
                classUsersMap[typeid] = [];
            }

            // 将用户信息添加到对应 typeid 的数组中
            classUsersMap[typeid].push({ label: name, value: name + '/' + _id });
        });


        // 遍历类表，将用户作为 children 添加
        const mergedClasses = classes.map(classItem => {
            return {
                "label": classItem.type,
                "value": classItem.type,
                children: classUsersMap[classItem._id] || []
            };
        });
        return mergedClasses;
    };

    // 使用合并函数
    const re = mergeData(users, classes);

    res.send({
        code: 200,
        msg: '查询成功',
        data: re
    })
})

// 查询人员
router.post('/getpersonneModel', async (req, res, next) => {
    let { name, positionid, typeid, address } = req.body;

    let query = [];
    let data;
    if (name) {
        query.push({ name: new RegExp(name) })
    }
    if (positionid) {
        query.push({ positionid: positionid })
    }
    if (typeid) {
        query.push({ typeid: typeid })
    }
    if (address) {
        query.push({ address: new RegExp(address) })
    }
    if (query.length == 0) {
        data = await personnelModel.find().populate('typeid').populate("positionid")
    } else {
        data = await personnelModel.find({ $and: query }).populate('typeid').populate("positionid")
    }
    res.send({
        code: 200,
        msg: '查询成功',
        data
    })
})

// 查询巡查点
router.get('/getpatrolSizeModel', async (req, res, next) => {
    let { name, object, inspect, state } = req.query;
    let stateNum = Number(state);
    let query = [];
    let data;
    if (name) {
        query.push({ name: new RegExp(name) })
    }
    if (object) {
        query.push({ object: new RegExp(object) })
    }
    if (inspect) {
        query.push({ inspect: new RegExp(inspect) })
    }
    if (stateNum) {
        query.push({ state: stateNum })
    }
    if (query.length == 0) {
        data = await patrolSizeModel.find()
    } else {
        data = await patrolSizeModel.find({ $and: query })
    }

    res.send({
        code: 200,
        msg: '查询成功',
        data
    })
})

// 添加巡查点
router.post('/addpatrolTaskModel', async (req, res, next) => {
    await patrolTaskModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

// 查询巡查任务
router.post('/getpatrolTaskModel', async (req, res, next) => {
    try {
        const { name, person, plan, state } = req.body;
        // 构造查询条件
        let query = [];
        let data;
        if (name) {
            query.push({ name: new RegExp(name) })
        }
        if (person) {
            query.push({ Taskpersonnel: new RegExp(person) })
        }
        if (plan) {
            query.push({ source: new RegExp(plan) })
        }
        if (state == 0 || state == 1) {
            query.push({ state: state })
        }
        // 根据条件查询数据库
        if (query.length == 0) {
            data = await patrolTaskModel.find()
        } else {
            data = await patrolTaskModel.find({ $and: query })
        }
        res.send({
            code: 200,
            msg: '查询成功',
            data
        });
    } catch (error) {
        res.send({
            code: 500,
            msg: '查询失败',
            data: [],
            error: error.message
        });
    }
})

// 删除巡查任务
router.delete("/delpatrols", async (req, res, next) => {
    let { _id } = req.query
    await patrolTaskModel.deleteOne({ _id: _id })
    res.send({
        code: 200,
        msg: '删除成功'
    })
})

// 修改巡查点
router.put("/point", async (req, res, next) => {
    await patrolSizeModel.updateOne({ _id: req.body.id }, req.body.state)
    res.send({
        code: 200,
        msg: '修改成功'
    })
})

// 添加巡查点
router.post("/pointadd", async (req, res, next) => {
    await patrolSizeModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

// 获取巡检计划
router.post('/getpatrolPlanModel', async (req, res, next) => {
    const { name, cycle, Taskpersonnel, state } = req.body
    let query = [];
    let data;
    if (name) {
        query.push({ name: new RegExp(name) })
    }
    if (cycle) {
        query.push({ cycle: new RegExp(cycle) })
    }
    if (Taskpersonnel) {
        query.push({ Taskpersonnel: new RegExp(Taskpersonnel) })
    }
    if (state === 0 || state == 1) {
        query.push({ state: state })
    }
    if (query.length == 0) {
        data = await patrolPlanModel.find()
    } else {
        data = await patrolPlanModel.find({ $and: query })
    }

    res.send({
        code: 200,
        msg: '查询成功',
        data
    })
})

// 添加巡检计划
router.post("/addpatrolPlanModel", async (req, res, next) => {
    const { id, arr } = req.body
    if (id != '') {
        await patrolPlanModel.updateOne({ _id: id }, arr)
    } else {
        await patrolPlanModel.create(req.body.arr)
    }
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

// 删除巡检计划
router.delete("/delpatrolPlanModel", async (req, res, next) => {
    let { _id } = req.query
    await patrolPlanModel.deleteOne({ _id: _id })
    res.send({
        code: 200,
        msg: '删除成功'
    })
})

// 添加隐患
router.post("/addpatrolHiddenModel", async (req, res, next) => {
    await patrolHiddenModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

// 获取隐患
router.post('/getpatrolHiddenModel', async (req, res, next) => {
    const { title, reporter, rectify, level, rectifystate } = req.body
    let query = [];
    let data;
    if (title) {
        query.push({ title: new RegExp(title) })
    }
    if (reporter) {
        query.push({ reporter: new RegExp(reporter) })
    }
    if (rectify) {
        query.push({ rectify: new RegExp(rectify) })
    }
    if (level === 0 || level == 1 || level == 2) {
        query.push({ level: level })
    }
    if (rectifystate === 0 || rectifystate == 1) {
        query.push({ rectifystate: rectifystate })
    }
    if (query.length == 0) {
        data = await patrolHiddenModel.find()
    } else {
        data = await patrolHiddenModel.find({ $and: query })
    }
    res.send({
        code: 200,
        msg: '查询成功',
        data
    })
})

// 获取模板
router.post('/gettemplateModel', async (req, res, next) => {
    const {name, state} = req.body
    let query = [];
    let data;
    if (name) {
        query.push({ name: new RegExp(name) })
    }
    if (state === 0 || state == 1) {
        query.push({ state: state })
    }
    if (query.length == 0) {
        data = await templateModel.find()
    }else{
        data = await templateModel.find({$and:query})
    }
    res.send({
        code: 200,
        msg: '查询成功',
        data
    })
})
// 修改模板
router.put("/settemplateModel", async (req, res, next) => {
    const { id, state } = req.body
    console.log(id, state)
    await templateModel.updateOne({ _id: id }, {
        name: state.name,
        content: state.content,
        state: state.state
    })
    res.send({
        code: 200,
        msg: '修改成功'
    })
})

// 删除模板
router.delete("/deltemplateModel", async (req, res, next) => {
    let { _id } = req.query
    await templateModel.deleteOne({ _id: _id })
    res.send({
        code: 200,
        msg: '删除成功'
    })
})

// 添加模板
router.post("/addtemplateModel", async (req, res, next) => {
    await templateModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

module.exports = router;
