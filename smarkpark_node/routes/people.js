var express = require('express');
var router = express.Router();
const { carModel, levelModel, staffModel, driverModel, personModel, recordModel } = require('../db/modules/people')
let multiparty = require('multiparty')

router.post('/upload', async (req, res) => {
  let form = new multiparty.Form()
  form.uploadDir = 'upload'
  form.parse(req, async (err, a, files) => {
    res.send({
      code: 200,
      file: files.avatar[0].path
    })
  })
})

router.get('/data', async (req, res) => {
  let { stateTime, endTime, message, pid, examine } = req.query
  let queryArr = [{}]
  let regMsg = new RegExp(message)
  if (message) {
    queryArr.push({ $or: [{ name: regMsg }, { plate: regMsg }, { phone: regMsg }, { company: regMsg }] })
  }
  if (pid) queryArr.push({ pid: pid })
  if (examine) queryArr.push({ state: examine })
  if (stateTime && endTime) queryArr.push({ date: { $gte: stateTime, $lte: endTime } })
  let data = await carModel.find({ $and: queryArr }).populate('pid')
  res.send({
    code: 200,
    data
  })
})

router.get('/Content', async (req, res) => {
  let { id } = req.query
  let data = await carModel.findOne({ _id: id }).populate('pid')
  res.send({
    code: 200,
    data,
  })
})

router.post('/add', (req, res) => {
  carModel.create(req.body)
  res.send({
    code: 200
  })
})

router.post('/edit', async (req, res) => {
  let body = req.body
  let { _id } = req.query
  await levelModel.updateOne({ _id }, body)
  res.send({
    code: 200
  })
})

router.post('/examine', async (req, res) => {
  let body = req.body
  let { _id } = req.query
  await carModel.updateOne({ _id }, body)
  res.send({
    code: 200
  })
})

router.get('/level', async (req, res) => {
  let { level, state } = req.query
  let queryState = Boolean(state)
  let queryArr = [{}]
  let levelReg = new RegExp(level)
  if (levelReg) queryArr.push({ level: levelReg })
  if (state) queryArr.push({ state: queryState })
  let data = await levelModel.find({ $and: queryArr })
  data = data.filter(item => item.level != '-')
  res.send({
    code: 200,
    data
  })
})

router.post('/add_level', (req, res) => {
  levelModel.create(req.body)
  res.send({
    code: 200
  })
})

router.post('/del_level', async (req, res) => {
  let id = req.query.id
  await staffModel.deleteOne({ _id: id })
  res.send({
    code: 200
  })
})

router.post('/del_staff', async (req, res) => {
  let id = req.query.id
  await staffModel.deleteOne({ _id: id })
  res.send({
    code: 200
  })
})

router.get('/staff', async (req, res) => {
  let { message, type } = req.query
  let queryArr = [{}]
  let regMsg = new RegExp(message)
  if (message) queryArr.push({ $or: [{ name: regMsg }, { position: regMsg }, { phone: regMsg }, { company: regMsg }] })
  if (type) queryArr.push({ type: type })
  let data = await staffModel.find({ $and: queryArr })
  res.send({
    code: 200,
    data
  })
})

router.post('/add_staff', (req, res) => {
  staffModel.create(req.body)
  res.send({
    code: 200
  })
})

router.post('/edit_staff', async (req, res) => {
  let body = req.body
  let { id } = req.query
  await staffModel.updateOne({ _id: id }, body)
  res.send({
    code: 200
  })
})

router.get('/driver', async (req, res) => {
  let { car, driver } = req.query
  let queryArr = [{}]
  let regCar = new RegExp(car)
  let regDriver = new RegExp(driver)
  if (regCar) queryArr.push({ $or: [{ plate: regCar }, { carType: regCar }, { purpose: regCar }] })
  if (regDriver) queryArr.push({ $or: [{ name: regDriver }, { phone: regDriver }] })
  let data = await driverModel.find({ $and: queryArr })
  res.send({
    code: 200,
    data
  })
})

router.post('/add_driver', (req, res) => {
  driverModel.create(req.body)
  res.send({
    code: 200
  })
})

router.post('/edit_driver', async (req, res) => {
  let body = req.body
  let { id } = req.query
  await driverModel.updateOne({ _id: id }, body)
  res.send({
    code: 200
  })
})

router.post('/del_driver', async (req, res) => {
  let id = req.query.id
  await driverModel.deleteOne({ _id: id })
  res.send({
    code: 200
  })
})

router.get('/blacklist', async (req, res) => {
  let { message } = req.query
  let reg = new RegExp(message)
  let queryArr = [{}]
  if (reg) queryArr.push({ $or: [{ name: reg }, { phone: reg }, { position: reg }, { company: reg }] })
  let data = await carModel.find({ $and: queryArr })
  data = data.filter(item => item.block)
  console.log(data);
  res.send({
    code: 200,
    data
  })
})

router.post('/edit_blacklist', async (req, res) => {
  let body = req.body
  let { id } = req.query
  await carModel.updateOne({ _id: id }, body)
  res.send({
    code: 200
  })
})

router.post('/del_blacklist', async (req, res) => {
  let { id } = req.query
  await carModel.updateOne({ _id: id }, { block: '', state: 0 })
  res.send({
    code: 200
  })
})

router.get('/person', async (req, res) => {
  let { person_message, person_type, access, pass, inOut, passStateTime, passEndTime } = req.query
  let reg = new RegExp(person_message)
  let queryArr = [{}]
  let inout = Boolean(inOut)
  if (reg) queryArr.push({ $or: [{ name: reg }, { phone: reg }, { position: reg }, { company: reg }] })
  if (person_type) queryArr.push({ type: person_type })
  if (access) queryArr.push({ access: access })
  if (pass) queryArr.push({ pass: pass })
  if (inOut) queryArr.push({ inOut: inout })
  if (passStateTime && passEndTime) queryArr.push({ date: { $gte: passStateTime, $lte: passEndTime } })
  let data = await personModel.find({ $and: queryArr })
  res.send({
    code: 200,
    data
  })
})

router.get('/record', async (req, res) => {
  let { record_message, record_driver, lockage, record_inOut, recordStateTime, recordEndTime } = req.query
  let regMsg = new RegExp(record_message)
  let regDir = new RegExp(record_driver)
  let inout = Boolean(record_inOut)
  let queryArr = [{}]
  if (regMsg) queryArr.push({ $or: [{ plate: regMsg }, { message: regMsg }, { purpose: regMsg }] })
  if (regDir) queryArr.push({ $or: [{ name: regDir }, { phone: regDir }] })
  if (lockage) queryArr.push({ lockage: lockage })
  if (record_inOut) queryArr.push({ inOut: inout })
  if (recordStateTime && recordEndTime) queryArr.push({ date: { $gte: recordStateTime, $lte: recordEndTime } })
  let data = await recordModel.find({$and: queryArr})
  res.send({
    code: 200,
    data
  })
})

module.exports = router;