var express = require('express');
var router = express.Router();
const { energyTypeModel, energyCycleModel, energyWarnModel, energyDeviceModel, waterDeviceModel, purposeGroupModel, purposeTypeModel, energyPriceModel } = require('../db/modules/energyModule');

//获取能耗类型
router.get('/getenergyType', async (req, res) => {
    let data = await energyTypeModel.find();
    res.send({
        code: 200,
        data
    })
})

//获取能耗周期
router.get('/getenergyCycle', async (req, res) => {
    let data = await energyCycleModel.find();
    res.send({
        code: 200,
        data
    })
})

//获取电表设备
router.get('/getenergyDevice', async (req, res) => {
    let data = await energyDeviceModel.find();
    res.send({
        code: 200,
        data
    })
})

//获取水表设备
router.get('/getwaterDevice', async (req, res) => {
    let data = await waterDeviceModel.find();
    res.send({
        code: 200,
        data
    })
})

//添加能耗预警
router.post('/addenergyWarn', (req, res) => {
    energyWarnModel.create(req.body)
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

//获取能耗预警
router.get('/getenergyWarn', async (req, res) => {
    let { sname, setid, state, time } = req.query
    let statenum = Number(state)
    let queryArr = []
    let data;
    if (sname) {
        queryArr.push({ name: new RegExp(sname) })
    }
    if (setid) {
        queryArr.push({ etid: setid })
    }
    if (statenum) {
        queryArr.push({ state: statenum })
    }
    if (time) {
        const date = new Date(time);
        const extractedDate = date.toISOString().slice(0, 10);

        // 构建时间范围查询条件
        const startOfDay = new Date(extractedDate);
        const endOfDay = new Date(extractedDate);
        endOfDay.setHours(23, 59, 59, 999);

        queryArr.push({ time: { $gte: startOfDay, $lte: endOfDay } });
    }

    if (queryArr.length == 0) {
        data = await energyWarnModel.find().populate(['etid', 'ecid', 'deviceid']);
    } else {
        data = await energyWarnModel.find({ $and: queryArr }).populate(['etid', 'ecid', 'deviceid']);
    }
    res.send({
        code: 200,
        data
    })
})

//修改能耗预警
router.post('/updateenergyWarn', async (req, res) => {
    await energyWarnModel.updateOne({ _id: req.query.id }, req.body);
    res.send({
        code: 200,
        msg: '修改成功'
    })
})

//删除能耗预警
router.delete('/deleteenergyWarn', async (req, res) => {
    await energyWarnModel.deleteOne({ _id: req.query.id });
    res.send({
        code: 200,
        msg: '删除成功'
    })
})

//修改能耗预警状态
router.post('/changeWarnState', async (req, res) => {
    const { _id, result, time } = req.query
    console.log(_id, result)
    if (result.includes('已')) {
        await energyWarnModel.updateOne({ _id: _id }, { state: 1, result: result, resultTime: time });
    } else {
        await energyWarnModel.updateOne({ _id: _id }, { state: 0, result: result, resultTime: time });
    }
    res.send({
        code: 200,
        msg: '修改成功'
    })
})

//添加用途分组
router.post('/addpurposeGroup', (req, res) => {
    purposeGroupModel.create(req.body);
    res.send({
        code: 200,
        msg: '添加成功'
    })
})

//获取用途类型
router.get('/getpurposeType', async (req, res) => {
    let data = await purposeTypeModel.find()
    res.send({
        code: 200,
        data
    })
})

//获取用途分组
router.get('/getpurposeGroup', async (req, res) => {
    let data = await purposeGroupModel.find().populate('deviceid').populate('waterid').populate('typeid').lean()
    let obj = {}
    data.forEach(item => {
        obj[item._id] = item
    })
    let result = []
    data.forEach(item => {
        if (!item.pid) {
            result.push(item)
        }
        if (obj[item.pid] && !obj[item.pid].children) {
            obj[item.pid].children = []
        }
        if (obj[item.pid]) {
            obj[item.pid].children.push(item)
        }
    })

    res.send({
        code: 200,
        result
    })
})

//添加能耗价格
router.post('/addenergyPrice', async (req, res) => {
    await energyPriceModel.create(req.body);

    res.send({
        code: 200,
        msg: '添加成功'
       
    })
})

//获取能耗价格
router.get('/getenergyPrice', async (req, res) => {
    let data = await energyPriceModel.find().populate('typeid').lean()


    let obj = {}
    data.forEach(item => {
        obj[item._id] = item
    })
    let result = []
    data.forEach(item => {
        if (!item.pid) {
            result.push(item)
        }
        if (obj[item.pid] && !obj[item.pid].children) {
            obj[item.pid].children = []
        }
        if (obj[item.pid]) {
            obj[item.pid].children.push(item)
        }
    })
    res.send({
        code: 200,
        result
    })
})

//删除阶梯
router.delete('/delstairs', async (req, res) => {
    let { _id } = req.query
    await energyPriceModel.deleteOne({ _id })
    res.send({
        code: 200,
        msg: '删除成功'
    })
})

//改变side角标状态
router.post('/changeshowFlag', async (req, res) => {
    let {_id}=req.query
    let purpose=await purposeGroupModel.findOne({_id})
    if(purpose.showFlag == true){
        await purposeGroupModel.updateOne({_id},{showFlag:false})
    }else{               
        await purposeGroupModel.updateOne({_id},{showFlag:true})
    }   
    res.send({
        code: 200,
        msg: '修改成功'
    })

})

//获取用途分组info
router.post('/getInfo', async (req, res) => {
    let {_id} = req.query
    let data = await purposeGroupModel.findOne({ _id })
    res.send({
        code: 200,
        data

    })
})



module.exports = router;
