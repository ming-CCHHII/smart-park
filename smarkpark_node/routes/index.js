var express = require('express');
var router = express.Router();
// const fileUpload = require('express-fileupload');
const { userModel,roleModel,menuModel } = require('../db/modules/modules')

// router.use(fileUpload());
/* GET home page. */

// Login
router.post("/login", async (req, res) => {
    let {uname, pwd} = req.body;
    console.log(uname, pwd);
    const users = await userModel.find({ uname: uname, pwd: pwd }).populate('roleid');
    if (!users) {}
    res.send({  
        status: 0,
        msg: "登录成功",
        userModel: users,
    })
})
//获取菜单
router.get('/getmenuone',async(req,res)=>{
    let menu_ls1=await menuModel.find()

    res.send({
        code:200,
        data:menu_ls1
    })
})

router.get('/getmenu',async(req,res)=>{
    let menudata=await menuModel.find().lean()
    let obj={}
    menudata.forEach(item=>{
        obj[item._id]=item
    })

    let result=[]
    menudata.forEach(item=>{
        if(!item.pid){
            result.push(item)
        }
        if(obj[item.pid] && !obj[item.pid].children){
            obj[item.pid].children=[]
        }
        if(obj[item.pid]){
            obj[item.pid].children.push(item)
        }
    })
    res.send({
        code:200,
        data:result
    })
})


module.exports = router;
