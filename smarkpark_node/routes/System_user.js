var express = require('express');
var router = express.Router();
const { UserModel,Departmentodel,Rolemodel, SpaceModel,DeviceModel, LogModel, Device_typeModel } = require('../db/modules/System')
let multiparty = require('multiparty')
//      用户管理
        //获取菜单
        router.get('/list',async(req,res)=>{
       
            let data=await UserModel.find()
            res.send({
                code:200,
                data:data
            })
        })
        // 上传图片
        router.post('/upload', async (req, res) => {
        let form = new multiparty.Form()
        form.uploadDir = 'upload'
        form.parse(req, async (err, a, files) => {
            res.send({
            code: 200,
            file: files.avatar[0].path
            })
        })
        })
        // 添加用户
        router.post("/add",async(req,res)=>{
            const {text}=req.body
            if(!text){
                return res.status(400).send('Text to encrypt is required');
            }
            let encrypted = key.encrypt(text, 'base64');
            res.send({
                encrypted
            })
        console.log(1)
            let {name,zhanghao,phone,sex,position,department,role,picture,createdata}=req.body
            let data=await UserModel.create({name,zhanghao,phone,sex,position,department,role,picture,createdata})
            res.send({
                code:200,
                data:data
            })
        })
        // 获取单条数据
        router.post("/getupdatedata",async(req,res)=>{
        let {id}=req.body
        let data=await UserModel.findOne({_id:id})
        res.send({
            code:200,
            data:data
        })
        })
        // 修改单条数据
        router.post("/updateonedata",async(req,res)=>{

            let {id,name,zhanghao,phone,sex,position,department,role,picture,createdata}=req.body
            let data=await UserModel.updateOne({_id:id},{name,zhanghao,phone,sex,position,department,role,picture,createdata})
            res.send({
                code:200,
                data:data
            })
        })
        // 删除用户
        router.post("/delete",async(req,res)=>{
            let {id}=req.body
            let data=await UserModel.deleteOne({_id:id})
            res.send({
                code:200,
                data:data
            })
        })




//      部门管理
// 添加一级部门
router.post("/department_add1",async(req,res)=>{

    const {name,depat,key,weight,updatadate,children}=req.body
    
    let data=await Departmentodel.create({name,depat,key,weight,updatadate,children})
    res.send({
        code:200,
        data:data
    })
})
// 添加二级部门
router.post("/department_add2",async(req,res)=>{

    const {name,depat,key,weight,updatadate}=req.body
   
    
    const dd={
        danid:Date.now(),
        name:name,
        key:key,
        weight:weight,
        updatadate:updatadate,
        depat:depat
    }
    let data=await Departmentodel.findOne({name:depat})
    data.children.push(dd)
    await data.save()
  
    res.send({
        code:200,
    })

})
// 删除部门
router.post("/department_delete",async(req,res)=>{
    let {depat,name}=req.body
    if(depat){
        let data=await Departmentodel.findOne({name:depat})
        const idx=data.children.findIndex(item=>item.name===depat)
        data.children.splice(idx,1)
        await data.save()
    }else{
        let data=await Departmentodel.deleteOne({name:name})
    }
  
    res.send({
        code:200
    })

})
// 获取部门
router.get("/department_list",async(req,res)=>{
    let data=await Departmentodel.find()
    res.send({
        code:200,
        data:data
    })
})
// 编辑部门
router.post("/department_update",async(req,res)=>{
    let {id,name,depat,key,weight,updatadate,danid}=req.body
    if(depat){
        let data=await Departmentodel.findOne({name:depat})
        data.children.forEach(item=>{
            if(item.danid===danid){
                item.name=name
                item.key=key
                item.weight=weight
                item.updatadate=updatadate
            }
        })
        let data1=await Departmentodel.updateOne({name:depat},data)
    }else{
        let data2=await Departmentodel.findOne({_id:id})
        data2.name=name
        data2.key=key
        data2.weight=weight
        data2.updatadate=updatadate
        data2.children.forEach(item=>{
            item.depat=name
        })
        let data=await Departmentodel.updateOne({_id:id},data2)
        console.log('====================================');
        console.log(name);
        console.log('====================================');
    }
   
    res.send({
        code:200,
       
    })
})

// 空间标识
// 添加一级区域
router.post("/space_add1",async(req,res)=>{
    const {name,depat,key,weight,updatadate,children,address}=req.body

    let data=await SpaceModel.create({name,key,weight,updatadate,children,address})
    res.send({
        code:200,
        data:data
    })
})
// 添加二级部门
router.post("/space_add2",async(req,res)=>{

    const {name,key,weight,updatadate,address,depat}=req.body
    const dd={
        danid:Date.now(),
        address:address,
        name:name,
        key:key,
        weight:weight,
        updatadate:updatadate,
        depat:depat
    }
    let data=await SpaceModel.findOne({name:depat})
    data.children.push(dd)
    await data.save()
  
    res.send({
        code:200,
    })

})



// 删除数据
router.post("/space_delete",async(req,res)=>{
    let {depat,name}=req.body
    if(depat){
        let data=await SpaceModel.findOne({name:depat})
        const idx=data.children.findIndex(item=>item.name===depat)
        data.children.splice(idx,1)
        await data.save()
    }else{
        let data=await SpaceModel.deleteOne({name:name})
    }
    res.send({
        code:200
    })
})
// 编辑部门
router.post("/space_update",async(req,res)=>{
    let {id,name,depat,key,weight,updatadate,danid,address}=req.body
    if(depat){
        let data=await SpaceModel.findOne({name:depat})
        data.children.forEach(item=>{
            if(item.danid===danid){
                item.address=address
                item.name=name
                item.key=key
                item.weight=weight
                item.updatadate=updatadate
            }
        })
        let data1=await SpaceModel.updateOne({name:depat},data)
    }else{
        let data2=await SpaceModel.findOne({_id:id})
        data2.name=name
        data2.key=key
        data2.weight=weight
        data2.updatadate=updatadate
        data2.address=address
        data2.children.forEach(item=>{
            item.depat=name
        })
        let data=await SpaceModel.updateOne({_id:id},data2)
  
    }
   
    res.send({
        code:200,
       
    })
})
// 获取区域
router.get("/space_list",async(req,res)=>{
    let data=await SpaceModel.find()
    res.send({
        code:200,
        data:data
    })
})

//   角色管理
// 添加角色数据
router.post("/role_add",async(req,res)=>{
    let {name,model,key,flag,fan,updatadate}=req.body
    let data={
        name:name,
        flag:flag,
        model:model,
        fan:fan,
        key:key,
        updatadate:updatadate,
    }
    let data1=await Rolemodel.create(data)
    res.send({
        code:200,
   
    })
})
// 获取角色数据
router.get("/role_list",async(req,res)=>{
   
    let data=await Rolemodel.find()
    res.send({
        code:200,
        data
   
    })
})
// 删除角色数据
router.post("/role_delete",async(req,res)=>{
   
    let data=await Rolemodel.deleteOne({_id:req.query.id})
    res.send({
        code:200,
        data
   
    })
})
// 获取单条数据
router.post("/role_dan",async(req,res)=>{
   
    let data=await Rolemodel.findOne({_id:req.query.id})
    res.send({
        code:200,
        data
   
    })
})

// 获取单条数据
router.post("/role_update",async(req,res)=>{
   
    let {name,model,key,flag,fan,updatadate,id}=req.body
    let data={
        name:name,
        flag:flag,
        model:model,
        fan:fan,
        key:key,
        updatadate:updatadate,
    }
    let data1=await Rolemodel.updateOne({_id:id},data)
    res.send({
        code:200,
    })
})
// 设备管理

// 获取设备数据
router.get("/device_list",async(req,res)=>{ 
    let data=await DeviceModel.find()
    res.send({
        code:200,
        data
   
    })
})
// 关键搜索
router.get("/device_listg",async(req,res)=>{

    let data=[]
    console.log(req.query)
    if(req.query.name){{
        data = await DeviceModel.find({ $text: { $search: req.query.name } });
    }}else{
        data=await DeviceModel.find()
    }
    console.log(data)
    res.send({
        code:200,
        data

    })
   
})
router.get("/device_top_list",async(req,res)=>{ 
    let {type_id}=req.query
    let data=await Device_typeModel.find({type_id:type_id})
    res.send({
        code:200,
        data
   
    })
})
// 添加设备数据
router.post("/device_add",async(req,res)=>{
    let {updatadate, name,flag,type,brand,position,address,bb}=req.body
    let leng=await DeviceModel.countDocuments().exec()
    console.log("长度",leng)
    let data={
        bian:bb+"0"+leng+1,
        name:name,
        flag:flag,
        type:type,
        position:position,
        address:address,
        brand:brand,
        updatadate:updatadate,
    }
    await DeviceModel.create(data)
    res.send({
        code:200,
        
    })
})
// 删除角色数据
router.post("/device_delete",async(req,res)=>{
   
    let data=await DeviceModel.deleteOne({_id:req.query.id})
    res.send({
        code:200,
        data
   
    })
})
// 编辑设备
router.post("/device_update",async(req,res)=>{
  await DeviceModel.updateOne({_id:req.query.id},req.body)
    res.send({
        code:200,
    })
})


// 设备类型
// 获取数据
router.get("/device_type_list",async(req,res)=>{
    let data=await Device_typeModel.find()
    res.send({
        code:200,
        data

    })
})
// 删除类型设备数据
router.post("/device_type_delete",async(req,res)=>{
   
    let data=await Device_typeModel.deleteOne({_id:req.query.id})
    res.send({
        code:200,
        data
   
    })
})
// 添加设备数据
router.post("/device_type_add",async(req,res)=>{
    let {updatadate, name,flag,type_id,describe}=req.body
  
    let data={

        name:name,
        flag:flag,
        type_id:type_id,
        describe:describe,
        updatadate:updatadate,
    }
    await Device_typeModel.create(data)
    res.send({
        code:200,
        
    })
})
// 编辑设备
router.post("/device_type_update",async(req,res)=>{
    await Device_typeModel.updateOne({_id:req.query.id},req.body)
      res.send({
          code:200,
      })
  })






// 系统日志
router.get("/log_list",async(req,res)=>{ 
    let data=await LogModel.find()
    res.send({
        code:200,
        data
   
    })
})
// 
module.exports = router;
