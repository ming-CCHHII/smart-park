var express = require('express');
var router = express.Router();
const { Security, securitys, securityLevel, area } = require('../db/modules/securityModule')

// 添加预警级别
router.post('/addSecurityLevel', (req, res) => {
    securityLevel.create(req.body)
    res.send({
        code: 200
    })
})
// 添加区域管理
router.post('/addArea', (req, res) => {
    area.create(req.body)
    res.send({
        code: 200
    })
})
// 添加摄像头管理
router.post('/addSecurity', (req, res) => {
    Security.create(req.body)
    res.send({
        code: 200
    })
})

// 预警级别管理
router.get('/getSecuritys', async (req, res) => {
    let data = await securityLevel.find()
    res.send({
        code: 200,
        data
    })
})

// 添加预警管理
router.post('/addSecuritys', (req, res) => {
    securitys.create(req.body)
    res.send({
        code: 200
    })
})

// 预警管理
router.get('/getSecurity', async (req, res) => {
    let data = await securitys.find().populate('securityId').populate('securityLevelId').populate('areaId')
    res.send({
        code: 200,
        data
    })
})


module.exports = router;