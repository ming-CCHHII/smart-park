var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/upload',express.static(path.join(__dirname, 'upload')));

// npm install express-rate-limit
function custmohandler(req,res,options){
  res.status(429); // 429 Too Many Requests 是标准的速率限制状态码
  // 设置自定义的JSON响应体
  res.json({
    error: 'Too many requests, please try again later.',
    limit: options.limit, // 超出限制的最大请求数
    remaining: options.remaining, // 在当前时间窗口内剩余的请求数
    reset: Math.ceil(options.windowMs / 1000) // 重置时间窗口的秒数（向上取整）
  });
}
const rateLimit=require("express-rate-limit")
// 为了防止通过发生大量请求让服务器崩溃
const limiter=rateLimit({
  windowMs: 15 * 60 * 1000, // 15分钟之内
  max: 50, // 最多请求10次
  handler:custmohandler
})
app.use("/System_user/",limiter)



const bodyParser = require('body-parser');
const NodeRSA = require('node-rsa');
app.use(bodyParser.json())

const key = new NodeRSA({b: 2048}); // 可以使用更大的位数（如2048）来提高安全性
const publicKey = key.exportKey('public');
const privateKey = key.exportKey('private');



var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var peopleRouter = require('./routes/people');
var patrolRouter = require('./routes/patrol');
var energyRouter = require('./routes/energy');
var systemRouter = require("./routes/System_user");
var securityRouter = require('./routes/security');
var systemRouter=require("./routes/System_user");
const { strict } = require('assert');

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/people',peopleRouter);
app.use('/patrol', patrolRouter);
app.use('/energy', energyRouter);
app.use('/System_user', systemRouter);
app.use('/security', securityRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
