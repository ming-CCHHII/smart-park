import { createBrowserRouter, Navigate } from "react-router-dom";
import App from "../App";
import Login from "../components/Login/index"
import Park from "../components/Park/index"

import Security from "../components/Security/index" //安防管理
import Video from "../components/Security/video"//视频监控
import Playback from "../components/Security/playback"//视频回访
import Warning from "../components/Security/earlywarning"//预警管理
import Definitiono from "../components/Security/definitiono"//预警事件
import Threshold from "../components/Security/threshold"//消防阈值
import Analysiss from "../components/Security/analysis"//视频ai


import People from "../components/People/index" //人車管理
import Request from "../components/People/request"
import Level from "../components/People/level";
import Staff from "../components/People/staff";
import Cars from "../components/People/cars";
import Blackls from "../components/People/blackls";
import Person from "../components/People/person";
import Record from "../components/People/record";

import Energy from "../components/Energy/index" //能耗管理
import Analysis from "../components/Energy/Analysis/Index"; //能耗分析
import Warn from "../components/Energy/Warning/Index";//能耗预警
import Strategy from "../components/Energy/strategy/Index"; //预警策略
import Price from "../components/Energy/Price"; //能源价格
import Monthly from "../components/Energy/Monthly"; //月度账单

import Patrol from "../components/Patrol/index"; //巡檢管理
import Task from "../components/Patrol/Tasks";
import Plain from "../components/Patrol/plain";
import Hidden from "../components/Patrol/hidden";
import Point from "../components/Patrol/point";
import Personnel from "../components/Patrol/personnel";
import Template from "../components/Patrol/template";

import System from "../components/System/index" //系統管理
import System_zhu from "../components/System/System_zhu";
import Department from "../components/System/Department";
import Roles from "../components/System/Roles";
import Space from "../components/System/Space";
import Device from "../components/System/Device";
import Monitor from "../components/System/component/device_fen/Monitor";
import Device_Energy from "../components/System/component/device_fen/device_Energy";
import Fire from "../components/System/component/device_fen/Fire";
import Access from "../components/System/component/device_fen/Access";
import Barrier from "../components/System/component/device_fen/Barrier";


import TMonitor from "../components/System/component/device_typefen/Monitor";
import TDevice_Energy from "../components/System/component/device_typefen/device_Energy";
import TAccess from "../components/System/component/device_typefen/Access";
import TFire from "../components/System/component/device_typefen/Fire";
import TBarrier from "../components/System/component/device_typefen/Barrier";

import Device_type from "../components/System/Device_type";
import Log from "../components/System/Log";


import Visualization from "../components/Visualization/Index" //可视化大屏
import Esecurity from '../components/Visualization/Echart_security/Main' //监控管理
import Eenergy from '../components/Visualization/Echart_enrgy/Index' //能耗管理
import Efire from '../components/Visualization/Echart_fire/Index' //消防管理
import Epatrol from '../components/Visualization/Echart_patrol/Index' //巡检管理
import Evehicle from '../components/Visualization/Echart_car/Index' //车辆管理

const router = createBrowserRouter([
    { path: '/app', element: <App></App> },
    { path: '/login', element: <Login></Login> },
    { path:"/visualization",element:<Visualization></Visualization>,children:[
        {path:'/visualization',element:<Evehicle></Evehicle>},
        {path:'/visualization/esecurity',element:<Esecurity></Esecurity>},
        {path:'/visualization/eenergy',element:<Eenergy></Eenergy>},
        {path:'/visualization/efire',element:<Efire></Efire>},
        {path:'/visualization/epatrol',element:<Epatrol></Epatrol>},
        {path:'/visualization/evehicle',element:<Evehicle></Evehicle>}
    ]},//可视化大屏
    {
        path: '/park', element: <Park></Park>, children: [
            // 安防管理
            {
                path: "/park/security", element: <Security></Security>, children: [
                    { path: "/park/security", element: <Video></Video> },
                    { path: "/park/security/monitoring", element: <Video></Video> },
                    { path: "/park/security/playback", element: <Playback></Playback> },
                    { path: "/park/security/earlywarning", element: <Warning></Warning> },
                    { path: "/park/security/definitiono", element: <Definitiono></Definitiono> },
                    { path: "/park/security/threshold", element: <Threshold></Threshold> },
                    { path: "/park/security/analysis", element: <Analysiss></Analysiss> }
                ]
            },

            // 人車管理
            {
                path: "/park/people", element: <People></People>, children: [
                    { path: '/park/people', element: <Request></Request> },
                    { path: '/park/people/request', element: <Request></Request> },
                    { path: '/park/people/level', element: <Level></Level> },
                    { path: '/park/people/staff', element: <Staff></Staff> },
                    { path: '/park/people/cars', element: <Cars></Cars> },
                    { path: '/park/people/blackls', element: <Blackls></Blackls> },
                    { path: '/park/people/person', element: <Person></Person> },
                    { path: '/park/people/record', element: <Record></Record> }
                ]
            },

            // 能耗管理
            { path: "/park/energy", element: <Energy></Energy>,children:[
                {path:"/park/energy",element:<Analysis></Analysis>},
                {path:"/park/energy/analysis",element:<Analysis></Analysis>},
                {path:"/park/energy/warn",element:<Warn></Warn>},
                {path:"/park/energy/strategy",element:<Strategy></Strategy>},
                {path:"/park/energy/price",element:<Price></Price>},
                {path:"/park/energy/monthly",element:<Monthly></Monthly>}
            ] },

            // 巡檢管理
            {
                path: "/park/patrol", element: <Patrol></Patrol>, children: [
                    { path: "/park/patrol", element: <Task></Task> },
                    { path: "/park/patrol/task", element: <Task></Task> },
                    { path: "/park/patrol/plain", element: <Plain></Plain> },
                    { path: "/park/patrol/hidden", element: <Hidden></Hidden> },
                    { path: "/park/patrol/point", element: <Point></Point> },
                    { path: "/park/patrol/personnel", element: <Personnel></Personnel> },
                    { path: "/park/patrol/template", element: <Template></Template> }
                ]
            },

            // 系統管理
            { path: "/park/system", element: <System></System> ,children:[
                {path: "/park/system", element: <System_zhu></System_zhu>},
                { path: "/park/system/user", element: <System_zhu></System_zhu> },
                { path: "/park/system/dept", element: <Department></Department> },
                { path: "/park/system/role", element: <Roles></Roles> },
                { path: "/park/system/space", element: <Space></Space> },
                { path: "/park/system/device", element: <Device></Device>,children:[
                    { path: "/park/system/device", element: <Monitor></Monitor> },
                    { path: "/park/system/device/energy", element: <Device_Energy></Device_Energy> },
                    { path: "/park/system/device/access", element: <Access></Access> },
                    { path: "/park/system/device/fire", element: <Fire></Fire> },
                    { path: "/park/system/device/barrier", element: <Barrier></Barrier> },
                ]},
                { path: "/park/system/type", element: <Device_type></Device_type>,children:[
                    { path: "/park/system/type", element: <TMonitor></TMonitor> },
                    { path: "/park/system/type/energy", element: <TDevice_Energy></TDevice_Energy> },
                    { path: "/park/system/type/access", element: <TAccess></TAccess> },
                    { path: "/park/system/type/fire", element: <TFire></TFire> },
                    { path: "/park/system/type/barrier", element: <TBarrier></TBarrier> },
                ] },
                { path: "/park/system/log", element: <Log></Log> }
            ]},
         
            
        ]
    },
    { path: '/', element: <Navigate to="/login"></Navigate> }
])


export default router