import { configureStore } from "@reduxjs/toolkit";
import data from "./module/data";
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// 配置持久化选项
const persistConfig = {
  key: 'root',
  storage
};


const persistedReducer = persistReducer(persistConfig, data.reducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ['persist/PERSIST', 'persist/REHYDRATE'], // 忽略特定的持久化动作
        ignoredPaths: ['data.register'], // 忽略特定的路径
      },
    })
})

// export default store;

const persistor = persistStore(store);

export { store, persistor };