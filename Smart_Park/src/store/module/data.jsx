import { createSlice } from "@reduxjs/toolkit";
const data = createSlice({
    name: "data",
    initialState: {
        mid: "67160f581d9ce9fe989fe468",
        userModel: {
            "_id": "6716351c1d9ce9fe989fe49f",
            "uname": "小池",
            "pwd": "12345",
            "roleid": {
                "mids": [],
                "_id": "671634381d9ce9fe989fe499",
                "rname": "普通用户"
            }
        },
        patrol:[],
        hidden: [],
        plain: [],
        personnels:[],
        options: [],
        template: [],
        message:"",
        pid:'',
        level:"",
        state:'',
        examine:"",
        stateTime:"",
        endTime:"",
        staff_message:"",
        type:"",
        car:'',
        driver:'',
        blackList_message:"",
        person_message:"",
        person_type:"",
        access:"",
        pass:"",
        inOut:"",
        passStateTime:"",
        passEndTime:"",
        record_message:"",
        record_driver:"",
        lockage:"",
        record_inOut:"",
        recordStateTime:"",
        recordEndTime:""
    },
    reducers: {
        getmid(state, action) {
            state.mid = action.payload
        },
        setuserModel(state, action) {
            state.userModel = action.payload[0]
        },

        // 巡检
        setpatrol(state, action) {
            state.patrol = action.payload
        },
        setplain(state, action) {
            state.plain = action.payload
        },
        sethidden(state, action) {
            state.hidden = action.payload
        },
        setpersonnels(state, action) {
            state.personnels = action.payload
        },
        setoptions(state, action) {
            state.options = action.payload
        },
        settemplate(state, action) {
            state.template = action.payload
        },
        // 巡检

        setQuery(state, action) {
            state.message = action.payload.message
            state.pid = action.payload.pid
            state.examine = action.payload.examine
            state.stateTime = action.payload.stateTime
            state.endTime = action.payload.endTime
        },
        delQuery(state, action) {
            state.message = ""
            state.pid = ""
            state.examine = ""
            state.stateTime = ""
            state.endTime = ""
        },
        level_setQuery(state, action) {
            state.level = action.payload.level
            state.state = action.payload.state
        },
        level_delQuery(state, action) {
            state.level = ''
            state.state = ''
        },
        staff_setQuery(state, action) {
            state.staff_message = action.payload.staff_message
            state.type = action.payload.type
        },
        staff_delQuery(state, action) {
            state.staff_message = ""
            state.type = ""
        },
        car_setQuery(state, action) {
            state.car = action.payload.car
            state.driver = action.payload.driver
        },
        car_delQuery(state, action) {
            state.car = ""
            state.driver = ""
        },
        blackList_setQuery(state, action) {
            state.blackList_message = action.payload.blackList_message
        },
        blackList_delQuery(state, action) {
            state.blackList_message = ""
        },
        person_setQuery(state, action) {
            state.person_message = action.payload.person_message
            state.person_type = action.payload.person_type
            state.access = action.payload.access
            state.pass = action.payload.pass
            state.inOut = action.payload.inOut
            state.passStateTime = action.payload.passStateTime
            state.passEndTime = action.payload.passEndTime
        },
        person_delQuery(state, action) {
            state.person_message = ""
            state.person_type = ""
            state.access = ""
            state.pass = ""
            state.inOut = ""
            state.passStateTime = ""
            state.passEndTime = ""
        },
        record_setQuery(state, action) {
            state.record_message = action.payload.record_message
            state.record_driver = action.payload.record_driver
            state.lockage = action.payload.lockage
            state.record_inOut = action.payload.record_inOut
            state.recordStateTime = action.payload.recordStateTime
            state.recordEndTime = action.payload.recordEndTime
        },
        record_delQuery(state, action) {
            state.record_message = ""
            state.record_driver = ""
            state.lockage = ""
            state.record_inOut = ""
            state.recordStateTime = ""
            state.recordEndTime = ""
        }
    }
})


// 不要忘记暴露
// 不要覆盖
export default data;
export const { getmid, setuserModel, setpatrol, setplain, sethidden,setoptions, setpersonnels, settemplate,setQuery, delQuery, level_setQuery, level_delQuery,staff_setQuery,staff_delQuery,car_setQuery,car_delQuery,blackList_setQuery,blackList_delQuery,person_setQuery,person_delQuery,record_setQuery,record_delQuery } = data.actions
