import React from 'react';
import './Login.css';
import { useNavigate } from 'react-router-dom';
import request from '../../utils/request';
import { useDispatch } from 'react-redux';
import { setuserModel } from '../../store/module/data';
import { UserOutlined,LockOutlined } from '@ant-design/icons';

export default function Index() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [uname, setUname] = React.useState('小池');
  const [pwd, setPwd] = React.useState('12345');

  const login = async (e) => {
    e.preventDefault(); // 阻止表单的默认提交行为
    let res = await request.post("/login", { uname, pwd });
    console.log(res);
    // 登录成功，将用户信息存储到redux中
    if (res.status === 0) {
      dispatch(setuserModel(res.userModel));
      navigate('/park/security');
    }
  }

  return (
    <div className='bodys'>   
      <div className="login-container">
        <div className="login-card">        
          <p style={{color: "#FFF",letterSpacing:"5px",marginBottom:"20px"}}>欢迎使用智会园区平台</p>
          <form onSubmit={login}> {/* 将 login 函数与表单的 onSubmit 事件关联 */}
            <div className="input-group">
              <label htmlFor="username"><UserOutlined style={{color:'white'}}/> </label>&nbsp;
              <input
                type="text"
                id="username"
                placeholder='请输入用户名'
                value={uname}
                onChange={(e) => setUname(e.target.value)} // 确保这里的 e 是事件对象
              />
            </div>
            <div className="input-group">
              <label htmlFor="password"><LockOutlined style={{color:'white'}}/></label>&nbsp;
              <input
                type="password"
                id="password"
                placeholder='请输入密码'
                value={pwd}
                onChange={(e) => setPwd(e.target.value)} // 确保这里的 e 是事件对象
              />
            </div>
            <button type="submit" className="login-button">登录</button> {/* 将按钮类型设置为 submit */}
          </form>
          <p className="footer-text">还没有账户？<a href="#">注册</a></p>
        </div>
      </div>
    </div>

  );
}
