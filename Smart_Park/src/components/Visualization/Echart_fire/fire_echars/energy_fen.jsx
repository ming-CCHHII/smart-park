import React, { useEffect, useState } from 'react'
import EChartsReact from 'echarts-for-react';
import axios from '../../../../utils/request'
export default function device_type() {
    const [type_nei, setType_nei] = useState(0)
    const [type_yu, setType_yu] = useState(0)
    const [type_fei, setType_fei] = useState(0)
    const [option, setOption] = useState({})
    
    const getPerson = async () => {
        const res = await axios.get('/people/person')
        setType_nei(res.data.filter(item => item.type === '火灾探测器').length)
        setType_yu(res.data.filter(item => item.type === '烟雾探测器').length)
        setType_fei(res.data.filter(item => item.type === '湿温度设备').length)
        setOption({
            color: [
                'rgb(10, 115, 255)',
                'rgb(61, 171, 255)',
                'rgb(67, 204, 255)',
            ],
            legend: {
                orient: 'vertical',
                top: '85%',
                orient: 'horizontal',
                textStyle: {
                    color: '#fff'
                },
            },
            series: [
                {
                    type: 'pie',
                    bottom: '20%',
                    data: [
                        { value: type_nei, name: '火灾预警' },
                        { value: type_yu, name: '烟雾预警' },
                        { value: type_fei, name: '高温预警' },
                    ],
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    
                }
            ]
        })
    }
    useEffect(() => {
        getPerson()
    }, [type_nei])
    return (
        <div>
            <EChartsReact option={option} style={{ width: '100%', height: '150px' }} />
        </div>
    )
}
