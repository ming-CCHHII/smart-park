import React, { useEffect, useState } from 'react'
import EChartsReact from 'echarts-for-react';
import axios from '../../../../utils/request'
export default function device_type() {
    const [type_nei, setType_nei] = useState(0)
    const [type_yu, setType_yu] = useState(0)
    const [type_fei, setType_fei] = useState(0)
    const [option, setOption] = useState({})
    const [data,setdata]=useState([])

    const getPerson = async () => {
        const {data}=await axios.get("/System_user/device_list")
        let  data1=data.filter((item)=>{
        return item.bian.slice(0,2)=="XF"
        })
      console.log(data1)
        setType_nei(data1.filter(item => item.type === '火灾探测器').length)
        setType_yu(data1.filter(item => item.type === '烟雾探测器').length)
        setType_fei(data1.filter(item => item.type === '湿温度设备').length)
        setOption({
            color: [
                'rgb(10, 115, 255)',
                'rgb(61, 171, 255)',
                'rgb(67, 204, 255)',
            ],
            legend: {
                orient: 'vertical',
                top: '85%',
                textStyle: {
                    color: '#fff'
                },
            },
            series: [
                {
                    type: 'pie',
                    bottom: '20%',
                    data: [
                        { value: type_nei, name: '火灾探测器' },
                        { value: type_yu, name: '烟雾探测器' },
                        { value: type_fei, name: '湿温度设备' },
                    ],
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    
                }
            ]
        })
    }
    useEffect(() => {
       
        getPerson()
      
       
    }, [])
    return (
        <div style={{marginTop:'30px'}}>
            <EChartsReact option={option} style={{ width: '100%', height: '150px' }} />
        </div>
    )
}
