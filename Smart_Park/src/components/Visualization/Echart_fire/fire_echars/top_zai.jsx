import React, { useEffect, useState } from 'react'
import axios from '../../../../utils/request'
export default function top_zai() {
    const [list, setlist] = useState([])
    const getDeviceList = async () => {
        // 获取设备列表
        const { data } = await axios.get("/System_user/device_list")
            setlist(data)
    }
    useEffect(() => {
        getDeviceList()
    }, [])
    return (
        <div style={{ display: "flex", justifyContent: "space-around", alignItems: "center", width: "100%", height: "100%" }}>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{list.length}</h3>
                <p>设备数量</p>
            </div>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{list.filter((item) => item.flag == 1).length}</h3>
                <p>在线设备</p>
            </div>
            <div>
                <h3 style={{color: "red",fontSize:'1.5rem'}}>{list.filter((item) => item.flag == 0).length}</h3>
                <p>离线设备</p>
            </div>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{list.filter((item) => item.flag == 2).length}</h3>
                <p>故障设备</p>
            </div>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{Math.floor((list.filter((item) => item.flag == 1).length / list.length)*100)}%</h3>
                <p>在线率</p>
            </div>
            <div>
                <h3 style={{color: "red",fontSize:'1.5rem'}}>{Math.floor((list.filter((item) => item.flag == 2).length / list.length)*100)}%</h3>
                <p>故障率</p>
            </div>
        </div>
    )
}
