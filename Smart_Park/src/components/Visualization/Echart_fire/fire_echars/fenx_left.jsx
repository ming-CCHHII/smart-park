import React, { useEffect, useState } from 'react'
import EChartsReact from 'echarts-for-react';
import request from '../../../../utils/request';
export default function fenx_left() {
    const [list,setlist]=useState([])
    const [list1,setlist1]=useState([])
  const  option = {
        xAxis: {
          type: 'category',
          boundaryGap: false,
          data: list
        },
        yAxis: {
          type: 'value'
        },
        series: [
          {
            data: list1,
            type: 'line',
            areaStyle: {}
          }
        ]
      };
    const getlist=async()=>{
      const {data}=await request.get("/energy/getenergyWarn")
      let pp1=[]
      let pp2=[]
       data.forEach(element => {
        // 去重
        if(!pp1.includes((element.time).slice(5,7))){
           pp1.push((element.time).slice(5,7))
        }
        pp2.push(element.threshold)
      });
      pp1.sort((a,b)=>a-b)
      setlist(pp1)
      setlist1(pp2)
    }
    useEffect(()=>{
      getlist()
    },[])
  return (
    <div>
        
        <EChartsReact option={option} style={{ width: '95%', height: '250px' ,marginLeft:"20px",marginTop:"-20px"}} />





    </div>
  )
}
