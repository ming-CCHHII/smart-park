import React from 'react'
import Left from './left'
import Center from './center'
import Right from "../Echart_fire/Right"
import Bar from "../Echart_patrol/Echars/bar"
export default function Index() {
  return (
    <>
      <div style={{display:'flex',justifyContent:'space-around',width:'100%',paddingTop:'20px'}}>
        <div style={{height:'80vh',width:'20%'}}>
          <Left/>
        </div>
        <div style={{height:'80vh',width:'40%'}}>
            <Center></Center>
        </div>
     
        
        <div style={{height:'80vh',width:'20%'}}>
            <Right></Right>
        </div>
      </div>
    </>
  )
}
