import React, { useEffect, useState } from 'react'
import axios from '../../../utils/request'
import { BorderBox1 } from '@jiaminghi/data-view-react'
import Device_type from './fire_echars/device_type'
import Fen_left from "./fire_echars/fenx_left"
import Energy_fen from "./fire_echars/energy_fen"
export default function left() {
    let [personLength, setPersonLength] = useState(0)
    let [carLength, setCarLength] = useState(0)
    const getPersonLength = async () => {
        let res = await axios.get('/people/person')
        setPersonLength(res.data.length)
    }
    const getCarLength = async () => {
        let res = await axios.get('/people/record')
        setCarLength(res.data.length)
    }
    useEffect(() => {
        getPersonLength()
        getCarLength()
    }, [personLength])
    return (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', height: '80vh', color: '#fff' }}>
            <div style={{ height: '25vh', width: '100%',backgroundImage: "url('https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13135.svg')" }}>
                <BorderBox1>
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>设备类型分析</p>
                    </div>
                    <div>
                        <Device_type/>
                    </div>
                </BorderBox1>
            </div>
            <div style={{ height: '25vh', width: '100%',backgroundImage: "url('https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13135.svg')" }}>
                <BorderBox1>
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>预警类型分析</p>
                    </div>
                    <div>
                        <Energy_fen/>
                    </div>
                </BorderBox1>
            </div>
            <div style={{ height: '25vh', width: '100%',backgroundImage: "url('https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13135.svg')" }}>
                <BorderBox1>
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>预警趋势分析</p>
                    </div>
                    <div>
                    <Fen_left></Fen_left>
                    </div>
                </BorderBox1>
            </div>
        </div>
    )
}
