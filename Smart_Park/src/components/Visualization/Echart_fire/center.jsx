import React from 'react'
import { Decoration3,Decoration10,BorderBox10,BorderBox11 } from '@jiaminghi/data-view-react'
import Top_zai from './fire_echars/top_zai'
import Fang from './fire_echars/fang'
import Bar from "../Echart_patrol/Echars/bar"


export default function center() {
    return (
        <div style={{width:"100%",height:"20%"}}>
            <BorderBox10  width="100px" >
            <Decoration3 style={{width: '100%', height: '50px'}} />
            <div style={{ height: '4vh', width: '100%', color: '#fff', textAlign: 'center'}}>
                <Top_zai />
            </div>
            </BorderBox10>
            <Decoration10 style={{width: '100%', height: '5px'}} />
            <div style={{ height: '50vh', width: '100%', marginTop: '10px', textAlign: 'center'}}>
                <Fang></Fang>
            </div>
            <div>
       
            </div>
            <div style={{ height: '80%', width: '100%', display: "flex", justifyContent: "space-around" }}>
            <BorderBox11 style={{ width: "90%",marginTop:"550px" }}>
                <div style={{ display: "flex", justifyContent: "center", alignItems: "center", width: "100%" }}>
                    <Bar></Bar>
                </div>
            </BorderBox11>
       
        </div>
        <div style={{ height: '35%', width: '100%', display: "flex", justifyContent: "space-around" }}>
            <BorderBox11 style={{ width: "90%",marginTop:"550px" }}>
                <div style={{ display: "flex", justifyContent: "center", alignItems: "center", width: "100%" }}>
                    <Bar></Bar>
                </div>
            </BorderBox11>
       
        </div>
        </div>
        
    )
}
