import React from 'react'
import style from '../../../assets/css/energy.module.css'
export default function Index() {
  return (
    <div className={style.energy_parsel}>
      <div className={style.left}>
        <div className={style.l_box1}>
          <div className={style.template_box_tit}>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <p className={style.tit_p}>
              <span>能耗费用统计</span>
            </p>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
          </div>
        </div>
        <div className={style.l_box1}>
          <div className={style.template_box_tit}>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <p className={style.tit_p}>
              <span>用电类型分析</span>
            </p>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
          </div>
        </div>
        <div className={style.l_box1}>
          <div className={style.template_box_tit}>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <p className={style.tit_p}>
              <span>用水类型分析</span>
            </p>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
          </div>
        </div>
      </div>
      <div className={style.center}>
        <div className={style.img_box}>
          <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13103.png" alt="" />
        </div>
        <div className={style.img_box1}>
          <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E8%83%BD%E8%80%97%E6%95%B0%E6%8D%AE%E5%A4%A7%E5%B1%8F/u13341.png" alt="" />
        </div>
        <div className={style.cen_bottom}>
          <div className={style.l_box}>
            <div className={style.template_box_tit}>
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
              <p className={style.tit_p1}>
                <span>能耗趋势分析</span>
              </p>
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
            </div>
          </div>
          <div className={style.l_box1}>
            <div className={style.template_box_tit}>
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
              <p className={style.tit_p2}>
                <span>预警趋势分析</span>
              </p>
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
              <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
            </div>
          </div>
        </div>

      </div>
      <div className={style.right}>
        <div className={style.r_box}>
          <div className={style.template_box_tit}>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <p className={style.tit_p3}>
              <span>预计处理统计</span>
            </p>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
          </div>
        </div>
        <div className={style.r_box1}>
          <div className={style.template_box_tit}>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <p className={style.tit_p5}>
              <span>预警记录</span>
            </p>
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13131.svg" alt="" />
            <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13130.svg" alt="" />
          </div>
        </div>
      </div>
    </div>
  )
}
