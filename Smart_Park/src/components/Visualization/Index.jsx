import React, { useState } from 'react'
import style from '../../assets/css/echarts.module.css'
import { Outlet, useNavigate } from 'react-router-dom'
import dayjs from 'dayjs'

export default function Index() {
  const navigate=useNavigate()
  const [ull, setUll] = useState([
    { name: '监控管理', icon: 'https://cdn7.axureshop.com/demo2024/2291721/images/%E8%A7%86%E9%A2%91%E7%9B%91%E6%8E%A7/u505.png', path: '/visualization/esecurity' },
    { name: '能耗管理', icon: 'https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13116.png', path: '/visualization/eenergy' },
    { name: '人车管理', icon: 'https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13120.png', path: '/visualization/evehicle' },
    {name:'',path:''},
    {name:'',path:''},
    {name:'',path:''},
    {name:'',path:''},
    {name:'',path:''},
    {name:'',path:''},
    {name:'',path:''},
    {name:'',path:''},
    { name: '消防数据', icon: 'https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13124.png', path: '/visualization/efire' },
    { name: '巡检数据', icon: 'https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13128.png', path: '/visualization/epatrol' }
  ])
  const [num, setNum] = useState(0)
  return (
    <div className={style.echarts_box}>
      <div className={style.top}>
        <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13105.png" alt="" />
      </div>
      <div className={style.position_tit}>
        智慧园区可视化驾驶舱
      </div>
      <ul className={style.position_ull}>
        {ull.map((item, index) => {
          return (
            <li key={index} onClick={() => {
              setNum(index)
              navigate(item.path)
            }} className={num === index ? style.active : ''}>
              <img src={item.icon} alt="" style={{
                display:item.icon?'block':'none'
              }} />
              {item.name}
            </li>
          )
        })
        }
      </ul>
      <div className={style.main}>
        <Outlet></Outlet>
      </div>
    </div>
  )
}
