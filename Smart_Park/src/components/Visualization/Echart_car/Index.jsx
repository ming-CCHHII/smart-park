import React from 'react'
import Left from './left'
import Center from './Center'
import Right from './Right'
export default function Index() {
  return (
    <>
      <div style={{display:'flex',justifyContent:'space-around',width:'100%',paddingTop:'20px'}}>
        <div style={{height:'80vh',width:'20%'}}>
          <Left/>
        </div>
        <div style={{height:'80vh',width:'60%'}}>
          <Center/>
        </div>
        <div style={{height:'80vh',width:'20%'}}>
          <Right/>
        </div>
      </div>
    </>
  )
}
