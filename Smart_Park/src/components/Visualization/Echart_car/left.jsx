import React, { useEffect, useState } from 'react'
import axios from '../../../utils/request'
import { BorderBox1, Decoration11 } from '@jiaminghi/data-view-react'
import Person_data from './Echarts/Person_data'
import Car_data from './Echarts/Car_data'
export default function left() {
    let [personLength, setPersonLength] = useState(0)
    let [carLength, setCarLength] = useState(0)
    const getPersonLength = async () => {
        let res = await axios.get('/people/person')
        setPersonLength(res.data.length)
    }
    const getCarLength = async () => {
        let res = await axios.get('/people/record')
        setCarLength(res.data.length)
    }
    useEffect(() => {
        getPersonLength()
        getCarLength()
    }, [personLength])
    return (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', height: '80vh', color: '#fff' }}>
            <div style={{ height: '25vh', width: '100%',backgroundImage: "url('https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13135.svg')" }}>
                <BorderBox1>
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>人车计数统计</p>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '10px' }}>
                        <Decoration11 style={{ width: '200px', height: '60px' }} >
                            <div>
                                <svg t="1731313227534" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2342" width="32" height="32"><path d="M640.766712 362.494964c0-71.386864-58.078767-129.464607-129.464607-129.464607-71.386864 0-129.464607 58.077743-129.464607 129.464607s58.077743 129.464607 129.464607 129.464607C582.687946 491.959571 640.766712 433.881828 640.766712 362.494964z" fill="#59b2db" p-id="2343"></path><path d="M829.65164 195.676611C744.998635 111.023606 632.445186 64.402799 512.727571 64.402799c-119.718639 0-232.272088 46.62183-316.925093 131.274836C111.148449 280.33064 64.527642 392.884089 64.527642 512.602727c0 119.717615 46.62183 232.272088 131.274836 316.925093 84.654029 84.655052 197.207477 131.275859 316.925093 131.275859 119.717615 0 232.272088-46.620807 316.925093-131.275859 84.655052-84.653005 131.275859-197.207477 131.275859-316.925093C960.927499 392.884089 914.306692 280.33064 829.65164 195.676611zM732.97581 764.003478c-11.04966 0-20.008706-8.959046-20.008706-20.008706 0-111.199615-90.466408-201.666022-201.664999-201.666022S309.637106 632.795157 309.637106 743.994772c0 11.04966-8.958022 20.008706-20.008706 20.008706-11.050684 0-20.008706-8.959046-20.008706-20.008706 0-107.011222 69.916373-197.986213 166.462243-229.684146-55.807027-27.76332-94.262874-85.36932-94.262874-151.816684 0-93.452415 76.029604-169.483042 169.483042-169.483042 93.452415 0 169.483042 76.029604 169.483042 169.483042 0 66.446341-38.455847 124.052341-94.262874 151.816684 96.54587 31.697933 166.462243 122.6719 166.462243 229.684146C752.984516 755.044432 744.025471 764.003478 732.97581 764.003478z" fill="#59b2db" p-id="2344"></path></svg>
                            </div>
                            <div style={{marginLeft:'1rem',textAlign:'center'}}>
                                <p style={{ color: 'rgb(75, 122, 166)' }}>人流次数</p>
                                <h4>{personLength}</h4>
                            </div>
                        </Decoration11>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '10px' }}>
                        <Decoration11 style={{ width: '200px', height: '60px' }} >
                            <div>
                                <svg t="1731314302507" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3564" width="32" height="32"><path d="M912.832 467.104h-22.272l-6.624-22.464h60.928a32 32 0 1 0 0-64h-79.808l-35.584-120.544C815.168 221.28 771.52 192 728 192H302.592c-43.52 0-87.136 29.28-102.112 70.112l-34.944 118.528H96a32 32 0 0 0 0 64h50.624l-6.624 22.464H128c-35.296 0-64 28.704-64 64v282.944h51.104v34.816a32 32 0 1 0 64 0v-34.816h690.528v34.816a32 32 0 1 0 64 0v-34.816h43.232v-282.944a64.128 64.128 0 0 0-64.032-64zM128 750.016v-104.96h73.792c26.464 0 50.528-16.672 59.872-41.408l24.064-63.712c1.088-2.912 1.216-5.888 1.44-8.832h450.528c0.224 2.944 0.352 5.92 1.44 8.832l24.064 63.648a64.256 64.256 0 0 0 59.872 41.472h89.76v104.96H128z m784.832-168.96l-89.76-0.032-18.848-49.952h108.608v49.984zM218.528 426.944a30.944 30.944 0 0 0 2.816-9.568L261.184 282.24c4.48-12.16 23.52-26.24 41.408-26.24H728c17.888 0 36.928 14.08 40.736 24.192l55.104 186.88H206.72l11.808-40.128z m2.144 104.16l-18.88 49.984H128v-49.984h92.672z" p-id="3565" fill="#59b2db"></path><path d="M614.752 626.24h-188.64a32 32 0 1 0 0 64h188.64a32 32 0 1 0 0-64z" p-id="3566" fill="#59b2db"></path></svg>
                            </div>
                            <div style={{marginLeft:'1rem',textAlign:'center'}}>
                                <p style={{ color: 'rgb(75, 122, 166)' }}>人流次数</p>
                                <h4>{carLength}</h4>
                            </div>
                        </Decoration11>
                    </div>
                </BorderBox1>
            </div>
            <div style={{ height: '25vh', width: '100%',backgroundImage: "url('https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13135.svg')" }}>
                <BorderBox1>
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>人流类型分析</p>
                    </div>
                    <div>
                        <Person_data/>
                    </div>
                </BorderBox1>
            </div>
            <div style={{ height: '25vh', width: '100%',backgroundImage: "url('https://cdn7.axureshop.com/demo2024/2291721/images/%E5%AE%89%E9%98%B2%E7%9B%91%E6%8E%A7%E5%A4%A7%E5%B1%8F/u13135.svg')" }}>
                <BorderBox1>
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>车流类型分析</p>
                    </div>
                    <div>
                        <Car_data />
                    </div>
                </BorderBox1>
            </div>
        </div>
    )
}
