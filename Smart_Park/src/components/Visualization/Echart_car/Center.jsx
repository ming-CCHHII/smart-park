import React from 'react'
import { Decoration3, Decoration10, BorderBox1 } from '@jiaminghi/data-view-react'
import Device_list from './Echarts/Device_list'
import ThreeCar from './Echarts/ThreeCar'
import PersonAndCar_data from './Echarts/PersonAndCar_data'
import WarningTrendAnalysis from './Echarts/WarningTrendAnalysis'
export default function Center() {
    return (
        <div>
            <Decoration3 style={{ width: '90%', height: '30px', margin: '0 auto' }} />
            <div style={{ height: '6vh', width: '90%', color: '#fff', textAlign: 'center', margin: '0 auto' }}>
                <Device_list />
            </div>
            <Decoration10 style={{ width: '90%', height: '5px', margin: '0 auto' }} />
            <div style={{ height: '40vh', width: '90%', margin: '1rem auto' }}>
                <ThreeCar />
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between', width: '90%', margin: '0 auto', color: '#fff' }}>
                <BorderBox1 style={{ width: '40%', height: '25vh' }} >
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>人/车流趋势分析</p>
                    </div>
                    <div>
                        <PersonAndCar_data />
                    </div>
                </BorderBox1>
                <BorderBox1 style={{ width: '40%', height: '25vh' }} >
                    <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
                        <p>预警趋势分析</p>
                    </div>
                    <div>
                        <WarningTrendAnalysis/>
                    </div>
                </BorderBox1>
            </div>
        </div>
    )
}
