import React, { useEffect, useState } from 'react'
import axios from '../../../../utils/request'
export default function Device_list() {
    const [device_list, setDevice_list] = useState([])
    const [DZ_list, setDZ_list] = useState([])
    const getDeviceList = async () => {
        // 获取设备列表
        const { data } = await axios.get("/System_user/device_list")
        setDevice_list(data.filter((item) => {
            return item.bian.slice(0, 2) == "MJ"
        }))
        setDZ_list(data.filter((item) => {
            return item.bian.slice(0, 2) == "DZ"
        }))
    }
    useEffect(() => {
        getDeviceList()
    }, [])
    return (
        <div style={{ display: "flex", justifyContent: "space-around", alignItems: "center", width: "100%", height: "100%" }}>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{device_list.length}</h3>
                <p>门禁设备</p>
            </div>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{device_list.filter((item) => item.flag == 1).length}</h3>
                <p>门禁在线</p>
            </div>
            <div>
                <h3 style={{color: "red",fontSize:'1.5rem'}}>{device_list.filter((item) => item.flag == 0).length}</h3>
                <p>门禁离线</p>
            </div>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{DZ_list.length}</h3>
                <p>车闸设备</p>
            </div>
            <div>
                <h3 style={{fontSize:'1.5rem'}}>{DZ_list.filter((item) => item.flag == 1).length}</h3>
                <p>车闸在线</p>
            </div>
            <div>
                <h3 style={{color: "red",fontSize:'1.5rem'}}>{DZ_list.filter((item) => item.flag == 0).length}</h3>
                <p>车闸离线</p>
            </div>
        </div>
    )
}
