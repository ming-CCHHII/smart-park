import React from 'react'

export default function VehicleCrossingCapture() {
  return (
    <div>
      <div style={{ margin: '0 auto', width: '100%', textAlign: 'center' }}>
        <img style={{ width: '70%' }} src="https://cdn7.axureshop.com/demo2024/2291721/images/%E4%BA%BA%E8%BD%A6%E6%95%B0%E6%8D%AE%E5%A4%A7%E5%B1%8F/u13804.png" alt="" />
      </div>
      <div style={{display: 'flex', justifyContent: 'space-between', marginTop: '10px',width:'70%',margin: '0 auto'}}>
        <div>
          <div>
            <div>
              <p>粤B 21D323</p>
            </div>
            <div style={{color: "rgb(255,255,255,0.5)"}}>
              <span>2022/12/12 12:12:10</span>
            </div>
          </div>
        </div>
        <div>
          <div>
            <p>司机：小白</p>
          </div>
          <div style={{color: "rgb(255,255,255,0.5)"}}>
            <span>员工私家车</span>
          </div>
        </div>
      </div>

    </div>
  )
}
