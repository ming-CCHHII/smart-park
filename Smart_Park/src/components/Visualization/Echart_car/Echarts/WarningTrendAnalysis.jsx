import React, { useState } from 'react'
import EChartsReact from 'echarts-for-react';
export default function WarningTrendAnalysis() {
    const [option, setOption] = useState({
        xAxis: {
            type: 'category',
            data: ['10', '11', '12', '13', '14', '15', '16']
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                data: [2, 4, 6, 6, 8, 4, 2],
                type: 'line'
            }
        ]
    })
    return (
        <div>
            <EChartsReact option={option} style={{ width: '100%', height: '200px' }} />
        </div>
    )
}
