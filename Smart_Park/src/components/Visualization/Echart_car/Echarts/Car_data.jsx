import React, { useEffect, useState } from 'react'
import EChartsReact from 'echarts-for-react';
import axios from '../../../../utils/request'
export default function Person_data() {
    const [fang_purpose, setFangPurpose] = useState(0)
    const [work_purpose, setWorkPurpose] = useState(0)
    const [company_purpose, setCompanyPurpose] = useState(0)
    const [option, setOption] = useState({})
    const getCar = async () => {
        const res = await axios.get('/people/record')
        setFangPurpose(res.data.filter((item) => item.purpose.slice(0,2) == '访客' ).length)
        setWorkPurpose(res.data.filter((item) => item.purpose.slice(0,2) == '员工' ).length)
        setCompanyPurpose(res.data.filter((item) => item.purpose.slice(0,2) == '公司' ).length)
        setOption({
            color: [
                'rgb(10, 115, 255)',
                'rgb(61, 171, 255)',
                'rgb(67, 204, 255)',
            ],
            legend: {
                orient: 'vertical',
                top: '85%',
                orient: 'horizontal',
                textStyle: {
                    color: '#fff'
                },
            },
            series: [
                {
                    type: 'pie',
                    bottom:'20%',
                    data: [
                        { value: fang_purpose, name: '访客车辆' },
                        { value: work_purpose, name: '员工私家车' },
                        { value: company_purpose, name: '公司车辆' },
                    ],
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        })
    }
    useEffect(() => {
        getCar()
    },[])
    return (
        <div>
            <EChartsReact option={option} style={{ width: '100%', height: '150px' }} />
        </div>
    )
}
