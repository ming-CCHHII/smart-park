import React, { useRef, useEffect } from 'react';
import { Canvas } from '@react-three/fiber';
import { OrbitControls, useTexture } from '@react-three/drei';
import * as THREE from 'three';

export default function SmartPark() {
    function Building({ position, height, texture, x, y }) {
        const [colorMap] = useTexture([texture]);
        colorMap.wrapS = THREE.RepeatWrapping;
        colorMap.wrapT = THREE.RepeatWrapping;
        colorMap.repeat.set(x, y); // 纹理重复次数
        colorMap.offset.set(0, 0); // 纹理偏移
        return (
            <mesh position={position}>
                <boxGeometry args={[4, height, 4]} />
                <meshStandardMaterial map={colorMap} />
            </mesh>
        );
    }

    function Tree({ position }) {
        return (
            <>
                <mesh position={position}>
                    <cylinderGeometry args={[0.9, 0.2, 1, 8]} />
                    <meshStandardMaterial color={0x8B4513} />
                </mesh>
                <mesh position={[position[0], position[1] + 1, position[2]]}>
                    <sphereGeometry args={[0.8, 8, 8]} />
                    <meshStandardMaterial color={0x228B22} />
                </mesh>
            </>
        );
    }
    // 创建停车位
    const createParkingSpaces = () => {
        const spaces = [];
        const spaceWidth = 6;
        const spaceLength = 12;
        const gap = 1;

        // 通过循环计算停车位的位置
        for (let row = 0; row < 5; row++) {
            for (let col = 0; col < 8; col++) {
                const position = [
                    col * (spaceWidth + gap) - 15,  // X轴
                    0.05,                            // Y轴（轻微抬高避免与地面重叠）
                    row * (spaceLength + gap) - 15   // Z轴
                ];
                spaces.push(position);
            }
        }

        return spaces;
    };

    // 创建车辆函数
    const createCar = (position) => {
        // 车轮的偏移量
        const wheelOffsets = [
            [-2, -3], [2, -3], // 前轮
            [-2, 3], [2, 3]    // 后轮
        ];

        return (
            <group position={position}>
                {/* 车身 */}
                <mesh position={[0, 1, 0]}>
                    <boxGeometry args={[4, 1, 8]} />
                    <meshStandardMaterial color={0xe74c3c}/>
                </mesh>

                <mesh position={[0, 2, 0]}>
                    <boxGeometry args={[3, 2, 4]} />
                    <meshStandardMaterial color={0xe74c3c}/>
                </mesh>
                <mesh position={[0, 2, 0]}>
                    <boxGeometry args={[2, 2.5, 4.5]} />
                    <meshStandardMaterial/>
                </mesh>

                {/* 车轮 */}
                {wheelOffsets.map(([x, z], index) => (
                    <mesh key={index} position={[x,0.5, z]}>
                        <cylinderGeometry args={[0.5, 0.5, 1]} />
                        <meshStandardMaterial color={0x11111e}/>
                    </mesh>
                ))}
            </group>
        );
    };

    // 获取停车位位置
    const parkingSpaces = createParkingSpaces();

    return (
        <Canvas>
            <ambientLight intensity={0.5} />
            <directionalLight position={[5, 10, 7.5]} intensity={1} />
            {/* 旋转控制 */}
            <OrbitControls
                target={[0, 2, 0]} // 设置旋转目标
                minDistance={5}     // 设置最小距离
                maxDistance={50}    // 设置最大距离
                enableZoom={true}   // 启用缩放
                enablePan={true}    // 启用平移
                dampingFactor={0.1} // 设置阻尼因子
                position={[30, 20, 30]} // 设置摄像机位置
            />

            {/* 地面 */}
            <mesh position={[0, -0.3, 0]} rotation={[-Math.PI / 2, 0, 0]}>
                <planeGeometry args={[80, 100]} />
                <meshStandardMaterial color={0x7f8c8d} />
            </mesh>

            {/* 添加停车位 */}
            {parkingSpaces.map((position, index) => (
                <mesh key={index} position={position}>
                    <boxGeometry args={[1, 0.1, 8]} />
                    <meshStandardMaterial />
                </mesh>
            ))}

            {/* 添加车辆 */}
            {parkingSpaces.slice(0, 6).map((position, index) => createCar(position))}
            <Building position={[-30, 4, -40]} height={8} texture='/textures/Gs.jpg' x='4' y='4' />
            <Building position={[30, 4, -40]} height={18} texture='/textures/pp.jpg' x='4' y='4' />
            <Building position={[20, 4, -40]} height={18} texture='/textures/L3.jpg' x='4' y='4' />
            <Building position={[-10, 4, -40]} height={18} texture='/textures/L3.jpg' x='4' y='4' />
            <Tree position={[-23, 0, -23]} />
            <Tree position={[-10, 0, -23]} />

            <Tree position={[23, 0, -23]} />
            <Tree position={[10, 0, -23]} />
        </Canvas>
    );
}
