import React,{useState,useEffect} from 'react'
import axios from "../../../../utils/request"
export default function PersonnelEntryAndExitRecords() {
  let [data,setData] = useState([])
  const getData = async()=>{
    let res = await axios.get('/people/person')
    setData(res.data)
    console.log(res.data);
  }
  useEffect(()=>{
    getData()
  },[])
  return (
    <div style={{height:'80%',width: '90%', overflowY: "scroll", msOverflowStyle: "none", scrollbarWidth: "none"}}>
            {data.map((item) => (
                <div key={item._id} style={{height: "50px",width: "100%", background: "rgb(15,23,45)", marginTop: "20px", boxSizing: "border-box", borderRadius: '5px',display:'flex',alignItems:'center',justifyContent:'space-around',padding:'0 10px'}}>
                    <div>
                        <img src={item.img} alt="" style={{width: "40px", height: "40px", borderRadius: "50%"}} />    
                    </div>
                    <div>
                        <div>
                            <p>人员姓名：{item.name}</p>    
                        </div>
                        <div style={{color: "rgb(255,255,255,0.5)"}}>
                            <span>{item.data}</span>
                        </div>
                    </div>
                    <div style={{textAlign: 'center'}}>
                        <div>
                            <p>{item.inOut ? '进场' : '离场'}</p>
                        </div>
                        <div style={{color: "rgb(255,255,255,0.5)"}}>
                            <span>{item.access}</span>
                        </div>
                    </div>               
                </div>
            ))}
        </div>
  )
}
