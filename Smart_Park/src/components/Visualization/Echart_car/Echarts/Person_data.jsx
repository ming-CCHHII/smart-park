import React, { useEffect, useState } from 'react'
import EChartsReact from 'echarts-for-react';
import axios from '../../../../utils/request'
export default function Person_data() {
    const [type_nei, setType_nei] = useState(0)
    const [type_yu, setType_yu] = useState(0)
    const [type_fei, setType_fei] = useState(0)
    const [option, setOption] = useState({})
    const getPerson = async () => {
        const res = await axios.get('/people/person')
        setType_nei(res.data.filter(item => item.type === '内部员工').length)
        setType_yu(res.data.filter(item => item.type === '预约访客').length)
        setType_fei(res.data.filter(item => item.type === '非内部员工').length)
        setOption({
            color: [
                'rgb(10, 115, 255)',
                'rgb(61, 171, 255)',
                'rgb(67, 204, 255)',
            ],
            legend: {
                orient: 'vertical',
                top: '85%',
                orient: 'horizontal',
                textStyle: {
                    color: '#fff'
                },
            },
            series: [
                {
                    type: 'pie',
                    bottom: '20%',
                    data: [
                        { value: type_nei, name: '内部员工' },
                        { value: type_yu, name: '预约访客' },
                        { value: type_fei, name: '非内部员工' },
                    ],
                    emphasis: {
                        itemStyle: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    
                }
            ]
        })
    }
    useEffect(() => {
        getPerson()
    }, [type_nei])
    return (
        <div>
            <EChartsReact option={option} style={{ width: '100%', height: '150px' }} />
        </div>
    )
}
