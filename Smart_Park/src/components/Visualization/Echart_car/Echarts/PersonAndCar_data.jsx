import React, { useState } from 'react'
import EChartsReact from 'echarts-for-react'
export default function PersonAndCar_data() {
    const [option, setOption] = useState({
        legend: {
            orient: 'horizontal',
            textStyle: {
                color: '#fff',
            },
        },
        color: [
            'rgb(10, 115, 255)',
            'rgb(61, 171, 255)',
            'rgb(67, 204, 255)',
        ],
        tooltip: {},
        dataset: {
            dimensions: ['product', '人流量', '车流量'],
            source: [
                { product: '1月', '人流量': 143, '车流量': 185 },
                { product: '2月', '人流量': 283, '车流量': 173 },
                { product: '3月', '人流量': 186, '车流量': 265 },
                { product: '4月', '人流量': 172, '车流量': 153 },
            ]
        },
        xAxis: { type: 'category' },
        yAxis: {},
        // Declare several bar series, each will be mapped
        // to a column of dataset.source by default.
        series: [{ type: 'bar' }, { type: 'bar' }]
    })
    return (
        <div>
            <EChartsReact option={option} style={{ width: '100%',height:'200px' }} />
        </div>
    )
}
