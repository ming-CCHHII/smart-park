import React from 'react'
import { BorderBox1, Decoration11 } from '@jiaminghi/data-view-react'
import VehicleCrossingCapture from './Echarts/VehicleCrossingCapture'
import PersonnelEntryAndExitRecords from './Echarts/PersonnelEntryAndExitRecords'
export default function Right() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', height: '80vh', color: '#fff' }}>
      <div style={{ height: '25vh', width: '100%' }}>
        <BorderBox1>
          <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
            <p>车辆过闸抓拍</p>
          </div>
          <div>
            <VehicleCrossingCapture />
          </div>
        </BorderBox1>
      </div>
      <div style={{ height: '50vh', width: '100%' }}>
        <BorderBox1>
          <div style={{ width: '100%', textAlign: 'center', margin: '0 auto' }}>
            <p>人员进出记录</p>
          </div>
          <div style={{ width: "100%", height: "100%", display: 'flex', justifyContent: 'center' }}>
            <PersonnelEntryAndExitRecords />
          </div>
        </BorderBox1>
      </div>
    </div>
  )
}
