import React from 'react'
import { BorderBox1, Decoration5, Decoration11, BorderBox11 } from '@jiaminghi/data-view-react'
import Pie from "./Echars/pie"
import Pies from "./Echars/pie copy"
import Line from "./Echars/line"
export default function left() {
    return (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', height: '100%', color: '#fff' }}>
            <div style={{ height: '40vh', width: '100%' }}>
                <BorderBox11 title="区域巡查点分析">
                    <BorderBox1>
                        <div style={{ paddingTop: "20px", boxSizing: "border-box",display: "flex", justifyContent: "center", alignItems: "center" }}>
                            <Pie></Pie>
                        </div>
                    </BorderBox1>
                </BorderBox11>
            </div>
            <div style={{ height: '40vh', width: '100%' }}>
                <BorderBox11 title="区域巡隐患等级分析">
                    <BorderBox1>
                        <div style={{ paddingTop: "20px", boxSizing: "border-box", display: "flex", justifyContent: "center", alignItems: "center"}}>
                            <Pies></Pies>
                        </div>
                    </BorderBox1>
                </BorderBox11>
            </div>
            <div style={{ height: '40vh', width: '100%' }}>
                <BorderBox11 title="隐患趋势分析">
                    <BorderBox1>
                        <div style={{ paddingTop: '10px', boxSizing: "border-box", display: "flex", justifyContent: "center", alignItems: "center"}}>
                            <Line></Line>
                        </div>
                    </BorderBox1>
                </BorderBox11>
            </div>
        </div>
    )
}
