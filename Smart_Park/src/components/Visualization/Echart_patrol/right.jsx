import React from 'react'
import { BorderBox1, Decoration5, Decoration11, BorderBox11 } from '@jiaminghi/data-view-react'
import RightTop from './Echars/rightTop'
import RightBot from "./Echars/rightBot"
export default function right() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', height: '100%', color: '#fff' }}>
            <div style={{ height: '40vh', width: '100%' }}>
                <BorderBox11 title="隐患整改统计">
                    <BorderBox1>
                        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100%' }}>
                            <RightTop></RightTop>
                        </div>
                    </BorderBox1>
                </BorderBox11>
            </div>
            <div style={{ height: '60vh', width: '100%' }}>
                <BorderBox11 title="隐患记录">
                    <BorderBox1>
                        <div style={{ width: "100%", height: "100%", display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '10px' }}>
                        <RightBot></RightBot>
                        </div>
                    </BorderBox1>
                </BorderBox11>
            </div>
        </div>
  )
}
