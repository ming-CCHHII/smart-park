// src/SmartPark.js
import React, { useRef, useEffect } from 'react';
import { Canvas } from '@react-three/fiber';
import { OrbitControls, useTexture } from '@react-three/drei';
import * as THREE from 'three';

function Building({ position, height, texture, x, y }) {
    const [colorMap] = useTexture([texture]);
    colorMap.wrapS = THREE.RepeatWrapping;
    colorMap.wrapT = THREE.RepeatWrapping;
    colorMap.repeat.set(x, y); // 纹理重复次数
    colorMap.offset.set(0, 0); // 纹理偏移
    return (
        <mesh position={position}>
            <boxGeometry args={[4, height, 4]} />
            <meshStandardMaterial map={colorMap} />
        </mesh>
    );
}

function Road() {
    const [roadTexture] = useTexture(['/textures/road_texture.png']);

    return (
        <mesh rotation-x={-Math.PI / 2}>
            <planeGeometry args={[130, 80]} />
            <meshStandardMaterial map={roadTexture} />
        </mesh>
    );
}

function Grass() {
    const [roadTexture] = useTexture(['/textures/618.png']);

    return (
        <mesh rotation-x={-Math.PI / 2} position={[0, 0.01, 0]}>
            <planeGeometry args={[100, 70]} />
            <meshStandardMaterial map={roadTexture} />
        </mesh>
    );
}

function Tree({ position }) {
    return (
        <>
            <mesh position={position}>
                <cylinderGeometry args={[0.9, 0.2, 1, 8]} />
                <meshStandardMaterial color={0x8B4513} />
            </mesh>
            <mesh position={[position[0], position[1] + 1, position[2]]}>
                <sphereGeometry args={[0.8, 8, 8]} />
                <meshStandardMaterial color={0x228B22} />
            </mesh>
        </>
    );
}

function SmartLamp({ position }) {
    return (
        <>
            <mesh position={position}>
                <cylinderGeometry args={[0.1, 0.1, 2, 8]} />
                <meshStandardMaterial color={0x555555} />
            </mesh>
            <mesh position={[position[0], position[1] + 2, position[2]]}>
                <sphereGeometry args={[0.5, 8, 8]} />
                <meshStandardMaterial color={0xffff00} />
            </mesh>
        </>
    );
}

const Warehouse = ({ position }) => {
    return (
        <>
            <mesh position={position} >
                <Ground />
                <Walls />
                <Roof />
                <OrbitControls />
            </mesh>

        </>
    );
};

const Ground = () => {
    return (
        <mesh rotation-x={-Math.PI / 2}>
            <planeGeometry args={[25, 20]} />
            <meshStandardMaterial color={0xaaaaaa} />
        </mesh>
    );
};

const Walls = () => {
    const wallMaterial = <meshStandardMaterial color={0xcccccc} />;

    return (
        <>
            <mesh position={[0, 2.5, -5]}>
                <boxGeometry args={[15, 5, 0.1]} />
                {wallMaterial}
            </mesh>
            <mesh position={[0, 2.5, 5]}>
                <boxGeometry args={[15, 5, 0.1]} />
                {wallMaterial}
            </mesh>
            <mesh position={[-5, 2.5, 0]}>
                <boxGeometry args={[10, 5, 10]} />
                {wallMaterial}
            </mesh>
            <mesh position={[5, 2.5, 0]}>
                <boxGeometry args={[10, 5, 10]} />
                {wallMaterial}
            </mesh>
        </>
    );
};

const Roof = () => {
    return (
        <mesh position={[0, 5.1, 0]}>
            <boxGeometry args={[20, 0.1, 10.5]} />
            <meshStandardMaterial color={0x888888} />
        </mesh>
    );
};
export default function SmartPark() {
    return (
        <Canvas
            camera={{ position: [20, 10, 20], fov: 100 }}
        >
            <ambientLight intensity={1} />
            <directionalLight position={[5, 10, 7.5]} intensity={1} />
            <OrbitControls
                target={[0, 2, 10]} // 设置旋转目标
                minDistance={5}     // 设置最小距离
                maxDistance={50}    // 设置最大距离
                enableZoom={true}   // 启用缩放
                enablePan={true}    // 启用平移
                dampingFactor={0.1} // 设置阻尼因子
            />
            <Building position={[-20, 4, -20]} height={8} texture='/textures/Gs.jpg' x='4' y='4' />
            <Building position={[0, 5, -20]} height={10} texture='/textures/pp.jpg' x='3' y='5' />
            <Building position={[20, 8, -20]} height={15} texture='/textures/L3.jpg' x='1' y='2' />

            <Building position={[-20, 6, -10]} height={12} texture='/textures/L2.jpg' x='1' y='4' />
            <Building position={[0, 7, -10]} height={15} texture='/textures/L1.jpg' x='1' y='2' />
            <Building position={[20, 3, -10]} height={4} texture='/textures/f8.jpg' x='4' y='3' />


            <Building position={[-20, 4, 25]} height={6} texture='/textures/Gs.jpg' x='4' y='4' />
            <Building position={[0, 5, 25]} height={10} texture='/textures/pp.jpg' x='3' y='5' />
            <Building position={[20, 3, 25]} height={6} texture='/textures/L3.jpg' x='1' y='2' />

            <Building position={[-20, 9, 10]} height={18} texture='/textures/L2.jpg' x='1' y='4' />
            <Building position={[0, 10, 10]} height={20} texture='/textures/L1.jpg' x='1' y='6' />
            <Building position={[20, 5, 10]} height={8} texture='/textures/f8.jpg' x='4' y='4' />


            <Road />
            <Grass />
            
            <Tree position={[-5, 0, -23]} />
            <Tree position={[-10, 0, -25]} />

            <Tree position={[5, 0, -23]} />
            <Tree position={[10, 0, -25]} />

            <Warehouse position={[0, 0, 0]} />

            <SmartLamp position={[-10, 1, -30]} />
            <SmartLamp position={[10, 1, -30]} />
        </Canvas>
    );
}
