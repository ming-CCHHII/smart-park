import React, { useState, useEffect } from 'react'
import EChartsReact from 'echarts-for-react';

export default function Line() {
  const [option, setOption] = useState({
    color: [
      'rgb(10, 115, 255)',
      'rgb(61, 171, 255)',
      'rgb(67, 204, 255)',
    ],
    series: [
      {
        name: 'Access From',
        type: 'pie',
        radius: ['40%', '30%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderWidth: 5,
        },
        emphasis: {
          label: {
            show: true,
            fontSize: 10
          }
        },
        labelLine: {
          show: false
        },
        data: [
          { value: 1048, name: 'A' },
          { value: 735, name: 'B' },
          { value: 580, name: 'C' },
          { value: 484, name: 'D' },
          { value: 300, name: 'E' }
        ]
      }
    ]
  });
  return (
    <>
      <EChartsReact option={option} style={{ height: '200px', width: '100%' }} />
    </>
  )
}
