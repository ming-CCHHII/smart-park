import React, { useEffect, useState } from 'react'
import request from "../../../../utils/request"
export default function rightBot() {
    const [data, setdata] = useState([])
    // 获取数据
    const gettabledata = async (item) => {
        const res = await request.post('/patrol/getpatrolHiddenModel', item)
        if (res.code == 200) {
            setdata(res.data)
            console.log(res.data)
        }
    }
    useEffect(() => {
        gettabledata()
    }, [])
    return (
        <div style={{height:'70%',width: '75%', overflowY: "scroll", msOverflowStyle: "none", scrollbarWidth: "none"}}>
            {data.map((item) => (
                <div key={item._id} style={{height: "50px",width: "100%", background: "rgb(15,23,45)", marginTop: "12px", padding: "12px", boxSizing: "border-box", borderRadius: '5px'}}>
                   温度过高预警（东区-1栋-大仓库）<br></br>
                    <span style={{fontSize: "10px", color: "#999"}}>{item.createtime}</span>                    
                </div>
            ))}
        </div>
    )
}
