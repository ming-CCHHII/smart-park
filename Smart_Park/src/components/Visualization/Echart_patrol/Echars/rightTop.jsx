import React, { useState, useEffect } from 'react'
import { Flex, Progress, ConfigProvider } from 'antd';

export default function Line() {

    return (
        <>
            <ConfigProvider
                theme={{
                    components: {
                        Progress: {
                            circleTextColor: "#fff"
                        },
                    },
                }}
            >
                <div>
                    <Progress trailColor="#ffcc00" type="circle" percent={75} />
                </div>

                <div style={{ display: 'flex', justifyContent: 'space-around', width: '100%' }}>
                    <div>
                        <span style={{ display: "block", width: "10px", height: "5px", backgroundColor: "blue" }}></span>
                        <span>已处理</span>
                    </div>
                    <div>
                        <span style={{ display: "block", width: "10px", height: "5px", backgroundColor: "#ffcc00" }}></span>
                        <span>未处理</span>
                    </div>
                </div>
            </ConfigProvider>
        </>
    )
}
