import React, { useState, useEffect } from 'react'
import EChartsReact from 'echarts-for-react';

export default function Line() {
    const [option, setOption] = useState({
        color: [
            'rgb(10, 115, 255)',
            'rgb(61, 171, 255)',
            'rgb(67, 204, 255)',
        ],
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },
        yAxis: {
            type: 'value'
        },
        series: [
            {
                data: [820, 932, 901, 934, 1290, 1330, 1320],
                type: 'line',
                areaStyle: {}
            }
        ]
    });
    return (
        <>
            <EChartsReact option={option} style={{ height: '250px', width: '80%' }} />
        </>
    )
}
