import React, { useState, useEffect } from 'react'
import EChartsReact from 'echarts-for-react';

export default function Line() {
  const [option, setOption] = useState({
    color: [
      'rgb(10, 115, 255)',
      'rgb(61, 171, 255)',
      'rgb(67, 204, 255)',
    ],
    series: [

      {
        name: 'Access From',
        type: 'pie',
        radius: ['0%', '40%'],
        avoidLabelOverlap: false,
        itemStyle: {
          borderWidth: 5,
        },
        emphasis: {
          label: {
            show: true,
            fontSize: 10
          }
        },
        labelLine: {
          show: false
        },
        data: [
          { value: 1048, name: 'Search Engine' },
          { value: 735, name: 'Direct' },
          { value: 580, name: 'Email' },
          { value: 484, name: 'Union Ads' },
          { value: 300, name: 'Video Ads' }
        ]
      }
    ]
  });
  return (
    <>
      <EChartsReact option={option} style={{ height: '230px', width: '100%' }} />
    </>
  )
}
