import React, { useState, useEffect } from 'react'
import EChartsReact from 'echarts-for-react';

export default function Line() {
    const [option, setOption] = useState({
        color: [
            'rgb(10, 115, 255)',
            'rgb(61, 171, 255)',
            'rgb(67, 204, 255)',
        ],
        legend: {
            top: "85%"
        },
        tooltip: {},
        dataset: {
            dimensions: ['product', '2015', '2016', '2017'],
            source: [
                { product: 'Matcha Latte', 2015: 43.3, 2016: 85.8, 2017: 93.7 },
                { product: 'Milk Tea', 2015: 83.1, 2016: 73.4, 2017: 55.1 },
                { product: 'Cheese Cocoa', 2015: 86.4, 2016: 65.2, 2017: 82.5 },
                { product: 'Walnut Brownie', 2015: 72.4, 2016: 53.9, 2017: 39.1 }
            ]
        },
        xAxis: { type: 'category' },
        yAxis: {},
        series: [{ type: 'bar' }, { type: 'bar' }, { type: 'bar' }]
    });
    return (
        <>
            <EChartsReact option={option} style={{ height: '250px', width: '90%' }} />
        </>
    )
}
