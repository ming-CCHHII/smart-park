import React from 'react'
import { BorderBox11 } from "@jiaminghi/data-view-react"
import Bar from "./Echars/bar"
import Bars from "./Echars/botRight"
export default function bottom() {
    return (
        <div style={{ height: '35%', width: '100%', display: "flex", justifyContent: "space-around" }}>
            <BorderBox11 style={{ width: "60%" }}>
                <div style={{ display: "flex", justifyContent: "center", alignItems: "center", width: "100%" }}>
                    <Bar></Bar>
                </div>
            </BorderBox11>
            <BorderBox11 style={{ width: '40%', height: '100%' }}>
                <div style={{ display: "flex", justifyContent: "center", alignItems: "center", width: "100%", height: "100%"}}>
                    <Bars></Bars>
                </div>
            </BorderBox11>
        </div>
    )
}
