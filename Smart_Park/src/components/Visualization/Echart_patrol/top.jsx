import React from 'react'
import { BorderBox4, Decoration3, Decoration10 } from '@jiaminghi/data-view-react'
import svg from "../../../assets/image/u13284.svg"
export default function top() {
    return (
        <div style={{ height: '20%', width: '100%'}}>
            <Decoration3 style={{width: "100%", height: "10%"}}></Decoration3>
            <BorderBox4>
                    <div style={{ height: '80%', width: '100%', display: 'flex', justifyContent: 'space-around', alignItems: 'center', color: "#fff" }}>
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "2rem"}}>20</p>
                            <br></br>
                            <p style={{fontSize: "90%"}}><img src={svg}/><img src={svg}/>设备总数</p>
                        </div>
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "2rem"}}>18</p>
                            <br></br>
                            <p style={{fontSize: "90%"}}><img src={svg}/><img src={svg}/>在线设备</p>
                        </div>
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "2rem"}}>2</p>
                            <br></br>
                            <p style={{fontSize: "90%"}}><img src={svg}/><img src={svg}/>离线设备</p>
                        </div>
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "2rem"}}>95%</p>
                            <br></br>
                            <p style={{fontSize: "90%"}}><img src={svg}/><img src={svg}/>在线率</p>
                        </div>
                        <div style={{textAlign: "center"}}>
                            <p style={{fontSize: "2rem"}}>0</p>
                            <br></br>
                            <p style={{fontSize: "90%"}}><img src={svg}/><img src={svg}/>设备故障</p>
                        </div>
                    </div>
                    <Decoration10 style={{width: "100%", height: "10%"}}></Decoration10>
            </BorderBox4>
        </div>
    )
}
