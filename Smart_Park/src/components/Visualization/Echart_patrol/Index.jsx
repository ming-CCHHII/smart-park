import React from 'react'
import Left from "./left"
import Right from "./right"
import Center from "./conter"
import Top from "./top"
import Bottom from "./bottom"
export default function Index() {
  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'space-around', width: '100%', paddingTop: '20px' }}>
        <div style={{ height: '100vh', width: '20%' }}>
          <Left />
        </div>
        <div style={{ height: '100vh', width: '50%', flexDirection: "column",display:'flex',justifyContent:'space-between'}}>
          <Top></Top>
          <Center></Center>
          <Bottom></Bottom>
        </div>
        <div style={{ height: '100vh', width: '20%' }}>
          <Right />
        </div>
      </div>
    </>
  )
}
