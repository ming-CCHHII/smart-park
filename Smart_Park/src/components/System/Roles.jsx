import React, { useEffect } from "react";
import { Button,Form, Tree ,Cascader, Input,Drawer,Switch,message,Table,Space } from "antd";
import dayjs from "dayjs"
import request from "../../utils/request";
export default function Roles() {
  const options2 = [
    {
      value: true,
      label: "有效",
    },
    {
      value: false,
      label: "关闭",
    },
  ];
  const optionfan=[
    {
      value: "全系统",
      label: "全系统",
    },
    {
      value: "所在部门及下级部门",
      label: "所在部门及下级部门",
    }
  ]
  const treeData = [
    {
      title: '安防管理',
      key: '0-0',
      children: [
        {
          title: '视频监控',
          key: '0-0-0',
        },
        {
          title: '视频回放',
          key: '0-0-1',
        },
        {
          title: '预警管理',
          key: '0-0-2',
        },
        {
          title: '预警事件定义',
          key: '0-0-3',
        },
        {
          title: '消防阈值设置',
          key: '0-0-4',
        },
        {
          title: '视频AI分析',
          key: '0-0-5',
        },
      ],
    },
    {
      title: '人车管理',
      key: '0-1',
      children: [
        {
          title: '访客申请管理',
          key: '0-1-0',
        },
        {
          title: '访客等级管理',
          key: '0-1-1',
        },
        {
          title: '人员管理',
          key: '0-1-2',
        },
        {
          title: '车辆管理',
          key: '0-1-3',
        },
        {
          title: '黑名单管理',
          key: '0-1-4',
        },
        {
          title: '人员进出管理',
          key: '0-1-5',
        },
        {
          title: '车辆进出设置',
          key: '0-1-6',
        },
      ],
    },
    {
      title: '能耗管理',
      key: '0-2',
      children: [
        {
          title: '能耗分析',
          key: '0-2-0',
        },
        {
          title: '能耗预警',
          key: '0-2-1',
        },
        {
          title: '预警策略',
          key: '0-2-2',
        },
        {
          title: '能源价格',
          key: '0-2-3',
        },
        {
          title: '月度账单',
          key: '0-2-4',
        },

      ],
    },
    {
      title: '巡检管理',
      key: '0-3',
      children: [
        {
          title: '巡检任务',
          key: '0-3-0',
        },
        {
          title: '巡检计划',
          key: '0-3-1',
        },
        {
          title: '隐患管理',
          key: '0-3-2',
        },
        {
          title: '巡查点管理',
          key: '0-3-3',
        },
        {
          title: '巡查人员管理',
          key: '0-3-4',
        },
        {
          title: '模版管理',
          key: '0-3-5',
        },

      ],
    },
    {
      title: '系统管理',
      key: '0-4',
      children: [
        {
          title: '用户管理',
          key: '0-4-0',
        },
        {
          title: '部门管理',
          key: '0-4-1',
        },
        {
          title: '角色管理',
          key: '0-4-2',
        },
        {
          title: '空间标识',
          key: '0-4-3',
        },
        {
          title: '设备管理',
          key: '0-4-4',
        },
        {
          title: '设备类型管理',
          key: '0-4-5',
        },
        {
          title: '系统日志',
          key: '0-4-6',
        },

      ],
    },
  ];
  const onChange2 = (value) => {
    console.log(value);
    setflag(value);
  };
  const onChangefan = (value) => {
    console.log(value);
  setfanadd(value[0])
  }
  const onChange= (value) => {
    console.log(value);
setflagadd(value)
  }
  const onCloseadd=()=>{
    setshowDraweradd(false)
  }
  const onFinishadd= async(values) => {


    
    if(update_id){
    let add={
      id:update_id,
      name:nameadd,
      flag:flagadd,
      fan:fanadd,
      model:model,
      key:key,
      updatadate:new Date()
    }
    console.log(add)
      await request.post("/System_user/role_update",add).then(res=>{
        if(res.code==200){
          message.success("修改成功")
          setshowDraweradd(false)
          getdata()
        }else{
          message.error("修改失败")
        }
      })
      setshowDraweradd(false)





    }else{
      let add1={
        name:nameadd,
        flag:flagadd,
        fan:fanadd,
        model:model,
        key:key,
        updatadate:new Date()
      }
      await request.post("/System_user/role_add",add1).then(res=>{
        if(res.code==200){
          message.success("添加成功")
          setshowDraweradd(false)
          getdata()
        }else{
          message.error("添加失败")
        }
      })
      setshowDraweradd(false)
    }
    





  }
  const onFinishFailed= (errorInfo) => {
    console.log("Failed:", errorInfo);
  }
  const [name, setname] = React.useState("");

  const [flag, setflag] = React.useState("");
  const [showDraweradd,setshowDraweradd]= React.useState(false);


  const [fanadd,setfanadd]= React.useState("");
  const [flagadd,setflagadd]= React.useState(false)
  const [nameadd,setnameadd]= React.useState("")

  // 树选择
  const [expandedKeys, setExpandedKeys] = React.useState(['0-0-0', '0-0-1']);
  const [checkedKeys, setCheckedKeys] = React.useState(['0-0-0']);
  const [selectedKeys, setSelectedKeys] = React.useState([]);
  const [autoExpandParent, setAutoExpandParent] = React.useState(true);
  const [key,setkey]=React.useState("")
  const [model,setmodel]=React.useState([])
  const onExpand = (expandedKeysValue) => {
    console.log('onExpand', expandedKeysValue);
    // if not set autoExpandParent to false, if children expanded, parent can not collapse.
    // or, you can remove all expanded children keys.
    setExpandedKeys(expandedKeysValue);
    setAutoExpandParent(false);
  };
  const onCheck = (checkedKeysValue,title) => {
    setCheckedKeys(checkedKeysValue);
    setmodel(title.checkedNodes)
    setkey(checkedKeysValue)
  };
  const onSelect = (selectedKeysValue, info) => {
    console.log('onSelect', info);
    setSelectedKeys(selectedKeysValue);
  };

  // 表格
  const [data,setdata]=React.useState([])
  const getdata=async()=>{
    await request.get("/System_user/role_list").then(res=>{
      setdata(res.data)
      console.log(res.data)
    })
  }
  useEffect(()=>{
    getdata()
  },[])
  const columns = [
    {
      title: '角色名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '权限模块',
      dataIndex: 'model',
      key: 'model',
      render: (text) => <div style={{display:"flex",width:'100%',flexWrap:"wrap",justifyContent:"left",alignItems:"center"}}>
        
        
        
        {text.map(item=>{
          return <span style={{display:"inline-block",padding:'10px',margin:"0 5px",color:'rgb(92, 145, 255)',borderRadius:"5px",backgroundColor:"rgb(216, 234, 255)"}} key={item._id}>{item.title} </span>
        })}
        
        </div> 
    },
    {
      title: '数据范围',
      dataIndex: 'fan',
      key: 'fan',

    },
    {
      title: '是否有效',
      dataIndex: 'flag',
      key: 'flag',
      render: (text) => (text == false ? '关闭' : '有效'),
    },



    {
      title: '更新时间',
      dataIndex: 'updatadate',
      key: 'updatadate',
      render: (text) => <span style={{width:'50%'}}>{(dayjs(text).format('YYYY-MM-DD HH:mm:ss'))}</span> ,
      width: 150,
    },
    {
      title: '操作',
      dataIndex: 'todo',
      key: 'todo',
      render: (_, record) => (
        <Space size="middle" style={{width:'130%'}}>
          <a onClick={()=>{
            role_hui(record)
          }} >编辑</a>
          <a style={{ color: "red" }} onClick={()=>{
          deleta(record._id)
          console.log(_id)
          }}>删除</a>
        </Space>
      ),
    },
  ]
  // 删除数据
  const deleta=async(id)=>{
  console.log(id)
    await request.post(`/System_user/role_delete?id=${id}`).then(res=>{
          if(res.code==200){
            message.success(res.msg)
            getdata()
          }else{
            message.error(res.msg)
          }
    })
  }
  // 回显数据
  const role_hui=async(ll)=>{
    setshowDraweradd(true)
    console.log(ll._id)
    setupdate_id(ll._id)
  await request.post(`/System_user/role_dan?id=${ll._id}`).then(res=>{
    if(res.code==200){
      setnameadd(ll.name)
      setflagadd(res.data.flag)
      setCheckedKeys(res.data.key)
      setfanadd(res.data.fan)
      setkey(res.data.key)
      setmodel(res.data.model)
    }
  
   
  })
  }
  const [update_id,setupdate_id]=React.useState("")
  const resetfang=()=>{
    setname("")
    setflag([])
  }

  // 修改数据
  return (
    <div style={{ width: "100%", height: "100%" }}>
      <div
        style={{
          width: "95%",
          backgroundColor: "rgb(255,255,255)",
          padding: "25px",
          marginBottom: "15px",
          boxSizing: "border-box",
          borderRadius: "5px",
        }}
      >
        <p style={{ display: "inline-block", width: "30%" }}>
          <span style={{ paddingRight: "5px" }}>角色名称:</span>
          <Input
            style={{ width: "70%" }}
            placeholder="请输入"
            value={name}
            onChange={(e) => {
              setname(e.target.value);
            }}
          ></Input>
        </p>
        <p style={{ display: "inline-block", width: "30%" }}>
          <span style={{ paddingRight: "5px" }}>是否有效:</span>
          <Cascader
            options={options2}
            onChange={onChange2}
            value={flag}
            placeholder="Please select"
          />
        </p>
        <Button type="primary" style={{ height: "40px" }}>
          搜索
        </Button>
        <Button
          style={{ marginLeft: "20px", height: "40px" }}
          onClick={() => {
            resetfang();
          }}
        >
          重置
        </Button>
      </div>
      <div
        style={{
          width: "95%",
          backgroundColor: "rgb(255,255,255)",
          padding: "25px",
          boxSizing: "border-box",
          borderRadius: "5px",
        }}
      >
        <div>
          <Button
            type="primary"
            onClick={() => {
             setshowDraweradd(true)
            }}
          >
            新增角色
          </Button>
        </div>




        <div>
        <Table columns={columns}
            // pagination={{
            //   current,
            //   pageSize,
            //   onChange: handlePageChange,
            //   showSizeChanger: true,
            //   pageSizeOptions: ['5', '10', '15'],
            // }}
          
            dataSource={data.map((item) => {
              return {
                ...item,
                key: item._id
              }
            })
            }

          /> 



        </div>
      </div>
      <div
        style={{
          width: "95%",
          backgroundColor: "rgb(255,255,255)",
          padding: "25px",
          boxSizing: "border-box",
          borderRadius: "5px",
        }}
      >
    <Drawer title="添加角色" onClose={onCloseadd} rules={[{ required: true }]} open={showDraweradd}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinishadd}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item  label="角色名称"
            name="name">
            <div>
              <Input value={nameadd}  onChange={(e)=>{
                setnameadd(e.target.value)
              }}></Input>
            </div>
          </Form.Item>
         
        
          <Form.Item  label="数据范围"
            name="fan">
            <div>
          <Cascader  options={optionfan}  onChange={onChangefan} value={fanadd}  placeholder="Please select" />
            </div>
          </Form.Item>

          <Form.Item  label="权限模块"
            name="model">
            <div style={{height:'500px',border:'1px solid gray',paddingLeft:'10px',paddingTop:'10px'}}>
            <Tree
      checkable
      onExpand={onExpand}
      expandedKeys={expandedKeys}
      autoExpandParent={autoExpandParent}
      onCheck={onCheck}
      checkedKeys={checkedKeys}
      onSelect={onSelect}
      selectedKeys={selectedKeys}
      treeData={treeData}
    />
























            </div>
          </Form.Item>
          <Form.Item  label="是否有效"
            name="flagadd">
            <div>
            <Switch value={flagadd} onChange={onChange}  />
            </div>
          </Form.Item>

          
          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             确认
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              取消
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
    
        
      </div>
    </div>
  );
}
