import React, { useEffect, useState,useCallback  } from 'react'
// import Table1 from './components/table1'
import request from '../../utils/request';
import { Space, Table,message } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import dayjs from 'dayjs';a
import { Input, Form, Drawer, Cascader, Button, Radio, Upload } from 'antd';

export default function task() {

  const [text, setText] = useState('');
  const [encryptedText, setEncryptedText] = useState('');
  const [decryptedText, setDecryptedText] = useState('');
  const handleEncrypt = async () => {
    try {
      const {data} = await axios.post('/System_user/list', { text });
      setEncryptedText(data.encrypted);
    } catch (error) {
      console.error('Error encrypting text:', error);
    }
  };









  const columns = [
    {
      title: '用户姓名',
      dataIndex: 'name',
      key: 'name',
      width: 100,
    },
    {
      title: '账号',
      dataIndex: 'zhanghao',
      key: 'zhanghao',
    },
    {
      title: '联系方式',
      dataIndex: 'phone',
      key: 'phone',

    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      render: (text) => (text == false ? '女' : '男'),
    },
    {
      title: '职位',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: '部门',
      dataIndex: 'department',
      key: 'department',
      render: (text) => (text.map((item) => item).join('-'))

    },
    {
      title: '角色',
      dataIndex: 'role',
      key: 'role',

    },
    {
      title: '照片',
      dataIndex: 'picture',
      key: 'picture',
      render: (text) => <img src={'http://127.0.0.1:3001/' + text} style={{ width: '50%' }} />,
    },
    {
      title: '创建时间',
      dataIndex: 'createdata',
      key: 'createdata',
      render: (text) => <span style={{width:'50%'}}>{(dayjs(text).format('YYYY-MM-DD HH:mm:ss'))}</span> ,
      width: 150,
    },
    {
      title: '操作',
      dataIndex: 'todo',
      key: 'todo',
      
      render: (_, { _id }) => (
        <Space size="middle" style={{width:'130%'}}>
          <a >重置密码</a>
          <a onClick={()=>{
            
            setOpenupdate(true)
            getupdatedata(_id)
          }} >编辑</a>
          <a style={{ color: "red" }} onClick={()=>{
            deletee(_id)
          }}>删除</a>
        </Space>
      ),
    },
  ]
  const [flag1, setFlag] = useState(false);
  const [current, setCurrent] = React.useState(1);
  const [pageSize, setPageSize] = React.useState(5);
  const [value, setValue] = React.useState("");
  const [value4, setValue4] = React.useState(1);
  const [depat, setDepat] = React.useState("");
  const [role, setRole] = React.useState("");

  const [yedepat,setyedepat]=React.useState("");
  const [yerole,setyerole]=React.useState("");
  const [data, setdata] = React.useState([])
  const [imageUrl, setimageUrl] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [updatedata, setupdatedata] = React.useState([])
  const [sexadd,setsexadd]=React.useState(true)
  const [updateImg,setupdateImg]=React.useState("")
  // 删除
const deletee=async(_id)=>{
  request.post("/System_user/delete",{id:_id}).then(res=>{
    if (res.code == 200) {
      message.open({
        type: 'success',
        content: '删除成功',
      });
      getdata()
      
    }
  })
}
// 获取单挑数据
const getupdatedata=(_id)=>{
  request.post("/System_user/getupdatedata",{id:_id}).then(res=>{
    if (res.code == 200) {
      // setyedepat(res.data[0].department)
      // setyerole(res.data[0].role)
      console.log(res.data)
      setupdatedata(res.data)
      setsexadd(res.data.sex)
      setupdateImg("http://127.0.0.1:3001/"+res.data.picture)
    }
  })
}


//  添加
  const onFinish1 = async(values) => {
    let add = {
      name: values.name,
      phone: values.phone,
      position: values.position,
      zhanghao: values.zhanghao,
      sex: sexadd,
      department: depat,
      role: role[0],
      createdata: new Date(),
      picture: values.picture.file.response.file
    }
    
    request.post('/System_user/add', add).then(res => {
      if (res.code == 200) {
        message.open({
          type: 'success',
          content: '添加成功',
        });
        getdata()
        setOpenadd(false); //关闭抽屉
      }
    })
  };
  // 编辑
  const onFinish2 = async(values) => {
    
    console.log(values)
    let add = {
      id: updatedata._id,
      name: updatedata.name,
      phone: updatedata.phone,
      position: updatedata.position,
      zhanghao: updatedata.zhanghao,
      sex: updatedata.sex,
      department: updatedata.department,
      role: updatedata.role,
      createdata: new Date(),
      picture: values.picture.file.response.file
    }
    
    request.post('/System_user/updateonedata', add).then(res => {
      if (res.code == 200) {
        message.open({
          type: 'success',
          content: '编辑成功',
        });
        getdata()
        setOpenupdate(false); //关闭抽屉
      }
    })
  };
  const options1 = [
    {
      value: '生产部门',
      label: '生产部门',
      children: [
        {
          value: '产品部',
          label: '产品部',
        },
        {
          value: '销售部',
          label: '销售部',
        },
        {
          value: '研发部',
          label: '研发部',
        },
      ],
    },
    {
      value: '安防部门',
      label: '安防部门',
      children: [
        {
          value: '安保部',
          label: '安保部',
        },
        {
          value: '巡检部',
          label: '巡检部',
        },
        {
          value: '维修部',
          label: '维修部',
        },
      ],
    },
  ];
  const options2 = [
    {
      value: 'admin',
      label: 'admin',

    },
    {
      value: '管理员',
      label: '管理员',

    },
    {
      value: '安保角色',
      label: '安保角色',

    },
    {
      value: '维修角色',
      label: '维修角色',

    },
  ];
  const onChange1 = (value) => {
    console.log(value);
    setDepat(value)

  };
 const onChange1update=(value)=>{
    console.log(value);
    setupdatedata({...updatedata,department:value})
  }
const   onChange2update=(value)=>{
    console.log(value);
    setupdatedata({...updatedata,role:value[0]})
  }
  const onChangeyedepat=(value)=>{
    console.log(value);
    setyedepat(value)
  }
  const onChangeyerole=(value)=>{
    console.log(value);
    setyerole(value)
  }
  const onChange2 = (value) => {
    console.log(value[0]);
    setRole(value)
  };
  const resetfang = () => {
    setDepat("")
    setRole("")
    setValue("")
  }


  //添加抽屉
  const [openadd, setOpenadd] = React.useState(false);
  
  const showDraweradd = () => {
    setOpenadd(true);
  };
  const onCloseadd = () => {
    setOpenadd(false);
  };
  const onCloseupdate=()=>{
    setOpenupdate(false);
  }
// 修改抽屉
  const [openupdate, setOpenupdate] = React.useState(false);
// 获取数据
  const getdata = async () => {
    const res = await request.get("/System_user/list")
    setdata(res.data)
    console.log(res.data)
  }
  //分页
  const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  };
  const handleChange2 = (info) => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj, (url) => {
        setLoading(false);
        console.log(url)
        setimageUrl(url)
      });
    }
  }
  const handleChange2update = (info) => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj, (url) => {
        setLoading(false);
       setupdateImg(url)
        
        setupdatedata({...updatedata,picture:url})
      });
    }
  }
  const handlePageChange = (page, pageSize) => {
    setCurrent(page);
    setPageSize(pageSize);


  };




  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  const uploadButton = (
    <button
      style={{
        border: 0,
        background: 'none',
      }}
      type="button"
    >
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div
        style={{
          marginTop: 8,
        }}
      >
        上传111
      </div>
    </button>
  );
  let changeState = useCallback((i) => {
    setFlag(i)
  }, [flag1])

  useEffect(() => {
    getdata()
  }, [])




  return (
    <div style={{ width: "100%", height: "100%" }}>
      <p>Original Data: {dataToEncrypt}</p>
      <p>Encrypted Data: {encryptedData}</p>
      <Drawer title="添加用户" onClose={onCloseadd} rules={[{ required: true }]} open={openadd}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish1}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="用户姓名"
            name="name">
            <div>
              <Input />
            </div>
          </Form.Item>
          <Form.Item rules={[{ required: true }]} label="账号"
            name="zhanghao">
            <div>
              <Input />
            </div>
          </Form.Item>
          <Form.Item rules={[{ required: true }]} label="联系方式"
            name="phone">
            <div>
              <Input />
            </div>
          </Form.Item>
          <Form.Item rules={[{ required: false }]} label="性别"
            name="sex">
            <div>
            <Radio.Group defaultValue={true} buttonStyle="solid">
                <Radio.Button value={true} onChange={() => {
                          setsexadd(true)
                      }}>男</Radio.Button>
                      <Radio.Button value={false} onChange={() => {
                              setsexadd(false)
                        }}>女</Radio.Button>
                    </Radio.Group>
            </div>
          </Form.Item>
          <Form.Item rules={[{ required: true }]} label="职位"
            name="position">
            <div>
              <Input />
            </div>
          </Form.Item>
          <Form.Item  label="部门"
            name="department">
            <div>
          
          <Cascader  options={options1}  onChange={onChange1} value={depat} placeholder="Please select" />
      
            </div>
          </Form.Item>
          <Form.Item  label="角色"
            name="role">
            <div>
            <Cascader options={options2} onChange={onChange2} value={role} placeholder="Please select" />
            </div>
          </Form.Item>
          <Form.Item label="上传图片"
            name="picture"
            rules={[{ required: true, message: '请输入人脸图片' }]}>
            <Upload 
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="http://127.0.0.1:3001/System_user/upload"
              onChange={handleChange2}
              maxCount={1}
            >
              {imageUrl ? (
                <img
                  src={"http://127.0.0.1:3001/"+imageUrl}
                  alt="avatar"
                  style={{
                    width: '5rem',
                  }}
                />
              ) : (
                uploadButton
              )}
            </Upload>



          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
              添加
            </Button>

            <Button htmlType="reset" onClick={() => {
              setFlag(false)
            }}>
              重置
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Drawer title="编辑用户" onClose={onCloseupdate} rules={[{ required: true }]} open={openupdate}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish2}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item label="用户姓名"
            name="name">
            <div>
              <Input value={updatedata.name}  onChange={(e)=>{
                setupdatedata({...updatedata,name:e.target.value})
              }}/>
            </div>
          </Form.Item>
          <Form.Item label="账号"
            name="zhanghao">
            <div>
              <Input  onChange={(e)=>{
                setupdatedata({...updatedata,zhanghao:e.target.value})
              }} value={updatedata.zhanghao}/>
            </div>
          </Form.Item>
          <Form.Item  label="联系方式"
            name="phone">
            <div>
              <Input  onChange={(e)=>{
                setupdatedata({...updatedata,phone:e.target.value})
              }} value={updatedata.phone}/>
            </div>
          </Form.Item>
          <Form.Item  label="性别"
            name="sex">
            <div>
            <Radio.Group value={updatedata.sex} buttonStyle="solid">
                <Radio.Button value={true} onChange={(e) => {
                  setupdatedata({...updatedata,sex:true})
                      }}>男</Radio.Button>
                                <Radio.Button value={false} onChange={(e) => {
                  setupdatedata({...updatedata,sex:false})
                      }}>女</Radio.Button>
                            </Radio.Group>
            </div>
          </Form.Item>
          <Form.Item  label="职位"
            name="position">
            <div>
              <Input onChange={(e) => {
                setupdatedata({...updatedata,position:e.target.value})
                      }} value={updatedata.position} />
            </div>
          </Form.Item>
          <Form.Item  label="部门"
            name="department">
            <div>
        
          <Cascader   options={options1}  onChange={onChange1update} value={updatedata.department} placeholder="Please select" />
      
            </div>
          </Form.Item>
          <Form.Item  label="角色"
            name="role">
            <div>
            <Cascader options={options2} onChange={onChange2update} value={updatedata.role} placeholder="Please select" />
            </div>
          </Form.Item>
          <Form.Item label="上传图片"
            name="picture"
           >
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="http://127.0.0.1:3001/System_user/upload"
              onChange={handleChange2update}
              maxCount={1}
            >
              {updateImg ? (
                <img
                  src={updateImg}
                  alt="avatar"
                  style={{
                    width: '5rem',
                  }}
                />
              ) : (
                uploadButton
              )}
            </Upload>



          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
              修改
            </Button>

            <Button htmlType="reset" onClick={() => {
              setFlag(false)
            }}>
              重置
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      


  
      <div style={{ width: '95%', backgroundColor: 'rgb(255,255,255)', padding: '25px', marginBottom: '15px', boxSizing: 'border-box', display: 'flex', justifyContent: 'space-between', alignItems: 'center', borderRadius: '5px' }}>

        <p style={{ display: 'inline-block', width: '25%' }}><span style={{ paddingRight: '5px' }}>用户信息:</span><Input style={{ width: "70%" }} placeholder='请输入名称、账号、联系方式、职位' value={value} onChange={(e) => {
          setValue(e.target.value)
        }}></Input></p>
        <p style={{ display: 'inline-block', width: '20%' }}>部门:
          <Cascader options={options1} onChange={onChangeyedepat} value={yedepat} placeholder="Please select" />

        </p>
        <p style={{ display: 'inline-block', width: '20%' }}>角色:

          <Cascader options={options2} onChange={onChangeyerole} value={yerole} placeholder="Please select" />

        </p>
        <p style={{ display: 'inline-block', width: '20%' }}>

          <Button type="primary" style={{ height: '40px' }}>搜索</Button>
          <Button style={{ marginLeft: '20px', height: '40px' }} onClick={() => {
            resetfang()
          }}>重置</Button>
        </p>

      </div>
      <div style={{ width: '95%', backgroundColor: 'rgb(255,255,255)', padding: '25px', boxSizing: 'border-box', borderRadius: '5px' }}>
        <div>
          <Button type="primary" onClick={() => {
            showDraweradd()
          }}>新增用户</Button>
          <Button type="primary" style={{ marginLeft: '20px' }}>批量导入</Button>
        </div>
        <div>
          <Table columns={columns}
            pagination={{
              current,
              pageSize,
              onChange: handlePageChange,
          
              pageSizeOptions: ['5', '10', '15'],
            }}
            scroll={{
              y: 500
            }}
            dataSource={data.map((item) => {
              return {
                ...item,
                key: item._id
              }
            })
            }

          />
        </div>

























      </div>
    </div>
  )
}
