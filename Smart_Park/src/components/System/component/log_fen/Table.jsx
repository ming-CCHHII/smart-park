import React, { useEffect, useState } from 'react'
import { Table } from 'antd'
import axios from 'axios'
import dayjs from 'dayjs'
import request from '../../../../utils/request'
export default function table() {
 const columns =[
   {
     title:"序号",
     dataIndex:"xuid",
     key:"xuid"
   },
   {
    title:"日志类型",
    dataIndex:"type",
    key:"type"
  },
   {
    title: '日志详情',
    dataIndex: 'xiang',
    key: 'xiang',
    render: (text) => (text.map((item) => item).join('-'))
  },
   {
    title:"操作用户",
    dataIndex:"name",
    key:"name"
   },
   {
    title:"账号",
    dataIndex:"zhanghao",
    key:"zhanghao"
   },
   {
    title:"上报时间",
    dataIndex:"updatadate",
    key:"updatadate",
    render: (text) => <span style={{width:'50%'}}>{(dayjs(text).format('YYYY-MM-DD HH:mm:ss'))}</span> ,
   }
 ]
 const [data,setdata]=useState([])
 const getlist=async()=>{
  const {data}=await request.get('/System_user/log_list')
  setdata(data)
 }
 useEffect(()=>{
  getlist()
 })
  return (
    <div>
      <Table dataSource={data} columns={columns} />







    </div>
  )
}
