import React, { useEffect, useState } from 'react'
import {Button, Drawer,Form,Input,Table,Space,Cascader, Flex, Radio } from 'antd'
import dayjs from 'dayjs';
import request from "../../../../utils/request";

export default function Access() {
  const columns = [
    {
      title: '设备编号',
      dataIndex: 'bian',
      key: 'bian',
      width: '8%',
    },
    {
      title: '设备名称',
      dataIndex: 'name',
      key: 'name',
      width: '12%',
    },
    {
      title: '设备类型',
      dataIndex: 'type',
      key: 'type',
      width: '12%',
    },
    {
      title: '设备品牌',
      dataIndex: 'brand',
      key: 'brand',
      width: '12%',
    },
    {
      title: 'IP地址',
      dataIndex: 'address',
      key: 'address',
      width: '12%',
    },
    {
      title: '所在位置',
      dataIndex: 'position',
      key: 'position',
      width: '10%',
    },
    {
      title: '更新时间',
      dataIndex: 'updatadate',
      width: '20%',
      key: 'updatadate',
      render: (text) => <span style={{width:'50%'}}>{(dayjs(text).format('YYYY-MM-DD HH:mm:ss'))}</span> ,
    },
    {
      title: '状态',
      dataIndex: 'flag',
      key: 'flag',
      width: '12%',
      render: (text) => <span>
        <span style={{display:text==1?'block':'none',color:'blue'}}>在线</span>
        <span style={{display:text==0?'block':'none',color:'black'}}>离线</span>
        <span style={{display:text==2?'block':'none',color:'red'}}>故障</span>

        
      </span> ,
    },
    {
      title: '操作',
      key: 'action',
      width: '30%',
      render: (_, record) => (
        <Space size="middle" >
          <a style={{display:"inline-block",width:'40px'}} onClick={()=>{
            updatehui(record)
          }}>编辑</a>
          <a style={{display:"inline-block",width:'40px',color:"red"}} onClick={()=>{
            console.log(record)
            deleta(record)
          }}>删除</a>
        </Space>
      ),
    }
  ];
  const [xindata,setxindata]=useState([])
  const [value,setvalue]=useState("")
  const getlist=async()=>{
    const {data}=await request.get("/System_user/device_list")
    let  data1=data.filter((item)=>{
        return item.bian.slice(0,2)=="MJ"
      })
      setxindata(data1)
  }
  useEffect(()=>{
    getlist()
    option1gai()
  },[])
  const [showDraweradd, setshowDraweradd] = useState(false);
  const [showDrawerupdate, setshowDrawerupdate] = useState(false);

  const onCloseadd=()=>{
    setshowDraweradd(false)
  }
const  onCloseupdate=()=>{
    setshowDrawerupdate(false)
  }
  const deleta=async(value)=>{
    await request.post(`/System_user/device_delete?id=${value._id}`)
    getlist()
  }
  const updatehui=async(record)=>{
    let dd={
      id:record._id,
      name:record.name,
      type:record.type,
      brand:record.brand,
      address:record.address,
      position:record.position,
      flag:record.flag
    }
    setupdate(dd)
    setshowDrawerupdate(true)

  }
  const [update, setupdate]=useState([])
  const onFinish=async(value)=>{
  let add={
    updatadate:new Date(),
    name:value.name,
    type:value.type,
    brand:value.brand,
    address:value.address,
    position:value.position,
    flag:0,
    bb:"MJ"
  }
  console.log(add)
  await request.post("/System_user/device_add",add)
  setshowDraweradd(false)
  getlist()
  }
  const onFinishupdate=async(value)=>{
    let add={
      updatadate:new Date(),
      name:value.name,
      type:value.type,
      brand:value.brand,
      address:value.address,
      position:value.position,
      flag:update.flag
    }
  await request.post(`/System_user/device_update?id=${update.id}`,add)
      
          setshowDrawerupdate(false)
          getlist()
   
  }
  // const [options2, setOptions2] = React.useState([ ])
  const [flag,setflag]=useState("")
  const [type,settype]=useState("")
 const onChange1 = (value) => {
    settype(value);
  }
  const onChange2 = (value) => {
    setflag(value);
  }

  const option1gai=async()=>{
    const {data}=await request.get(`/System_user/device_top_list?type_id=门禁`)
    
    let oo=data.map((item)=>{
      return {
        value:item.name,
        label:item.name
      }
    })
    setoption1(oo)
  }
const [option1,setoption1]=useState([])
  const option2=[
    {
      value: '1',
      label: '在线',
    },
    {
      value: '0',
      label: '离线',
    },
    {
      value: '2',
      label: '故障',
    }
  ]




  const options = [
    { label: '在线', value: 1 },
    { label: '离线', value:0 },
    { label: '故障', value: 2 },
  ];
  const onchange1i=(value)=>{
    console.log(value)
   setupdate({...update,flag:value})
  }
  return (
    <div style={{ width: "100%", height: "92%" }}>
      <div style={{width:"95%",backgroundColor:"rgb(255,255,255)",padding:"25px",marginBottom:"15px",boxSizing:"border-box",borderRadius:"5px"}}>
      <p style={{width:"25%",display:"inline-block"}}>
        <span style={{ paddingRight: "5px" }}>设备信息:</span>
        <Input
          style={{ width: "70%" }}
          placeholder="请输入编号、名称、品牌等"
          value={value}
          onChange={(e) => {
            setvalue(e.target.value);
          }}
        ></Input>
      </p>
      <p style={{width:"25%",display:"inline-block"}}>
        <span style={{ paddingRight: "5px" }}>设备类型:</span>
        <Cascader
            options={option1}
            onChange={onChange1}
            value={type}
            placeholder="Please select"
          />
      </p>

      <p style={{width:"25%",display:"inline-block"}}>
        <span style={{ paddingRight: "5px" }}>状态:</span>
        <Cascader
            options={option2}
            onChange={onChange2}
            value={flag}
            placeholder="Please select"
          />
      </p>

      <p style={{width:"25%",display:"inline-block"}}>
        <Button type="primary">搜索</Button>
        <Button
          style={{ marginLeft: "20px" }}
          onClick={() => {
            resetfang();
          }}
        >
          重置
        </Button>
      </p>
      
    </div>


    <div style={{width:"95%",backgroundColor:"rgb(255,255,255)",padding:"25px",marginBottom:"15px",boxSizing:"border-box",borderRadius:"5px"}}>
    <Button type="primary" onClick={()=>{
          setshowDraweradd(true)
        }}>新增设备</Button>
          <div style={{float:'right',width:'500px',display:"flex"}}>
                 <span style={{margin:"0 20px",display:"flex",alignItems:"center"}}>设备总数:<span style={{fontSize:"23px"}}>{xindata.length}</span></span>
                 <span style={{margin:"0 20px",display:"flex",alignItems:"center"}}>在线设备:<span style={{fontSize:"23px",color:"rgb(64, 158, 255)"}}>{xindata.filter(item=>item.flag==1).length}</span></span>
                 <span style={{margin:"0 20px",display:"flex",alignItems:"center"}}>离线总数:<span style={{fontSize:"23px"}}>{xindata.filter(item=>item.flag==0).length}</span></span>
                 <span style={{margin:"0 20px",display:"flex",alignItems:"center"}}>故障总数:<span style={{fontSize:"23px",color:"red"}}>{xindata.filter(item=>item.flag==2).length}</span></span>
          </div>
         
      
    
      <div>
      <Drawer title="添加监控设备" onClose={onCloseadd} rules={[{ required: true }]} open={showDraweradd}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish}
       
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="设备名称"
            name="name"> 
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="设备类型"
            name="type">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="设备品牌"
            name="brand">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="IP地址"
            name="address">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="所在位置"
            name="position">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             确认
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              取消
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Drawer title="修改监控设备" onClose={onCloseupdate} rules={[{ required: true }]} open={showDrawerupdate}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinishupdate}
       
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="设备名称"
            name="name"> 
            <div>
              <Input value={update.name} onChange={(e)=>{
                setupdate({...update,name:e.target.value})
              }} />
            </div>
          </Form.Item>
          <Form.Item  label="设备类型"
            name="type">
            <div>
            <Input value={update.type} onChange={(e)=>{
                setupdate({...update,type:e.target.value})
              }} />
            </div>
          </Form.Item>
          <Form.Item  label="设备品牌"
            name="brand">
            <div>
            <Input value={update.brand} onChange={(e)=>{
                setupdate({...update,brand:e.target.value})
              }} />
            </div>
          </Form.Item>
          <Form.Item  label="IP地址"
            name="address">
            <div>
            <Input value={update.address} onChange={(e)=>{
                setupdate({...update,address:e.target.value})
              }} />
            </div>
          </Form.Item>
          <Form.Item  label="所在位置"
            name="position">
            <div>
            <Input value={update.position} onChange={(e)=>{
                setupdate({...update,position:e.target.value})
              }} />
            </div>
          </Form.Item>
          <Form.Item  label="状态"
            name="address">
            <div>
            <Radio.Group block options={options} value={update.flag} onChange={(e)=>{
              setupdate({...update,flag:e.target.value})
              console.log(e.target.value)
            }} defaultValue="1" />
            </div>
          </Form.Item>




          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             确认
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              取消
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Table style={{marginTop:"20px"}}
        columns={columns}
        dataSource={xindata}/>
      </div>
    </div>






    </div>
  )
}
