import React, { useEffect } from 'react'
import { Button, Table,Drawer,Space,Form,Input } from 'antd';
import axios from 'axios';
import request from '../../../../utils/request';
import dayjs from 'dayjs';
export default function table() {
  const columns = [
    {
      title: '区域名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '区域描述',
      dataIndex: 'address',
      key: 'address',
      width: '12%',
    },
    {
      title: '排序权重',
      dataIndex: 'weight',
      key: 'weight',
      width: '12%',
    },
    {
      title: '更新时间',
      dataIndex: 'updatadate',
      width: '30%',
      key: 'updatadate',
      render: (text) => <span style={{width:'50%'}}>{(dayjs(text).format('YYYY-MM-DD HH:mm:ss'))}</span> ,
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={()=>{
            setshowDrawer(true)
            setdepat(record.name)
            console.log(record.name)
          }}>新增子部门</a>
          <a onClick={()=>{
            updatehui(record)
          }}>编辑</a>
          <a style={{color:'red'}} onClick={()=>{
            console.log(record)
            deleta(record)
          }}>删除</a>
        </Space>
      ),
    }
  ];
  const updatehui=async(values)=>{
    setshowDrawer1(true)
    setupname(values.name)
    setupaddress(values.address)
    setupweight(values.weight)
    if(values.depat){
      setdepat(values.depat)
      console.log(depat)
      setdanid(values.danid)
    }
    setid(values._id)
    console.log(values)
  }
  const [xindata,setxindata]=React.useState([])
  const [showDrawer,setshowDrawer]=React.useState(false)
  const [showDrawer1,setshowDrawer1]=React.useState(false)
  const [upname,setupname]=React.useState("")
  const [upaddress,setupaddress]=React.useState("")
  const [upweight,setupweight]=React.useState("")
  const [danid,setdanid]=React.useState("")
  const [id,setid]=React.useState("")
  const getlist=async()=>{
    await request.get("/System_user/space_list").then(res=>{
      if(res.code===200){
        setxindata(res.data)
      }
    })
  }
  const [depat,setdepat]=React.useState("")
  const onCloseadd=()=>{
    setshowDrawer(false)
  }
  useEffect(()=>{
    getlist()
  },[])
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  }
  const onCloseadd1=()=>{
    setshowDrawer1(false)
  }
// 添加数据
const onFinish1=async(values)=>{
  console.log(depat)
  if(depat){
    let add={
      depat:depat,
      address:values.address,
      key:values.weight,
      name:values.name,
      weight:values.weight,
      updatadate:new Date(),
    }
   
    await request.post("/System_user/space_add2",add).then(res=>{
      if(res.code===200){
        
        getlist()
        setdepat("")
        setshowDrawer(false)
      }
    })
  }else{
    let add={
      address:values.address,
      key:values.weight,
      name:values.name,
      weight:values.weight,
      updatadate:new Date(),
      Children:[]
    }
    console.log(add)
    await request.post("/System_user/space_add1",add).then(res=>{
      if(res.code===200){
        getlist()
        setdepat("")
        setshowDrawer(false)
      }
    })
  }
}
 // 修改数据
 const onFinish2=async(values)=>{
  let add={
    id:id,
    danid:danid,
    depat:depat,
    address:values.address,
    key:values.weight,
    name:values.name,
    weight:values.weight,
    updatadate:new Date(),
  }
  console.log(add)
  await request.post("/System_user/space_update",add).then(res=>{
    if(res.code===200){
      getlist()
      setdepat("")
      setupname("")
      setupaddress("")
      setupweight("")
      setshowDrawer1(false)
    }
  })
}
// 删除数据
const deleta=async(record)=>{
  await request.post("/System_user/space_delete",record).then(res=>{
    if(res.code===200){
      getlist()
      setshowDrawer(false)
    }
  })
}



  return (
    <div>
      <Drawer title="添加区域" onClose={onCloseadd} rules={[{ required: true }]} open={showDrawer}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish1}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="区域名称"
            name="name"> 
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="区域描述"
            name="address">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="排序权重"
            name="weight">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             确认
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              取消
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Drawer title="编辑区域" onClose={onCloseadd1} rules={[{ required: true }]} open={showDrawer1}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish2}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="区域名称"
            name="name"> 
            <div>
              <Input value={upname} onChange={(e)=>{
                setupname(e.target.value)
              }}/>
            </div>
          </Form.Item>
          <Form.Item  label="区域描述"
            name="address">
            <div>
              <Input value={upaddress} onChange={(e)=>{
                setupaddress(e.target.value)
              }}/>
            </div>
          </Form.Item>
          <Form.Item  label="排序权重"
            name="weight">
            <div>
              <Input value={upweight} onChange={(e)=>{
                setupweight(e.target.value)
              }} />
            </div>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             确认
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              取消
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
        <Button type="primary"  onClick={()=>{
            setshowDrawer(true)
        }}>新增区域</Button>
        <Table
        columns={columns}
        dataSource={xindata}/>
        </div>
  )
}
