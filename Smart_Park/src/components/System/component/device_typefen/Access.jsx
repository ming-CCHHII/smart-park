import React, { useEffect, useState } from 'react'
import {Button, Drawer,Form,Input,Table,Space,Cascader,Switch} from 'antd'
import dayjs from 'dayjs';
import request from "../../../../utils/request";

export default function Access() {
  const columns = [
    {
      title: '分类名称',
      dataIndex: 'name',
      key: 'name',
      width: '20%',
    },
    {
      title: '分类描述',
      dataIndex: 'describe',
      key: 'describe',
      width: '20%',
    },
  
    {
      title: '更新时间',
      dataIndex: 'updatadate',
      width: '20%',
      key: 'updatadate',
      render: (text) => <span style={{width:'50%'}}>{(dayjs(text).format('YYYY-MM-DD HH:mm:ss'))}</span> ,
    },
    {
      title: '状态',
      dataIndex: 'flag',
      key: 'flag',
      width: '20%',
      render: (text) => <span>
        {text==1?"有效":"关闭"}
      </span> ,
    },
    {
      title: '操作',
      key: 'action',
      width: '30%',
      render: (_, record) => (
        <Space size="middle" >
          <a style={{display:"inline-block",width:'40px'}} onClick={()=>{
            updatehui(record)
          }}>编辑</a>
          <a style={{display:"inline-block",width:'40px',color:"red"}} onClick={()=>{
            console.log(record)
            deleta(record)
          }}>删除</a>
        </Space>
      ),
    }
  ];
  const [xindata,setxindata]=useState([])
  const [value,setvalue]=useState("")
  const getlist=async()=>{
    const {data}=await request.get("/System_user/device_type_list")
    let  data1=data.filter((item)=>{
        return item.type_id=="门禁"
      })
      setxindata(data1)
      console.log(data1)
  }
  useEffect(()=>{
    getlist()
  },[])
  const [showDraweradd, setshowDraweradd] = useState(false);
  const [showDrawerupdate, setshowDrawerupdate] = useState(false);

  const onCloseadd=()=>{
    setshowDraweradd(false)
  }
const  onCloseupdate=()=>{
    setshowDrawerupdate(false)
  }
  const deleta=async(value)=>{
    await request.post(`/System_user/device_type_delete?id=${value._id}`)
    getlist()
  }
  const updatehui=async(record)=>{
    let dd={
      id:record._id,
      name:record.name,
      type_id:record.type_id,
      flag:record.flag,
      describe:record.describe,
    }
    setupdate(dd)
    setshowDrawerupdate(true)

  }
  const [update, setupdate]=useState([])
  const onFinish=async(value)=>{
  let add={
    updatadate:new Date(),
    describe:value.describe,
    name:value.name,
    flag:flagadd,
    type_id:"门禁"
  }
  console.log(add)
  await request.post("/System_user/device_type_add",add)
  setshowDraweradd(false)
  getlist()
  }
  const onFinishupdate=async(value)=>{
    let add={
      updatadate:new Date(),
    describe:update.describe,
    name:update.name,
    flag:update.flag,
    type_id:"门禁"
    }
  await request.post(`/System_user/device_type_update?id=${update.id}`,add)
      
          setshowDrawerupdate(false)
          getlist()
   
  }
  // const [options2, setOptions2] = React.useState([ ])
  const [flag,setflag]=useState("")
  const [type,settype]=useState("")
 const onChange1 = (value) => {
    settype(value);
  }
  const onChange2 = (value) => {
    setflag(value);
  }
  const option1=[
    {
      value: '1',
      label: '球型',
    },
    {
      value: '0',
      label: '枪型',
    },
    {
      value: '2',
      label: '全景型',
    }
  ]
  const option2=[
    {
      value: '1',
      label: '有效',
    },
    {
      value: '0',
      label: '关闭',
    },
  
  ]
  const [flagadd,setflagadd]=useState(false)
  const onChange = (value) => {
    setflagadd(value);
  }
  const onChangeflag= (value) => {
    setupdate({...update,flag:value})
  }
  return (
    <div style={{ width: "100%", height: "92%" }}>
      <div style={{width:"95%",backgroundColor:"rgb(255,255,255)",padding:"25px",marginBottom:"15px",boxSizing:"border-box",borderRadius:"5px"}}>
      <p style={{width:"25%",display:"inline-block"}}>
        <span style={{ paddingRight: "5px" }}>分类名称:</span>
        <Input
          style={{ width: "70%" }}
          placeholder="请输入编号、名称、品牌等"
          value={value}
          onChange={(e) => {
            setvalue(e.target.value);
          }}
        ></Input>
      </p>
      <p style={{width:"25%",display:"inline-block"}}>
        <span style={{ paddingRight: "5px" }}>是否有效:</span>
        <Cascader
            options={option2}
            onChange={onChange2}
            value={flag}
            placeholder="Please select"
          />
      </p>

      <p style={{width:"25%",display:"inline-block"}}>
        <Button type="primary">搜索</Button>
        <Button
          style={{ marginLeft: "20px" }}
          onClick={() => {
            resetfang();
          }}
        >
          重置
        </Button>
      </p>
      
    </div>

        
    <div style={{width:"95%",backgroundColor:"rgb(255,255,255)",padding:"25px",marginBottom:"15px",boxSizing:"border-box",borderRadius:"5px"}}>
    <Button type="primary" onClick={()=>{
          setshowDraweradd(true)
        }}>新增分类</Button>
      
         
      
    
      <div>
      <Drawer title="添加监控设备" onClose={onCloseadd} rules={[{ required: true }]} open={showDraweradd}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish}
       
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="分类名称"
            name="name"> 
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="分类描述"
            name="describe">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item  label="是否有效"
            name="flagadd">
            <div>
            <Switch value={flagadd} onChange={onChange}  />
            </div>
          </Form.Item>






          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             确认
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              取消
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Drawer title="修改监控设备" onClose={onCloseupdate} rules={[{ required: true }]} open={showDrawerupdate}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinishupdate}
       
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="分类名称"
            name="name"> 
            <div>
              <Input value={update.name} onChange={(e)=>{
                setupdate({...update,name:e.target.value})
              }} />
            </div>
          </Form.Item>
          <Form.Item  label="分类描述"
            name="type">
            <div>
            <Input value={update.describe} onChange={(e)=>{
                setupdate({...update,describe:e.target.value})
              }} />
            </div>
          </Form.Item>
          <Form.Item  label="是否有效"
            name="flagadd">
            <div>
            <Switch value={update.flag} onChange={onChangeflag}  />
            </div>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             确认
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              取消
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Table style={{marginTop:"20px"}}
        columns={columns}
        dataSource={xindata}/>
      </div>
    </div>






    </div>
  )
}
