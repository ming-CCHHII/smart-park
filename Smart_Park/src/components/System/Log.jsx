import React,{useState } from 'react'
import style from "../../assets/System/space.module.css"
import Table from "../../components/System/component/log_fen/Table"
import Top from "../../components/System/component/log_fen/Top"
import useDebounce from "../../utils/fangdou";
export default function Log() {
  const [inputValue, setInputValue] = useState('');
  const debouncedValue = useDebounce(inputValue, 500); // 500ms防抖
  return (
    <div style={{ width: "100%", height: "100%" }}>
   <input
      type="text"
      value={inputValue}
      onChange={(e) => setInputValue(e.target.value)}
      onBlur={() => onChange(debouncedValue)} // 在失去焦点时调用防抖后的值
    />
       <div className={style.nei}>
       <Top/>
       </div>
       <div className={style.nei}>
       <Table/>
       </div>
       
   



    </div>
  )
}
