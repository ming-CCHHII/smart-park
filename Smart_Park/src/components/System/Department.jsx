import React, { Children, useEffect } from 'react'
import { Input, Form,Drawer,Typography, Cascader,Space, Switch, Table, Button } from 'antd';
import request from '../../utils/request';
import dayjs from 'dayjs';

export default function department() {

  const [departname, setdepartname] = React.useState("")
  const columns = [
    {
      title: '部门名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '排序权重',
      dataIndex: 'weight',
      key: 'weight',
      width: '12%',
    },
    {
      title: '更新时间',
      dataIndex: 'updatadate',
      width: '30%',
      key: 'updatadate',
      render: (text) => <span style={{width:'50%'}}>{(dayjs(text).format('YYYY-MM-DD HH:mm:ss'))}</span> ,
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={()=>{
            setshowDrawer(true)
            setdepat(record.name)
            console.log(record.name)
          }}>新增子部门</a>
          <a onClick={()=>{
            updatehui(record)
          }}>编辑</a>
          <a style={{color:'red'}} onClick={()=>{
            console.log(record)
            deleta(record)
          }}>删除</a>
        </Space>
      ),
    }
  ];
  
  
  // rowSelection objects indicates the need for row selection
  const [xindata,setxindata]=React.useState([])
  const [checkStrictly, setCheckStrictly] = React.useState(false);
  const [showDrawer,setshowDrawer]=React.useState(false)
  const [showDrawer1,setshowDrawer1]=React.useState(false)
  const [depat,setdepat]=React.useState("")
  const [name1,setname1]=React.useState("")
  const [danid,setdanid]=React.useState("")
  const [weight, setweight] = React.useState("")
  const [id,setid]=React.useState("")
  // 回显数据
  const updatehui=async(values)=>{
    setshowDrawer1(true)
    setname1(values.name)
    setweight(values.weight)
    if(values.depat){
      setdepat(values.depat)
      console.log(depat)
      setdanid(values.danid)
    }
    setid(values._id)
    console.log(values)
  }
  
  // 删除数据
  const deleta=async(values)=>{
      await request.post("/System_user/department_delete",values).then(res=>{
        getdata()
      })
  }
  // 获取数据
  const getdata=async()=>{
   await request.get("/System_user/department_list").then(res=>{
      setxindata(res.data)
    })
  }
  // 添加数据
  const onFinish1=async(values)=>{
    if(depat){
      let add={
        key:values.weight,
        name:values.name,
        weight:values.weight,
        updatadate:new Date(),
        depat:depat
      }
      console.log(add)
      await request.post("/System_user/department_add2",add).then(res=>{
        if(res.code===200){
          
          getdata()
          setdepat("")
          setshowDrawer(false)
        }
      })
    }else{
      let add={
        key:values.weight,
        name:values.name,
        weight:values.weight,
        updatadate:new Date(),
        Children:[]
      }
      await request.post("/System_user/department_add1",add).then(res=>{
        if(res.code===200){
          getdata()
          setdepat("")
          setshowDrawer(false)
        }
      })
    }
  }
  // 修改数据
  const onFinish2=async(values)=>{
    let add={
      danid:danid,
      id:id,
      key:values.weight,
      name:name1,
      weight:weight,
      updatadate:new Date(),
      depat:depat
    }
    console.log(add)
    await request.post("/System_user/department_update",add).then(res=>{
      if(res.code===200){
        getdata()
        setdepat("")
        setshowDrawer(false)
      }
    })
  }
  const onCloseadd=()=>{
    setshowDrawer(false)
  }
  const onCloseadd1=()=>{
    setshowDrawer1(false)
  }
  const  openadd=()=>{
    setshowDrawer(true)
  }
  useEffect(()=>{
    getdata()
  },[])
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  const onChange1=(value)=>{
    console.log(value[0])
    setdepat(value[0])
  }
  return (
    <div style={{ width: "100%", height: "100%" }}>
    
    <div style={{ width: '95%', backgroundColor: 'rgb(255,255,255)', padding: '25px', marginBottom: '15px', boxSizing: 'border-box',borderRadius: '5px' }}>

      
    <p style={{ display: 'inline-block', width: '25%' }}><span style={{ paddingRight: '5px' }}>部门名称:</span><Input style={{ width: "70%" }} placeholder='请输入' value={departname} onChange={(e) => {
          setdepartname(e.target.value)
        }}></Input></p>
        <Button type="primary" style={{ height: '40px' }}>搜索</Button>
        <Button style={{ marginLeft: '20px', height: '40px' }} onClick={() => {
          resetfang()
        }}>重置</Button>
    </div>
    <div style={{ width: '95%', backgroundColor: 'rgb(255,255,255)', padding: '25px', boxSizing: 'border-box', borderRadius: '5px' }}>
      <div>
        <Button type="primary"  onClick={()=>{
       openadd()
        }}>新增部门</Button>
    
      </div>
      <div>
   
      </div>

























    </div>
    <div style={{ width: '95%', backgroundColor: 'rgb(255,255,255)', padding: '25px', boxSizing: 'border-box', borderRadius: '5px' }}> 
    <Drawer title="添加部门" onClose={onCloseadd} rules={[{ required: true }]} open={showDrawer}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish1}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="部门名称"
            name="name">
            <div>
              <Input  />
            </div>
          </Form.Item>
          <Form.Item rules={[{ required: true }]} label="排序权重"
            name="weight">
            <div>
              <Input  />
            </div>
          </Form.Item>
        
          {/* <Form.Item  label="部门"
            name="department">
            <div>
  
          <Cascader  options={options}  onChange={onChange1} value={depat} placeholder="Please select" />
            </div>
          </Form.Item> */}
          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             添加
            </Button>

            <Button htmlType="reset" onClick={() => {
             
            }}>
              重置
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Drawer title="编辑部门" onClose={onCloseadd1} rules={[{ required: true }]} open={showDrawer1}>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          clearOnDestroy={true}
          onFinish={onFinish2}
          onFinishFailed={onFinishFailed}
          autoComplete="off">
          <Form.Item rules={[{ required: true }]} label="部门名称"
            name="name">
            <div>
              <Input  value={name1} onChange={(e)=>{
                setname1(e.target.value)
              }}/>
            </div>
          </Form.Item>
          <Form.Item rules={[{ required: true }]} label="排序权重"
            name="weight">
            <div>
              <Input value={weight} onChange={(e)=>{
                setweight(e.target.value)
              }} />
            </div>
          </Form.Item>
        
         
          <Form.Item>
            <Button type="primary" htmlType="submit"  style={{ margin: '0 1rem' }}>
             编辑
            </Button>

            <Button htmlType="reset" onClick={() => {
            }}>
              重置
            </Button>
          </Form.Item>

        </Form>
      </Drawer>
      <Table
        columns={columns}
dataSource={xindata}/>
    </div>
  </div>
  )
}
