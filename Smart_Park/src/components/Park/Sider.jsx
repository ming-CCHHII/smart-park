import React, { useState, useEffect, useContext } from 'react'
import style from '../../assets/css/side.module.css'
import { useDispatch, useSelector } from 'react-redux'
import request from '../../utils/request'
import { useNavigate } from 'react-router-dom'

export default function Sider() {
  const navigate = useNavigate()
  const num = JSON.parse(sessionStorage.getItem('num')) || 0
  const [menuls, setMenuls] = useState([])
  const mid = useSelector((store) => store.mid)
  const userModel = useSelector((store) => store.userModel)

  const getdata = async () => {
    let res = await request.get('/getmenu')
    setMenuls(res.data)
  }

  const setNum = (index) => {
    sessionStorage.setItem('num', JSON.stringify(index))
  }
  useEffect(() => {
    getdata()
  }, [])



  return (
    <div className={style.side}>
      <ul className={style.navTwo_ul}>
        {menuls.filter(item => item._id === mid).map(item => {
          return item.children.map((item, index) => {
            if (userModel.roleid.mids.indexOf(item._id) == -1) {
              return <li key={item._id} onClick={() => {
                setNum(index)
                navigate(item.path)
              }} className={num === index ? style.active1 : ''}>
                <img src={item.icon} alt="" />
                {item.mname}
              </li>
            }
          })

        })}
      </ul>

    </div>
  )
}
