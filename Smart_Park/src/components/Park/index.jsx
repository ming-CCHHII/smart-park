import React from 'react'
import Header from './Header'
import Sider from './Sider'
import style from '../../assets/css/index.module.css'
import { Outlet } from 'react-router-dom'

export default function index() {
  return (
    <div style={{ backgroundColor: "#f5f5f5" }}>
      <Header />
      <div style={{ width: "100%", display: "flex" }}>
        <div className={style.element} style={{ width: "15em", height: "calc(100vh - 4rem)" }}>
          <Sider />
        </div>
        <div style={{width: "calc(100% - 15em)", height: "calc(100vh - 4rem)", padding: "1.5% 0 0 1.5%", boxSizing: "border-box" }}>
          <Outlet></Outlet>
        </div>
      </div>
    </div>
  )
}