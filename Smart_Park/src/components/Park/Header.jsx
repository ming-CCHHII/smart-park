import React, { useEffect, useState} from 'react'
import style from '../../assets/css/nav.module.css'
import { Dropdown , Space} from "antd";
import { DownOutlined } from "@ant-design/icons";
import request from '../../utils/request'
import { getmid } from '../../store/module/data'
import {useDispatch, useSelector} from 'react-redux'
import { useNavigate } from 'react-router-dom'

const items = [
    {
        key: '1',
        label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
                1st menu item
            </a>
        ),
    },
    {
        key: '2',
        label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
                2nd menu item (disabled)
            </a>
        )
    },
];

export default function Header({children}) {
    const [navOne, setNavOne] = useState([])
    const nums = JSON.parse(sessionStorage.getItem('nums')) || 0
    const dispatch=useDispatch()
    const navigate = useNavigate()
    const uname=useSelector(store => store.userModel.uname)
    
    const getMenuOne = async()=>{
        let {data}=await request.get('/getmenuone')
        setNavOne(data)
    }

    const setNum = (index) => {
        sessionStorage.setItem('nums', JSON.stringify(index))    
        sessionStorage.setItem('num', JSON.stringify(0))    
      }

    useEffect(()=>{
        getMenuOne()
    },[])

    return (
        <div>  
            <div className={style.header}>
                <div className={style.header__logo}>
                    <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83/u217.png" />
                </div>
                <p className={style.header__title}>
                    AI智汇未来园区管理平台
                </p>
                <ul className={style.navOne_ul}>
                    {navOne.filter(item => !item.pid).map((item, index) => {
                        return (
                            <li className={nums == index ? style.active : ''} key={item._id} onClick={() => {
                                setNum(index)
                                navigate(item.path)
                                dispatch(getmid(item._id))
                            }}>
                                {item.mname}
                            </li>
                        )
                    })
                    }
                </ul>
                <p className={style.header__p} onClick={()=>{
                    navigate('/visualization')
                }}>
                    可视化大屏
                </p>
                <div className={style.header__user}>
                    <p className={style.header__imgp}>
                        <img src="https://img0.baidu.com/it/u=1056845909,3203874779&fm=253&fmt=auto&app=138&f=JPEG?w=300&h=300" alt="" />
                    </p>
                    <Dropdown menu={{ items }}>
                        <a onClick={(e) => e.preventDefault()}>
                            <Space style={{
                                color: '#fff',
                            }}>
                                {uname}
                                <DownOutlined />
                            </Space>
                        </a>
                    </Dropdown>
                </div>
            </div>
        </div>
    )
}
