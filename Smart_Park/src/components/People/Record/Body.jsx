import React, { useEffect, useState } from 'react'
import { Table } from 'antd';
import { useSelector } from 'react-redux';
import '../../../assets/People/index.css'
import axios from '../../../utils/request'
import dayjs from 'dayjs';
export default function Body() {
    const columns = [
        {
            title: '车牌号码',
            dataIndex: 'plate',
            key: 'plate',
        },
        {
            title: '车辆信息',
            dataIndex: 'message',
            key: 'message',
        },
        {
            title: '车辆用途',
            dataIndex: 'purpose',
            key: 'purpose',
        },
        {
            title: '司机姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '联系方式',
            dataIndex: 'phone',
            key: 'phone'
        },
        {
            title: '过闸设备',
            dataIndex: 'lockage',
            key: 'lockage',
        },
        {
            title: '进出方向',
            dataIndex: 'inOut',
            key: 'inOut',
            render: (text) => text ? '进场' : '出场'
        },
        {
            title: '车辆抓拍',
            dataIndex: 'img',
            key: 'img',
            render: (text) => <img src={text} style={{ width: '5rem', height: '4rem' }} alt="" />,
        },
        {
            title: '通行时间',
            dataIndex: 'date',
            key: 'date',
            render: (text) => dayjs(text).format('YYYY-MM-DD HH:mm:ss')
        }
    ];
    const [current, setCurrent] = useState(1);
    const [pageSize, setPageSize] = useState(5);
    const [data, setData] = useState([]);
    const record_message = useSelector(state => state.record_message);
    const record_driver = useSelector(state => state.record_driver);
    const lockage = useSelector(state => state.lockage);
    const record_inOut = useSelector(state => state.record_inOut);
    const recordStateTime = useSelector(state => state.recordStateTime);
    const recordEndTime = useSelector(state => state.recordEndTime);
    //分页
    const handlePageChange = (page, pageSize) => {
        setCurrent(page);
        setPageSize(pageSize);
    };
    //获取数据
    let getData = async () => {
        let { data } = await axios.get(`/people/record?record_message=${record_message}&record_driver=${record_driver}&
        lockage=${lockage}&record_inOut=${record_inOut}&recordStateTime=${recordStateTime}&recordEndTime=${recordEndTime}`)
        setData(data)
    }
    useEffect(() => {
        getData()
    }, [
        record_message,
        record_driver,
        lockage,
        record_inOut,
        recordStateTime,
        recordEndTime
    ])
    return (
        <div>
            <div className='body'>
                <div>
                    <Table columns={columns}
                        style={{ height: '100%' }}
                        pagination={{
                            current,
                            pageSize,
                            onChange: handlePageChange,
                            showSizeChanger: true,
                            pageSizeOptions: ['5', '10', '15'],
                        }}
                        scroll={{
                            y: 300
                        }}
                        dataSource={data.map((item) => {
                            return {
                                ...item,
                                key: item._id
                            }
                        })
                        } />
                </div>
            </div>
        </div>
    )
}
