import React, { useState } from 'react'
import { Button, Form, Input, Select, DatePicker, theme } from 'antd';
import { useDispatch } from 'react-redux';
import { record_setQuery, record_delQuery } from '../../../store/module/data'
import dayjs from 'dayjs';
export default function Header() {
  const [form] = Form.useForm();
  const { Option } = Select;
  const dispatch = useDispatch()
  const [record_message, setMessage] = useState('')
  const [record_driver, setDriver] = useState('')
  const [lockage, setLockage] = useState('')
  const [record_inOut, setInOut] = useState('')
  let [recordStateTime, setStateTime] = useState('')
  let [recordEndTime, setEndTime] = useState('')
  const handleChangeInOut = (value) => {
    setInOut(value)
  };
  const handleReset = () => {
    form.resetFields()
    dispatch(record_delQuery())
  }
  const { token } = theme.useToken();
  const style = {
    border: `1px solid ${token.colorPrimary}`,
    borderRadius: '50%',
  };
  const cellRender = (current, info) => {
    if (info.type !== 'date') {
      return info.originNode;
    }
    if (typeof current === 'number' || typeof current === 'string') {
      return <div className="ant-picker-cell-inner">{current}</div>;
    }
    return (
      <div className="ant-picker-cell-inner" style={current.date() === 1 ? style : {}}>
        {current.date()}
      </div>
    );
  }
  const onFinish = async (values) => {
    if (values.date) {
      setStateTime(dayjs(values.date[0]).format('YYYY-MM-DD'))
      setEndTime(dayjs(values.date[1]).format('YYYY-MM-DD'))
    }
    dispatch(record_setQuery({
      record_message: record_message,
      record_driver: record_driver,
      lockage: lockage,
      record_inOut: record_inOut,
      recordStateTime: recordStateTime,
      recordEndTime: recordEndTime
    }))
  };

  return (
    <div>
      <div className='level_header'>
        <Form
          form={form}
          onFinish={onFinish}
          layout='inline'
        >
          <Form.Item label="车辆信息" name='message' >
            <Input style={{ width: '15rem' }} placeholder="请输入车牌、信息、用途等" value={record_message} onChange={(e) => setMessage(e.target.value)} />
          </Form.Item>
          <Form.Item label="司机信息" name='type'>
            <Input style={{ width: '15rem' }} placeholder="请输入司机姓名、联系方式" value={record_driver} onChange={(e) => setDriver(e.target.value)} />
          </Form.Item>
          <Form.Item label="过闸设备" name='access' >
            <Input style={{ width: '15rem' }} placeholder="请输入编号、名称等" value={lockage} onChange={(e) => setLockage(e.target.value)} />
          </Form.Item>
          <Form.Item label="进出方向" name='inOut'>
            <Select
              placeholder="请输入"
              allowClear
              style={{ width: '12rem' }}
              onChange={handleChangeInOut}
            >
              <Option value="true">进场</Option>
              <Option value="false">出场</Option>
            </Select>
          </Form.Item>
          <Form.Item label="造访时间" name='date'>
            <DatePicker.RangePicker cellRender={cellRender} />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" style={{ marginRight: '1rem' }}>
              搜索
            </Button>
            <Button onClick={handleReset}>
              重置
            </Button>
          </Form.Item>
        </Form >
      </div >
    </div >
  )
}