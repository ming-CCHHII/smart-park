import React, { useEffect, useState } from 'react'
import { Button, Table, Drawer, message, Space } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import '../../../assets/People/index.css'
import axios from '../../../utils/request'
import dayjs from 'dayjs';
import Add from './Add';
import Edit from './Edit';
export default function Body() {
  const columns = [
    {
      title: '车辆号码',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '车辆信息',
      dataIndex: 'carType',
      key: 'carType',
    },
    {
      title: '车辆用途',
      dataIndex: 'purpose',
      key: 'purpose',
    },
    {
      title: '司机姓名',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '联系方式',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: '车辆照片',
      dataIndex: 'img',
      key: 'img',
      render: (text) => <img src={'http://127.0.0.1:3001/' + text} style={{ width: '4rem', height: '4rem' }} alt="" />,
    },
    {
      title: '创建时间',
      dataIndex: 'date',
      key: 'date',
      render: (text) => dayjs(text).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      title: '操作',
      dataIndex: 'todo',
      key: 'todo',
      render: (_, item) =>
        <Space size="middle">
          <a onClick={() => editCar(item)}>编辑</a>
          <a onClick={() => deleteCar(item._id)}>删除</a>
        </Space>
    }
  ];
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  const car = useSelector(store => store.car)
  const driver = useSelector(store => store.driver)
  let [list, setList] = useState([])
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  const onClose1 = () => {
    setOpen1(false);
  };
  //分页
  const handlePageChange = (page, pageSize) => {
    setCurrent(page);
    setPageSize(pageSize);
  };
  //获取数据
  let getData = async () => {
    let { data } = await axios.get(`/people/driver?car=${car}&driver=${driver}`)
    setData(data)
  }

  //删除
  const deleteCar = async (id) => {
    let res = await axios.post(`/people/del_driver?id=${id}`)
    if (res.code == 200) {
      message.open({
        type: 'success',
        content: '删除成功',
      });
      getData(); //重新获取数据
    }
  }
  // 修改
  const editCar = (item) => {
    setList(item)
    setOpen1(true)
  }
  useEffect(() => {
    getData()
  }, [car, driver, list])
  return (
    <div>
      <div className='body'>
        <div>
          <Button type='primary' onClick={showDrawer}>新增车辆</Button>
        </div>
        <Drawer title="新增车辆" onClose={onClose} open={open}>
          <Add setOpen={setOpen} getData={getData} />
        </Drawer>
        <Drawer title="车辆编辑" onClose={onClose1} open={open1}>
          <Edit setOpen={setOpen1} getData={getData} list={list} />
        </Drawer>
        <div>
          <Table columns={columns}
            style={{ height: '100%' }}
            pagination={{
              current,
              pageSize,
              onChange: handlePageChange,
              showSizeChanger: true,
              pageSizeOptions: ['5', '10', '15'],
            }}
            scroll={{
              y: 300
            }}
            dataSource={data.map((item) => {
              return {
                ...item,
                key: item._id
              }
            })
            } />
        </div>
      </div>
    </div>
  )
}
