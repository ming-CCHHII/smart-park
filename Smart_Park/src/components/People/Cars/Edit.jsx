import React, { useState, useCallback, useEffect } from 'react'
import { Button, Form, Input, Switch, message, Cascader, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import axios from '../../../utils/request'
export default function Add({ setOpen, getData, list }) {
    const options = [
        {
            value: 'A区',
            label: 'A区',
            children: [
                {
                    value: '1栋',
                    label: '1栋',
                },
                {
                    value: '2栋',
                    label: '2栋',
                },
                {
                    value: '3栋',
                    label: '3栋',
                },
            ],
        },
        {
            value: 'B区',
            label: 'B区',
            children: [
                {
                    value: '4栋',
                    label: '4栋',
                },
                {
                    value: '5栋',
                    label: '5栋',
                },

            ],
        },
        {
            value: 'C区',
            label: 'C区',
            children: [
                {
                    value: '6栋',
                    label: '6栋',
                },
            ],
        },
    ];
    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };
    const [imageUrl, setImageUrl] = useState('');
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const [plate, setPlate] = useState('');
    const [carType, setCarType] = useState('');
    const [purpose, setPurpose] = useState('');
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    //上传图片
    const handleChange = (info) => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, (url) => {
                setLoading(false);
                setImageUrl(url)
            });
        }
    }
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                上传照片
            </div>
        </button>
    );
    const onFinish = async (values) => {
        let edit = {
            plate: values.plate,
            carType: values.carType,
            purpose: values.purpose,
            name: values.name,
            phone: values.phone,
            img: values.img.file.response.file
        }
        await axios.post(`/people/edit_driver?id=${list._id}`, edit)
        setOpen(false)
        getData()
    }
    useEffect(() => {
        if (list != []) {
            form.setFieldsValue({
                plate: list.plate,
                carType: list.carType,
                purpose: list.purpose,
                name: list.name,
                phone: list.phone,
            })
            setImageUrl('http://127.0.0.1:3001/'+list.img)
        }
    }, [list])
    return (
        <div>
            <Form
                form={form}
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                clearOnDestroy={true}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="车牌号码"
                    name="plate"
                    rules={[{ required: true, message: '请输入人员姓名' }]}
                >
                    <Input value={plate} onChange={e => setPlate(e.target.value)}/>
                </Form.Item>

                <Form.Item
                    label="车辆信息"
                    name="carType"
                    rules={[{ required: true, message: '请输入职位' }]}
                >
                    <Input value={carType} onChange={e => setCarType(e.target.value)}/>
                </Form.Item>
                <Form.Item
                    label="车辆用途"
                    name="purpose"
                    rules={[{ required: true, message: '请输入部门/公司' }]}
                >
                    <Input value={purpose} onChange={e => setPurpose(e.target.value)}/>
                </Form.Item>
                <Form.Item
                    label="司机名称"
                    name="name"
                    rules={[{ required: true, message: '请选择状态' }]}
                >
                    <Input value={name} onChange={e => setName(e.target.value)}/>
                </Form.Item>
                <Form.Item
                    label="联系方式"
                    name="phone"
                    rules={[{ required: true, message: '请输入联系方式' }]}
                >
                    <Input value={phone} onChange={e => setPhone(e.target.value)}/>
                </Form.Item>
                <Form.Item
                    label="车辆照片"
                    name="img"
                    rules={[{ required: true, message: '请选择人脸照片' }]}
                >
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        action="http://127.0.0.1:3001/people/upload"
                        onChange={handleChange}
                        maxCount={1}
                    >
                        {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                style={{
                                    width: '5rem',
                                }}
                            />
                        ) : (
                            uploadButton
                        )}
                    </Upload>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
                        提交
                    </Button>
                    <Button htmlType="reset">
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}

