import React, { useState } from 'react'
import { Button, Form, Input, message, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import axios from '../../../utils/request'
export default function Add({ setOpen, getData }) {
    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };
    const [imageUrl, setImageUrl] = useState('');
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    //上传图片
    const handleChange = (info) => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, (url) => {
                setLoading(false);
                setImageUrl(url)
            });
        }
    }
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                上传照片
            </div>
        </button>
    );
    //添加
    const onFinish = (values) => {
        let add = {
            plate: values.plate,
            carType: values.carType,
            purpose: values.purpose,
            name: values.name,
            phone: values.phone,
            img: values.img.file.response.file
        }
        axios.post('/people/add_driver', add).then(res => {
            if (res.code == 200) {
                message.open({
                    type: 'success',
                    content: '添加成功',
                });
                getData();
                setOpen(false);
            }
        })
    };
    return (
        <div>
            <Form
                form={form}
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                clearOnDestroy={true}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="车牌号码"
                    name="plate"
                    rules={[{ required: true, message: '请输入人员姓名' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="车辆信息"
                    name="carType"
                    rules={[{ required: true, message: '请输入职位' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="车辆用途"
                    name="purpose"
                    rules={[{ required: true, message: '请输入部门/公司' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="司机名称"
                    name="name"
                    rules={[{ required: true, message: '请选择状态' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="联系方式"
                    name="phone"
                    rules={[{ required: true, message: '请输入联系方式' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="车辆照片"
                    name="img"
                    rules={[{ required: true, message: '请选择人脸照片' }]}
                >
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        action="http://127.0.0.1:3001/people/upload"
                        onChange={handleChange}
                        maxCount={1}
                    >
                        {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                style={{
                                    width: '5rem',
                                }}
                            />
                        ) : (
                            uploadButton
                        )}
                    </Upload>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
                        提交
                    </Button>
                    <Button htmlType="reset">
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
