import React, { useState } from 'react'
import { Button, Form, Input } from 'antd';
import { useDispatch } from 'react-redux';
import { car_setQuery, car_delQuery } from '../../../store/module/data'

export default function Header() {
    const [form] = Form.useForm();
    const dispatch = useDispatch()
    const [car, setCar] = useState('')
    const [driver, setDriver] = useState('')
    const handleReset = () => {
        form.resetFields()
        dispatch(car_delQuery())
    }
    const onFinish = async (values) => {
        dispatch(car_setQuery({
            car:car,
            driver:driver
        }))
    };

    return (
        <div>
            <div className='level_header'>
                <Form
                    form={form}
                    onFinish={onFinish}
                    layout='inline'
                >
                    <Form.Item label="车辆信息" name='message' >
                        <Input style={{ width: '15rem' }} placeholder="请输入车牌、信息、用途等" value={car} onChange={(e) => setCar(e.target.value)} />
                    </Form.Item>
                    <Form.Item label="司机信息" name='type'>
                        <Input style={{ width: '12rem' }} placeholder="请输入" value={driver} onChange={(e) => setDriver(e.target.value)} />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" style={{ marginRight: '1rem' }}>
                            搜索
                        </Button>
                        <Button onClick={handleReset}>
                            重置
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}