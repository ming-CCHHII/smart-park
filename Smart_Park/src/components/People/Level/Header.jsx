import React, { useState } from 'react'
import { Button, Form, Input, Select } from 'antd';
import { useDispatch } from 'react-redux';
import { level_setQuery,level_delQuery } from '../../../store/module/data'

export default function Header() {
  const [form] = Form.useForm();
  const { Option } = Select;
  let [state, setState] = useState('')
  const dispatch = useDispatch()
  const [level, setLevel] = useState("")
  const handleChangeState = (value) => {
    setState(value)
    console.log(value);
  };
  const handleReset = () => {
    form.resetFields()
    dispatch(level_delQuery())
  }
  const onFinish = async (values) => {
    dispatch(level_setQuery({ level:level,state:state }))
  };

  return (
    <div>
      <div className='level_header'>
        <Form
          form={form}
          onFinish={onFinish}
          layout='inline'
        >
          <Form.Item label="访客等级" name='level' >
            <Input style={{ width: '12rem' }} placeholder="请输入" value={level} onChange={(e) => {
              setLevel(e.target.value)
            }} />
          </Form.Item>
          <Form.Item label="状态" name='state'>
            <Select
              placeholder="请输入"
              allowClear
              style={{ width: '12rem' }}
              onChange={handleChangeState}
            >
              <Option value="true">启用</Option>
              <Option value="false">关闭</Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" style={{ marginRight: '1rem' }}>
              搜索
            </Button>
            <Button onClick={handleReset}>
              重置
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}