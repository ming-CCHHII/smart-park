import React, { useState, useEffect } from 'react'
import { Button, Form, Input, Switch } from 'antd';
import axios from '../../../utils/request'
export default function edit({ item, setOpen,getData }) {
    const [form] = Form.useForm();
    let [name, setName] = useState('')
    let [describe, setDescribe] = useState('')
    let [state, setState] = useState(false)
    const onChange = (checked) => {
        setState(checked)
    }
    const onFinish2 = async (values) => {
        await axios.post(`/people/edit?_id=${item._id}`, values)
        setOpen(false)
        getData()
    };
    useEffect(() => {
        if (item != []) {
            form.setFieldsValue({
                level: item.level,
                describe: item.describe,
                state: item.state
            })
        }
    }, [item])
    return (
        <div>
            <Form
                form={form}
                name='editForm'
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                clearOnDestroy={true}
                onFinish={onFinish2}
            >
                <Form.Item
                    label="访客等级"
                    name="level"
                    rules={[{ required: true, message: '请输入访客等级' }]}
                >
                    <Input value={name} onChange={(e) => {
                        setName(e.target.value)
                    }} />
                </Form.Item>
                <Form.Item
                    label="等级说明"
                    name="describe"
                    rules={[{ required: true, message: '请输入等级说明' }]}
                >
                    <Input value={describe} onChange={(e) => {
                        setDescribe(e.target.value)
                    }} />
                </Form.Item>
                <Form.Item
                    label="状态"
                    name="state"
                    rules={[{ required: true, message: '请选择状态' }]}
                >
                    <Switch onChange={onChange}/>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
                        提交
                    </Button>
                    <Button htmlType="reset">
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
