import React, { useCallback, useEffect, useState } from 'react'
import { Button, Form, Input, Table, Drawer, Switch, message, Space } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import '../../../assets/People/index.css'
import axios from '../../../utils/request'
import dayjs from 'dayjs';
import Edit from './edit'
export default function Body() {
  const columns = [
    {
      title: '访客等级',
      dataIndex: 'level',
      key: 'level',
    },
    {
      title: '等级说明',
      dataIndex: 'describe',
      key: 'describe',
    },
    {
      title: '创建时间',
      dataIndex: 'date',
      key: 'date',
      render: (text) => dayjs(text).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      title: '状态',
      dataIndex: 'state',
      key: 'state',
      render: (text) => (text ? '启用' : '关闭')
    },
    {
      title: '操作',
      dataIndex: 'todo',
      key: 'todo',
      render: (_, item) =>
        <Space size="middle">
          <a onClick={() => editLevel(item)}>编辑</a>
          <a onClick={() => deleteLevel(item._id)}>删除</a>
        </Space>
    }
  ];
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  let [state1, setState1] = useState(null)
  const level = useSelector(store => store.level)
  const state = useSelector(store => store.state)
  const [form] = Form.useForm();
  let [list, setList] = useState([])
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  const onClose1 = () => {
    setOpen1(false);
  };
  const onChange =(checked)=> {
    setState1(checked)
  }
  //添加
  const onFinish1 = (values) => {
    axios.post('/people/add_level', values).then(res => {
      if (res.code == 200) {
        message.open({
          type: 'success',
          content: '添加成功',
        });
        setOpen(false); //关闭抽屉
        getData() //重新获取数据
      }
    })
  };
  //分页
  const handlePageChange = (page, pageSize) => {
    setCurrent(page);
    setPageSize(pageSize);
  };
  //获取数据
  let getData = async () => {
    let { data } = await axios.get(`/people/level?level=${level}&state=${state}`)
    setData(data)
  }
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  //删除
  const deleteLevel = async (id) => {
    let res = await axios.post(`/people/del_level?id=${id}`)
    if (res.code == 200) {
      message.open({
        type: 'success',
        content: '删除成功',
      });
      getData(); //重新获取数据
    }
  }
  // 修改
  const editLevel = (item) => {
    setList(item)
    setOpen1(true)
  }
  useEffect(() => {
    getData()
  }, [level, state,list])
  return (
    <div>
      <div className='body'>
        <div>
          <Button type='primary' onClick={showDrawer}>新增等级</Button>
        </div>
        <Drawer title="新增等级" onClose={onClose} open={open}>
          <Form
            form={form}
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            clearOnDestroy={true}
            onFinish={onFinish1}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="访客等级"
              name="level"
              rules={[{ required: true, message: '请输入访客等级' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="等级说明"
              name="describe"
              rules={[{ required: true, message: '请输入等级说明' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="状态"
              name="state"
              rules={[{ required: true, message: '请选择状态' }]}
            >
              <Switch onChange={onChange} />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
                提交
              </Button>
              <Button htmlType="reset" onClick={() => {
                setFlag(false)
              }}>
                重置
              </Button>
            </Form.Item>
          </Form>
        </Drawer>
        <Drawer title="编辑等级" onClose={onClose1} open={open1}>
          <Edit item={list} getData={getData} setOpen={setOpen1}></Edit>
        </Drawer>
        <div>
          <Table columns={columns}
            style={{ height: '100%' }}
            pagination={{
              current,
              pageSize,
              onChange: handlePageChange,
              showSizeChanger: true,
              pageSizeOptions: ['5', '10', '15'],
            }}
            scroll={{
              y: 300
            }}
            dataSource={data.map((item) => {
              return {
                ...item,
                key: item._id
              }
            })
            } />
        </div>
      </div>
    </div>
  )
}
