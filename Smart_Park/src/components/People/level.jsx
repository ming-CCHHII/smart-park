import React from 'react'
import Header from './Level/Header'
import Body from './Level/Body'
export default function level() {
  return (
    <div>
      <Header />
      <Body />
    </div>
  )
}
