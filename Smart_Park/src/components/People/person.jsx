import React from 'react'
import Header from './Person/Header'
import Body from './Person/Body'
export default function person() {
  return (
    <div>
      <Header />
      <Body />
    </div>
  )
}
