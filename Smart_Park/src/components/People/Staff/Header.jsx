import React, { useState } from 'react'
import { Button, Form, Input, Select } from 'antd';
import { useDispatch } from 'react-redux';
import { staff_setQuery,staff_delQuery } from '../../../store/module/data'

export default function Header() {
  const [form] = Form.useForm();
  const { Option } = Select;
  const dispatch = useDispatch()
  const [type, setType] = useState('')
  const [staff_message, setMessage] = useState('')
  const handleChangeState = (value) => {
    setType(value)
  };
  const handleReset = () => {
    form.resetFields()
    dispatch(staff_delQuery())
  }
  const onFinish = async (values) => {
    dispatch(staff_setQuery({
      type: type,
      staff_message: staff_message
    }))
  };

  return (
    <div>
      <div className='level_header'>
        <Form
          form={form}
          onFinish={onFinish}
          layout='inline'
        >
          <Form.Item label="人员信息" name='message' >
            <Input style={{ width: '15rem' }} placeholder="请输入姓名、手机、职务等" value={staff_message} onChange={(e) => setMessage(e.target.value)} />
          </Form.Item>
          <Form.Item label="人员类型" name='type'>
            <Select
              placeholder="请输入"
              allowClear
              style={{ width: '12rem' }}
              onChange={handleChangeState}
            >
              <Option value="true">内部员工</Option>
              <Option value="false">非内部员工</Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" style={{ marginRight: '1rem' }}>
              搜索
            </Button>
            <Button onClick={handleReset}>
              重置
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}