import React, { useState, useCallback, useEffect } from 'react'
import { Button, Form, Input, Switch, message, Cascader, Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import axios from '../../../utils/request'
export default function Add({ setOpen, getData, list }) {
    const options = [
        {
            value: 'A区',
            label: 'A区',
            children: [
                {
                    value: '1栋',
                    label: '1栋',
                },
                {
                    value: '2栋',
                    label: '2栋',
                },
                {
                    value: '3栋',
                    label: '3栋',
                },
            ],
        },
        {
            value: 'B区',
            label: 'B区',
            children: [
                {
                    value: '4栋',
                    label: '4栋',
                },
                {
                    value: '5栋',
                    label: '5栋',
                },

            ],
        },
        {
            value: 'C区',
            label: 'C区',
            children: [
                {
                    value: '6栋',
                    label: '6栋',
                },
            ],
        },
    ];
    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };
    const [imageUrl, setImageUrl] = useState('');
    const [loading, setLoading] = useState(false);
    const [flag, setFlag] = useState(true)
    const [form] = Form.useForm();
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [position, setPosition] = useState('')
    const [company, setCompany] = useState('')
    const [address, setAddress] = useState([])
    const [type, setType] = useState(null)
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const onChange = (value) => {
        setAddress(value)
    };
    const onChange1 = (checked) => {
        setType(checked)
    }
    //上传图片
    const handleChange = (info) => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, (url) => {
                setLoading(false);
                setImageUrl(url)
            });
        }
    }
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                上传照片
            </div>
        </button>
    );
    const onFinish = async (values) => {
        let edit = {
            name: values.name,
            phone: values.phone,
            position: values.position,
            company: values.company,
            type: values.type,
            address: values.address,
            face: values.face.file.response.file
        }
        await axios.post(`/people/edit_staff?id=${list._id}`, edit)
        setOpen(false)
        getData()
    }
    useEffect(() => {
        if (list != []) {
            form.setFieldsValue({
                name: list.name,
                phone: list.phone,
                address: list.address,
                company: list.company,
                position: list.position,
                type: list.type,
            })
            setImageUrl('http://127.0.0.1:3001/'+list.face)
        }
    }, [list])
    return (
        <div>
            <Form
                form={form}
                name="basic"
                labelCol={{
                    span: 10,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                clearOnDestroy={true}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="人员姓名"
                    name="name"
                    rules={[{ required: true, message: '请输入人员姓名' }]}
                >
                    <Input value={name} onChange={(e) => setName(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="联系方式"
                    name="phone"
                    rules={[{ required: true, message: '请输入联系方式' }]}
                >
                    <Input value={phone} onChange={(e) => setPhone(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="职位"
                    name="position"
                    rules={[{ required: true, message: '请输入职位' }]}
                >
                    <Input value={position} onChange={(e) => setPosition(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="部门/公司"
                    name="company"
                    rules={[{ required: true, message: '请输入部门/公司' }]}
                >
                    <Input value={company} onChange={(e) => setCompany(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="是否为内部人员"
                    name="type"
                    rules={[{ required: true, message: '请选择是否是内部人员' }]}
                >
                    <Switch onChange={onChange1} />
                </Form.Item>
                <Form.Item
                    label="权限区域"
                    name="address"
                    rules={[{ required: true, message: '请输入部门/公司' }]}
                >
                    <Cascader options={options} onChange={onChange} style={{ width: '12rem' }} placeholder="请选择" />
                </Form.Item>
                <Form.Item
                    label="人脸照片"
                    name="face"
                    rules={[{ required: true, message: '请选择人脸照片' }]}
                >
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        action="http://127.0.0.1:3001/people/upload"
                        onChange={handleChange}
                        maxCount={1}
                    >
                        {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                style={{
                                    width: '5rem',
                                }}
                            />
                        ) : (
                            uploadButton
                        )}
                    </Upload>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
                        提交
                    </Button>
                    <Button htmlType="reset" onClick={() => {
                        setFlag(false)
                        setImageUrl('')
                    }}>
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
