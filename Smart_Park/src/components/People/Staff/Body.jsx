import React, { useEffect, useState } from 'react'
import { Button, Table, Drawer, message, Space } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import '../../../assets/People/index.css'
import axios from '../../../utils/request'
import dayjs from 'dayjs';
import Add from './Add';
import Edit from './Edit';
export default function Body() {
  const columns = [
    {
      title: '人员姓名',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '联系方式',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: '职位',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: '公司名称',
      dataIndex: 'company',
      key: 'company',
    },
    {
      title: '人员类型',
      dataIndex: 'type',
      key: 'type',
      render:(text)=>text?'内部员工':'非内部员工'
    },
    {
      title: '权限区域',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: '人脸照片',
      dataIndex: 'face',
      key: 'face',
      render: (text) => <img src={'http://127.0.0.1:3001/' + text} style={{ width: '4rem', height: '4rem' }} alt="" />,
    },
    {
      title: '创建时间',
      dataIndex: 'date',
      key: 'date',
      render: (text) => dayjs(text).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      title: '操作',
      dataIndex: 'todo',
      key: 'todo',
      render: (_, item) =>
        <Space size="middle">
          <a onClick={() => editStaff(item)}>编辑</a>
          <a onClick={() => deleteStaff(item._id)}>删除</a>
        </Space>
    }
  ];
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  const staff_message = useSelector(store => store.staff_message)
  const type = useSelector(store => store.type)
  let [list, setList] = useState([])
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  const onClose1 = () => {
    setOpen1(false);
  };
  //分页
  const handlePageChange = (page, pageSize) => {
    setCurrent(page);
    setPageSize(pageSize);
  };
  //获取数据
  let getData = async () => {
    let { data } = await axios.get(`/people/staff?message=${staff_message}&type=${type}`)
    setData(data)
  }

  //删除
  const deleteStaff = async (id) => {
    let res = await axios.post(`/people/del_staff?id=${id}`)
    if (res.code == 200) {
      message.open({
        type: 'success',
        content: '删除成功',
      });
      getData(); //重新获取数据
    }
  }
  // 修改
  const editStaff = (item) => {
    setList(item)
    setOpen1(true)
  }
  useEffect(() => {
    getData()
  }, [staff_message, type,list])
  return (
    <div>
      <div className='body'>
        <div>
          <Button type='primary' onClick={showDrawer}>新增人员</Button>    
        </div>
        <Drawer title="新增人员" onClose={onClose} open={open}>
          <Add setOpen={setOpen} getData={getData}/>
        </Drawer>
        <Drawer title="编辑人员" onClose={onClose1} open={open1}>
          <Edit setOpen={setOpen1} getData={getData} list={list}/>
        </Drawer>
        <div>
          <Table columns={columns}
            style={{ height: '100%' }}
            pagination={{
              current,
              pageSize,
              onChange: handlePageChange,
              showSizeChanger: true,
              pageSizeOptions: ['5', '10', '15'],
            }}
            scroll={{
              y: 300
            }}
            dataSource={data.map((item) => {
              return {
                ...item,
                key: item._id
              }
            })
            } />
        </div>
      </div>
    </div>
  )
}
