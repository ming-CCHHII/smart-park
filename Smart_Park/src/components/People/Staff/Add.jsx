import React, { useState, useCallback } from 'react'
import { Button, Form, Input, Radio, message, Cascader,Upload } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import axios from '../../../utils/request'
export default function Add({ setOpen, getData }) {
    const options = [
        {
            value: 'A区',
            label: 'A区',
            children: [
                {
                    value: '1栋',
                    label: '1栋',
                },
                {
                    value: '2栋',
                    label: '2栋',
                },
                {
                    value: '3栋',
                    label: '3栋',
                },
            ],
        },
        {
            value: 'B区',
            label: 'B区',
            children: [
                {
                    value: '4栋',
                    label: '4栋',
                },
                {
                    value: '5栋',
                    label: '5栋',
                },

            ],
        },
        {
            value: 'C区',
            label: 'C区',
            children: [
                {
                    value: '6栋',
                    label: '6栋',
                },
            ],
        },
    ];
    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };
    const [imageUrl, setImageUrl] = useState('');
    const [loading, setLoading] = useState(false);
    const [flag, setFlag] = useState(true)
    const [form] = Form.useForm();
    let changeState = useCallback((i) => {
        setFlag(i)
    }, [flag])
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const onChange = (value) => {
        console.log(value);
    };
    //上传图片
    const handleChange = (info) => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, (url) => {
                setLoading(false);
                // console.log(url)
                setImageUrl(url)
            });
        }
    }
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                上传照片
            </div>
        </button>
    );
    //添加
    const onFinish = (values) => {
        let add = {
            name: values.name,
            phone: values.phone,
            position: values.position,
            company: values.company,
            type: values.type,
            address: values.address,
            face: values.face.file.response.file
        }
        axios.post('/people/add_staff', add).then(res => {
            if (res.code == 200) {
                message.open({
                    type: 'success',
                    content: '添加成功',
                });
                getData() //重新获取数据
                setOpen(false); //关闭抽屉
            }
        })
    };
    return (
        <div>
            <Form
                form={form}
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
                clearOnDestroy={true}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="人员姓名"
                    name="name"
                    rules={[{ required: true, message: '请输入人员姓名' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="联系方式"
                    name="phone"
                    rules={[{ required: true, message: '请输入联系方式' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="职位"
                    name="position"
                    rules={[{ required: true, message: '请输入职位' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="部门/公司"
                    name="company"
                    rules={[{ required: true, message: '请输入部门/公司' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="人员类型"
                    name="type"
                    rules={[{ required: true, message: '请选择状态' }]}
                >
                    <Radio.Group defaultValue='true' buttonStyle="solid">
                        <Radio.Button value="true" onChange={() => {
                            changeState(false)
                        }}>内部员工</Radio.Button>
                        <Radio.Button value="false" onChange={() => {
                            changeState(true)
                        }}>非内部员工</Radio.Button>
                    </Radio.Group>
                </Form.Item>
                <Form.Item
                    label="权限区域"
                    name="address"
                    rules={[{ required: true, message: '请输入部门/公司' }]}
                >
                    <Cascader options={options} onChange={onChange} style={{ width: '12rem' }} placeholder="请选择" />
                </Form.Item>
                <Form.Item
                    label="人脸照片"
                    name="face"
                    rules={[{ required: true, message: '请选择人脸照片' }]}
                >
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        action="http://127.0.0.1:3001/people/upload"
                        onChange={handleChange}
                        maxCount={1}
                    >
                        {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                style={{
                                    width: '5rem',
                                }}
                            />
                        ) : (
                            uploadButton
                        )}
                    </Upload>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
                        提交
                    </Button>
                    <Button htmlType="reset" onClick={() => {
                        setFlag(false)
                    }}>
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
