import React, { useState } from 'react'
import { Button, Form, Input, Select } from 'antd';
import { useDispatch } from 'react-redux';
import { blackList_setQuery,blackList_delQuery } from '../../../store/module/data'

export default function Header() {
  const [form] = Form.useForm();
  const dispatch = useDispatch()
  const [blackList_message, setMessage] = useState('')
  const handleReset = () => {
    form.resetFields()
    dispatch(blackList_delQuery())
  }
  const onFinish = async (values) => {
    dispatch(blackList_setQuery({
      blackList_message: blackList_message
    }))
  };

  return (
    <div>
      <div className='level_header'>
        <Form
          form={form}
          onFinish={onFinish}
          layout='inline'
        >
          <Form.Item label="人员信息" name='message' >
            <Input style={{ width: '18rem' }} placeholder="请输入姓名、联系方式、职位、公司" value={blackList_message} onChange={(e) => setMessage(e.target.value)} />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" style={{ marginRight: '1rem' }}>
              搜索
            </Button>
            <Button onClick={handleReset}>
              重置
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}
