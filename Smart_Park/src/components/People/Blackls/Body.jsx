import React, { useEffect, useState } from 'react'
import { Button, Table, Drawer, message, Space } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import '../../../assets/People/index.css'
import axios from '../../../utils/request'
import dayjs from 'dayjs';
import Edit from './Edit'
export default function Body() {
  const columns = [
    {
      title: '姓名',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '联系方式',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: '职位',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: '公司名称',
      dataIndex: 'company',
      key: 'company',
    },
    {
      title: '拉黑原因',
      dataIndex: 'block',
      key: 'block',
    },
    {
      title: '人脸照片',
      dataIndex: 'face',
      key: 'face',
      render: (text) => <img src={'http://127.0.0.1:3001/' + text} style={{ width: '4rem', height: '4rem' }} alt="" />,
    },
    {
      title: '拉黑时间',
      dataIndex: 'date',
      key: 'date',
      render: (text) => dayjs(text).format('YYYY-MM-DD HH:mm:ss')
    },
    {
      title: '操作',
      dataIndex: 'todo',
      key: 'todo',
      render: (_, item) =>
        <Space size="middle">
          <a onClick={() => editBlackLS(item)}>编辑</a>
          <a style={{color: 'red'}} onClick={() => delBlackLS(item._id)}>移除黑名单</a>
        </Space>
    }
  ];
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [open, setOpen] = useState(false);
  const [data, setData] = useState([]);
  const blackList_message = useSelector(state => state.blackList_message)
  let [list, setList] = useState([])
  const onClose = () => {
    setOpen(false);
  };
  //分页
  const handlePageChange = (page, pageSize) => {
    setCurrent(page);
    setPageSize(pageSize);
  };
  //获取数据
  let getData = async () => {
    let { data } = await axios.get(`/people/blacklist?message=${blackList_message}`)
    setData(data)
  }

  //删除
  const delBlackLS = async (id) => {
    let res = await axios.post(`/people/del_blacklist?id=${id}`)
    if (res.code == 200) {
        message.open({
          type: 'success',
          content: '移除成功',
        });
        getData(); //重新获取数据
      }
  }
  // 修改
  const editBlackLS = (item) => {
    setList(item)
    setOpen(true)
  }
  useEffect(() => {
    getData()
  }, [blackList_message,list])
  return (
    <div>
      <div className='body'>
        <div>
          <Table columns={columns}
            style={{ height: '100%' }}
            pagination={{
              current,
              pageSize,
              onChange: handlePageChange,
              showSizeChanger: true,
              pageSizeOptions: ['5', '10', '15'],
            }}
            scroll={{
              y: 300
            }}
            dataSource={data.map((item) => {
              return {
                ...item,
                key: item._id
              }
            })
            } />
        </div>
        <Drawer title="黑名单编辑" onClose={onClose} open={open}>
          <Edit setOpen={setOpen} list={list} getData={getData}/>
        </Drawer>
      </div>
    </div>
  )
}
