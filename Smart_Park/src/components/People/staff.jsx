import React from 'react'
import Header from './Staff/Header'
import Body from './Staff/Body'
export default function staff() {
  return (
    <div>
      <Header/>
      <Body/>
    </div>
  )
}
