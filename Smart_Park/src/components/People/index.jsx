import React from 'react'
import { Outlet } from 'react-router-dom'
import '../../assets/People/index.css'
export default function index() {
  return (
    <Outlet></Outlet>
  )
}
