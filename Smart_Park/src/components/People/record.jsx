import React from 'react'
import Header from './Record/Header'
import Body from './Record/Body'
export default function record() {
  return (
    <div>
      <Header />
      <Body />
    </div>
  )
}
