import React from 'react'
import Header from './Cars/Header'
import Body from './Cars/Body'
export default function cars() {
  return (
    <div>
      <Header/>
      <Body/>
    </div>
  )
}
