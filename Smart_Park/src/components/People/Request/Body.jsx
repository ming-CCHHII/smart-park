import React, { useEffect, useState } from 'react'
import { Button, Table, Drawer, Space } from 'antd';
import axios from '../../../utils/request'
import { useSelector } from 'react-redux';
import '../../../assets/People/index.css'
import { createStyles } from 'antd-style';
import Add from './Add';
import Content from './Content';
import Examine from './Examine';
export default function Body() {
    const useStyle = createStyles(({ css, token }) => {
        const { antCls } = token;
        return {
            customTable: css`
            ${antCls}-table {
              ${antCls}-table-container {
                ${antCls}-table-body,
                ${antCls}-table-content {
                  scrollbar-width: thin;
                  scrollbar-color: #eaeaea transparent;
                  scrollbar-gutter: stable;
                }
              }
            }
          `,
        };
    });
    const { styles } = useStyle();
    const columns = [
        {
            title: '访客姓名',
            dataIndex: 'name',
            key: 'name',
            fixed: 'left',
        },
        {
            title: '联系方式',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: '职位',
            dataIndex: 'position',
            key: 'position',

        },
        {
            title: '造访目的',
            dataIndex: 'purpose',
            key: 'purpose',
        },
        {
            title: '公司名称',
            dataIndex: 'company',
            key: 'company',
        },
        {
            title: '是否驾车',
            dataIndex: 'flag',
            key: 'flag',
            render: (text) => (text ? '否' : '是'),
        },
        {
            title: '车牌号码',
            dataIndex: 'plate',
            key: 'plate',
            render: (text) => (text ? text : '- -'),
        },
        {
            title: '造访时间',
            dataIndex: 'date',
            key: 'date',
        },
        {
            title: '人脸照片',
            dataIndex: 'face',
            key: 'face',
            render: (text) => <img src={'http://127.0.0.1:3001/' + text} style={{ width: '4rem', height: '4rem' }} alt="" />,
        },
        {
            title: '访客等级',
            dataIndex: 'pid',
            key: 'pid',
            render: (text) => (text ? text.level : '- -'),
        },
        {
            title: '是否黑名单',
            dataIndex: 'blackls',
            key: 'blackls',
            render: (_, { block }) => {
                return block ? '是' : '否'
            }
        },
        {
            title: '审核情况',
            dataIndex: 'audit',
            key: 'audit',
            render: (_, { _id, state }) =>
                <Space size="middle">
                    {state == 0 ? <a onClick={() => {
                        handleId1(_id)
                    }}>未审核</a> : ''}
                    {state == 1 ? <a style={{ color: 'green' }}>已通过</a> : ''}
                    {state == 2 ? <a style={{ color: 'red' }}>已拒绝</a> : ''}
                    {state == 3 ? <a style={{ color: 'black' }}>已拉黑</a> : ''}
                </Space>
        },
        {
            title: '操作',
            dataIndex: 'todo',
            key: 'todo',
            fixed: 'right',
            render: (_, { _id }) =>
                <Space size="middle">
                    <a onClick={() => {
                        handleId(_id)
                    }}>查看详情</a>
                </Space>

        }
    ];
    const [current, setCurrent] = useState(1);
    const [pageSize, setPageSize] = useState(5);
    const [data, setData] = useState([]);
    const [data1, setData1] = useState([]);
    const [open, setOpen] = useState(false);
    const [open1, setOpen1] = useState(false);
    const [open2, setOpen2] = useState(false);
    let [ids, setId] = useState('')
    let [levelList, setLevelList] = useState([])
    let msg = useSelector(store => store.message)
    let pid = useSelector(store => store.pid)
    let stateTime = useSelector(store => store.stateTime)
    let endTime = useSelector(store => store.endTime)
    let examine = useSelector(store => store.examine)
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };
    const showDrawer1 = () => {
        setOpen1(true);
    };
    const onClose1 = () => {
        setOpen1(false);
    };
    const showDrawer2 = () => {
        setOpen2(true);
    };
    const onClose2 = () => {
        setOpen2(false);
    };
    //详情
    const handleId = async (id) => {
        let { data } = await axios.get(`/people/Content?id=${id}`)
        setData1(data)
        showDrawer1(true)
    }
    const handleId1 = async (id) => {
        setId(id)
        showDrawer2()
    }
    //分页
    const handlePageChange = (page, pageSize) => {
        setCurrent(page);
        setPageSize(pageSize);
    };
    //获取数据
    let getData = async () => {
        let { data } = await axios.get(`/people/data?message=${msg}&pid=${pid}&stateTime=${stateTime}&endTime=${endTime}&examine=${examine}`)
        setData(data)
    }

    let getLevelList = async () => {
        let { data } = await axios.get('/people/level')
        setLevelList(data)
    }
    useEffect(() => {
        getData(), getLevelList()
    }, [current, pageSize, msg, pid, stateTime, endTime, examine])
    return (
        <div>
            <div className='body'>
                <div>
                    <Button type='primary' onClick={showDrawer}>新增申请</Button>
                </div>
                <Drawer title="新增申请" onClose={onClose} open={open}>
                    <Add setOpen={setOpen} getData={getData} />
                </Drawer>
                <Drawer title="详情" onClose={onClose1} open={open1}>
                    <Content data1={data1} />
                </Drawer>
                <Drawer title="审核" onClose={onClose2} open={open2}>
                    <Examine setOpen2={setOpen2} getData={getData} onClose2={onClose2} levelList={levelList} ids={ids} />
                </Drawer>
                <div>
                    <Table columns={columns}
                        bordered
                        className={styles.customTable}
                        pagination={{
                            current,
                            pageSize,
                            onChange: handlePageChange,
                            showSizeChanger: true,
                            pageSizeOptions: ['5', '10', '15'],
                        }}
                        scroll={{
                            y: 250,
                            x: 2000
                        }}
                        dataSource={data.map((item) => {
                            return {
                                ...item,
                                key: item._id
                            }
                        })
                        } />
                </div>
            </div>
        </div >
    )
}