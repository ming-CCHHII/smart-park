import React, { useCallback, useState } from 'react'
import { Button, Form, Input, Radio, Upload, message, DatePicker } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import axios from '../../../utils/request'
import dayjs from 'dayjs';
export default function Add({setOpen,getData}) {
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState('');
    const [flag, setFlag] = useState(false);
    const [form] = Form.useForm();
    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };
    const handleChange = (info) => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, (url) => {
                setLoading(false);
                // console.log(url)
                setImageUrl(url)
            });
        }
    }
    const uploadButton = (
        <button
            style={{
                border: 0,
                background: 'none',
            }}
            type="button"
        >
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                上传照片
            </div>
        </button>
    );
    let changeState = useCallback((i) => {
        setFlag(i)
    }, [flag])
    //添加
    const onFinish1 = (values) => {
        let add = {
            name: values.name,
            phone: values.phone,
            position: values.position,
            purpose: values.purpose,
            company: values.company,
            flag: values.flag,
            plate: values.plate,
            date: dayjs(values.date).format('YYYY-MM-DD HH:mm:ss'),
            face: values.face.file.response.file
        }
        axios.post('/people/add', add).then(res => {
            if (res.code == 200) {
                message.open({
                    type: 'success',
                    content: '添加成功',
                });
                setOpen(false); //关闭抽屉
                form.resetFields()
            }
            getData()
        })
    };
    return (
        <div>
            <Form
                name="add"
                clearOnDestroy={true}
                onFinish={onFinish1}
                autoComplete="off"
                form={form}
                labelCol={{
                    span: 12,
                }}
                wrapperCol={{
                    span: 16,
                }}
                style={{
                    maxWidth: 600,
                }}
            >
                <Form.Item
                    label="访客姓名"
                    name="name"
                    rules={[{ required: true, message: '请输入访客姓名' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="联系方式"
                    name="phone"
                    rules={[{ required: true, message: '请输入联系方式' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="职位"
                    name="position"
                    rules={[{ required: true, message: '请输入职位' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="造访目的"
                    name="purpose"
                    rules={[{ required: true, message: '请输入造访目的' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="公司名称"
                    name="company"
                    rules={[{ required: true, message: '请输入公司名称' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="是否驾车"
                    name="flag"
                    rules={[{ required: true, message: '请选择是否驾车' }]}
                >
                    <Radio.Group defaultValue='false' buttonStyle="solid">
                        <Radio.Button value="false" onChange={() => {
                            changeState(false)
                        }}>是</Radio.Button>
                        <Radio.Button value="true" onChange={() => {
                            changeState(true)
                        }}>否</Radio.Button>
                    </Radio.Group>
                </Form.Item>
                <Form.Item
                    label="车牌号"
                    name="plate"
                >
                    <Input disabled={flag} />
                </Form.Item>
                <Form.Item
                    label="造访日期"
                    name="date"
                    rules={[{ required: true, message: '请选择造访日期' }]}
                >
                    <DatePicker showTime />
                </Form.Item>
                <Form.Item
                    label="人脸图片"
                    name="face"
                    rules={[{ required: true, message: '请输入人脸图片' }]}
                >
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        action="http://127.0.0.1:3001/people/upload"
                        onChange={handleChange}
                        maxCount={1}
                    >
                        {imageUrl ? (
                            <img
                                src={imageUrl}
                                alt="avatar"
                                style={{
                                    width: '5rem',
                                }}
                            />
                        ) : (
                            uploadButton
                        )}
                    </Upload>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" style={{ margin: '0 1rem' }}>
                        提交
                    </Button>
                    <Button htmlType="reset" onClick={() => {
                        setFlag(false)
                        setImageUrl('')
                    }}>
                        重置
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}
