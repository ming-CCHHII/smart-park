import React, { useCallback, useEffect, useState } from 'react'
import { Button, Form, Input, Select, DatePicker, theme } from 'antd';
import dayjs from 'dayjs';
import request from '../../../utils/request';
import { useDispatch } from 'react-redux';
import { delQuery, setQuery } from '../../../store/module/data'

export default function Header() {
    const [form] = Form.useForm();
    const { Option } = Select;
    let [stateTime, setStateTime] = useState('')
    let [endTime, setEndTime] = useState('')
    const dispatch = useDispatch()
    const [pid, setPid] = useState("")
    const [levelList, setLevelList] = useState([])
    const [examiner, setExaminer] = useState(null)
    const [msg, setMsg] = useState("")
    const handleChangeLevel = (value) => {
        setPid(value)
    };
    const handleChangeExamine = (value) => {
        setExaminer(value)
    };
    const handleReset = () => {
        form.resetFields()
        dispatch(delQuery())
    }
    const onFinish = async (values) => {
        if (values.date) {
            setStateTime(dayjs(values.date[0]).format('YYYY-MM-DD'))
            setEndTime(dayjs(values.date[1]).format('YYYY-MM-DD'))
        }
        dispatch(setQuery({
            message: msg,
            pid: pid,
            stateTime: stateTime,
            endTime: endTime,
            examine: examiner
        }))
    };
    const { token } = theme.useToken();
    const style = {
        border: `1px solid ${token.colorPrimary}`,
        borderRadius: '50%',
    };
    const cellRender = (current, info) => {
        if (info.type !== 'date') {
            return info.originNode;
        }
        if (typeof current === 'number' || typeof current === 'string') {
            return <div className="ant-picker-cell-inner">{current}</div>;
        }
        return (
            <div className="ant-picker-cell-inner" style={current.date() === 1 ? style : {}}>
                {current.date()}
            </div>
        );
    }
    let getLevelList = async () => {
        let { data } = await request.get('/people/level')
        setLevelList(data)
    }
    useEffect(() => {
        getLevelList()
    }, [msg, pid, stateTime, endTime, examiner])
    return (
        <div>
            <div className='header'>
                <Form
                    form={form}
                    onFinish={onFinish}
                >
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Form.Item label="访客信息" name='message' >
                            <Input style={{ width: '12rem' }} placeholder="姓名、公司、手机、车牌号" value={msg} onChange={(e) => {
                                setMsg(e.target.value)
                            }} />
                        </Form.Item>
                        <Form.Item label="访客等级" name='level'>
                            <Select
                                placeholder="请输入"
                                allowClear
                                style={{ width: '12rem' }}
                                onChange={handleChangeLevel}
                            >
                                {levelList.map((item) => {
                                    return <Option key={item._id}>{item.level}</Option>
                                })}
                            </Select>
                        </Form.Item>
                        <Form.Item label="审核状态" name='examine'>
                            <Select
                                allowClear='true'
                                placeholder="请选择"
                                style={{ width: '12rem' }}
                                onChange={handleChangeExamine}
                            >
                                <Option key={0}>未审核</Option>
                                <Option key={1}>已通过</Option>
                                <Option key={2}>已拒绝</Option>
                                <Option key={3}>已拉黑</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="造访时间" name='date'>
                            <DatePicker.RangePicker cellRender={cellRender} />
                        </Form.Item>
                    </div>
                    <div>
                        <Form.Item>
                            <div>
                                <Button type="primary" htmlType="submit" style={{ marginRight: '1rem' }}>
                                    搜索
                                </Button>
                                <Button onClick={handleReset}>
                                    重置
                                </Button>
                            </div>

                        </Form.Item>
                    </div>
                </Form>
            </div>
        </div>
    )
}
