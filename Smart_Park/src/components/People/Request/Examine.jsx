import React,{useState} from 'react'
import { Button, Form, Input, Radio, Select, Cascader } from 'antd';
import axios from '../../../utils/request'
export default function Examine({setOpen2,levelList,ids,onClose2,getData}) {
    const options = [
        {
            value: 'A区',
            label: 'A区',
            children: [
                {
                    value: '1栋',
                    label: '1栋',
                },
                {
                    value: '2栋',
                    label: '2栋',
                },
                {
                    value: '3栋',
                    label: '3栋',
                },
            ],
        },
        {
            value: 'B区',
            label: 'B区',
            children: [
                {
                    value: '4栋',
                    label: '4栋',
                },
                {
                    value: '5栋',
                    label: '5栋',
                },

            ],
        },
        {
            value: 'C区',
            label: 'C区',
            children: [
                {
                    value: '6栋',
                    label: '6栋',
                },
            ],
        },
    ];
    const { Option } = Select;
    const [value, setValue] = useState(1);
    const onFinish = async (values) => {
        let obj = {
            pid: values.pid,
            fulfill: values.fulfill,
            address: values.address,
            block: values.block,
            state: value,
        }
        await axios.post(`/people/examine?_id=${ids}`, obj)
        getData()
        setOpen2(false)
    }
    const onChange = (value) => {
        console.log(value);
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const onChangeRadio = (e) => {
        setValue(e.target.value);
    };
    return (
        <div>
            <Form
                name="examine"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 20,
                }}
                style={{
                    maxWidth: 800,
                }}
                clearOnDestroy={true}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    label="审核结果"
                    name="state"
                >
                    <Radio.Group onChange={onChangeRadio} defaultValue={1} value={value}>
                        <Radio value={1}>已通过</Radio>
                        <Radio value={2}>已拒绝</Radio>
                        <Radio value={3}>已拉黑</Radio>
                    </Radio.Group>
                </Form.Item>
                {value == 1 ? <div><Form.Item
                    label="访客等级"
                    name="pid"
                    rules={[{ required: true, message: '请选择访客等级' }]}
                >
                    <Select
                        placeholder="请选择"
                        allowClear
                        style={{ width: '12rem' }}
                    >
                        {levelList.map((item) => {
                            return <Option key={item._id} value={item._id}>{item.level}</Option>
                        })}
                    </Select>
                </Form.Item>
                    <Form.Item
                        label=" 权限区域"
                        name="address"
                        rules={[{ required: true, message: '请输入权限区域' }]}>
                        <Cascader options={options} onChange={onChange} style={{ width: '12rem' }} placeholder="请选择" />
                    </Form.Item>
                </div> : ''}
                {value == 2 ? <div><Form.Item
                    label="拒绝理由"
                    name="fulfill"
                    rules={[{ required: true, message: '请输入拒绝理由' }]}
                >
                    <Input style={{ width: '12rem', height: '2rem' }} placeholder='请输入拒绝理由' />
                </Form.Item>
                </div>
                    : ''}
                {value == 3 ? <div><Form.Item
                    label="拉黑理由"
                    name="block"
                    rules={[{ required: true, message: '请输入拉黑理由' }]}
                >
                    <Input style={{ width: '12rem', height: '2rem' }} placeholder='请输入拉黑理由' />
                </Form.Item>
                </div>
                    : ''}
                <div style={{ marginLeft: '3rem' }}>
                    <Form.Item>
                        <Button style={{ marginRight: '1rem' }} type="primary" htmlType="submit">确认</Button>
                        <Button htmlType="reset" onClick={onClose2}>取消</Button>
                    </Form.Item>
                </div>
            </Form>
        </div>
    )
}
