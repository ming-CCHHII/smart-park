import React,{useState} from 'react'
import dayjs from 'dayjs';
export default function Content({data1}) {
    
    return (
        <div>
            <table className='table'>
                <tbody>
                    <tr>
                        <td>访客姓名：</td>
                        <td>{data1.name}</td>
                    </tr>
                    <tr>
                        <td>联系方式：</td>
                        <td>{data1.phone}</td>
                    </tr>
                    <tr>
                        <td>职位：</td>
                        <td>{data1.position}</td>
                    </tr>
                    <tr>
                        <td>造访目的：</td>
                        <td>{data1.purpose}</td>
                    </tr>
                    <tr>
                        <td>公司名称：</td>
                        <td>{data1.company}</td>
                    </tr>
                    <tr>
                        <td>是否驾车：</td>
                        <td>{data1.flag ? '否' : '是'}</td>
                    </tr>
                    <tr>
                        <td>车牌号码：</td>
                        <td>{data1.flag ? '- -' : data1.plate}</td>
                    </tr>
                    <tr>
                        <td>造访日期-时间：</td>
                        <td>{dayjs(data1.date).format('YYYY-MM-DD')} {dayjs(data1.time).format('HH:mm:ss')}</td>
                    </tr>
                    <tr>
                        <td>人脸图片：</td>
                        <td>
                            <img src={'http://127.0.0.1:3001/' + data1.face} style={{ width: "100px", height: "100px" }} alt="" />
                        </td>
                    </tr>
                    <tr>
                        <td>审核结果：</td>
                        {data1.state == 0 ? <td>未审核</td> : ''}
                        {data1.state == 1 ? <td>已通过</td> : ''}
                        {data1.state == 2 ? <td style={{ color: 'red' }}>已拒绝</td> : ''}
                        {data1.state == 3 ? <td>已拉黑</td> : ''}
                    </tr>
                    {data1.state == 1 ?
                        <tr>
                            <td>访客等级：</td>
                            <td>{data1.pid.level}</td>
                        </tr> : ''}
                    {data1.state == 1 ? <tr>
                        <td>权限区域：</td>
                        <td>{data1.address}</td>
                    </tr> : ''}

                    {data1.state == 2 ?
                        <tr>
                            <td>拒绝理由：</td>
                            <td>{data1.fulfill}</td>
                        </tr>
                        : ''}
                    {data1.state == 3 ?
                        <tr>
                            <td>拉黑理由：</td>
                            <td>{data1.block}</td>
                        </tr>
                        : ''}
                </tbody>
            </table>
        </div>
    )
}
