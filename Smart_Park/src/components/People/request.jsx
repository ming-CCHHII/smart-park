import React from 'react'
import Header from './Request/Header'
import Table from './Request/Body'
export default function request() {
  return (
    <div>
      <Header/>
      <Table/>     
    </div>
  )
}
