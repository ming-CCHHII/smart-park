import React, { useCallback, useState } from 'react'
import { Button, Form, Input, Select, DatePicker, theme } from 'antd';
import { useDispatch } from 'react-redux';
import { person_setQuery, person_delQuery } from '../../../store/module/data'

export default function Header() {
  const [form] = Form.useForm();
  const { Option } = Select;
  const dispatch = useDispatch()
  const [person_type, setType] = useState('')
  const [person_message, setMessage] = useState('')
  const [access, setAccess] = useState('')
  const [pass, setPass] = useState('')
  const [inOut, setInOut] = useState('')
  let [passStateTime, setStateTime] = useState('')
  let [passEndTime, setEndTime] = useState('')
  const handleChangeState = (value) => {
    setType(value)
  };
  const handleChangePass = (value) => {
    setPass(value)
  };
  const handleChangeInOut = useCallback((value) =>{
    setInOut(value)
    console.log(value);
  },[inOut])
  const handleReset = () => {
    form.resetFields()
    dispatch(person_delQuery())
  }
  const { token } = theme.useToken();
  const style = {
    border: `1px solid ${token.colorPrimary}`,
    borderRadius: '50%',
  };
  const cellRender = (current, info) => {
    if (info.type !== 'date') {
      return info.originNode;
    }
    if (typeof current === 'number' || typeof current === 'string') {
      return <div className="ant-picker-cell-inner">{current}</div>;
    }
    return (
      <div className="ant-picker-cell-inner" style={current.date() === 1 ? style : {}}>
        {current.date()}
      </div>
    );
  }
  const onFinish = async (values) => {
    if (values.date) {
      setStateTime(dayjs(values.date[0]).format('YYYY-MM-DD'))
      setEndTime(dayjs(values.date[1]).format('YYYY-MM-DD'))
    }
    dispatch(person_setQuery({
      person_type: person_type,
      person_message: person_message,
      access: access,
      pass: pass,
      inOut: inOut,
      passStateTime: passStateTime,
      passEndTime: passEndTime
    }))
  };

  return (
    <div>
      <div className='level_header'>
        <Form
          form={form}
          onFinish={onFinish}
          layout='inline'
        >
          <Form.Item label="人员信息" name='message' >
            <Input style={{ width: '15rem' }} placeholder="请输入姓名、手机、职务等" value={person_message} onChange={(e) => setMessage(e.target.value)} />
          </Form.Item>
          <Form.Item label="人员类型" name='type'>
            <Select
              placeholder="请输入"
              allowClear
              style={{ width: '12rem' }}
              onChange={handleChangeState}
            >
              <Option value="内部员工">内部员工</Option>
              <Option value="预约访客">预约访客</Option>
              <Option value="非内部员工">非内部员工</Option>
            </Select>
          </Form.Item>
          <Form.Item label="门禁设备" name='access' >
            <Input style={{ width: '15rem' }} placeholder="请输入编号、名称等" value={access} onChange={(e) => setAccess(e.target.value)} />
          </Form.Item>
          <Form.Item label="同行方式" name='pass'>
            <Select
              placeholder="请输入"
              allowClear
              style={{ width: '12rem' }}
              onChange={handleChangePass}
            >
              <Option value="人脸识别">人脸识别</Option>
              <Option value="刷门禁卡">刷门禁卡</Option>
              <Option value="二维码通行">二维码通行</Option>
            </Select>
          </Form.Item>
          
              <Form.Item label="进出方向" name='inOut'>
                <Select
                  placeholder="请输入"
                  allowClear
                  style={{ width: '12rem' }}
                  onChange={handleChangeInOut}
                >
                  <Option value="true">进场</Option>
                  <Option value="false">出场</Option>
                </Select>
              </Form.Item>
              <Form.Item label="造访时间" name='date'>
                <DatePicker.RangePicker cellRender={cellRender} />
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" style={{ marginRight: '1rem' }}>
                  搜索
                </Button>
                <Button onClick={handleReset}>
                  重置
                </Button>
              </Form.Item>

        </Form>
      </div>
    </div>
  )
}