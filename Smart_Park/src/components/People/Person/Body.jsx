import React, { useEffect, useState } from 'react'
import { Button, Table, Drawer, message, Space } from 'antd';
import { useSelector } from 'react-redux';
import '../../../assets/People/index.css'
import axios from '../../../utils/request'
import dayjs from 'dayjs';
export default function Body() {
    const columns = [
        {
            title: '人员姓名',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '联系方式',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: '职位',
            dataIndex: 'position',
            key: 'position',
        },
        {
            title: '部门/公司',
            dataIndex: 'company',
            key: 'company',
        },
        {
            title: '人员类型',
            dataIndex: 'type',
            key: 'type',
            render: (text) => text ? '内部员工' : '非内部员工'
        },
        {
            title: '门禁设备',
            dataIndex: 'access',
            key: 'access',
        },
        {
            title: '所在地区',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: '通行抓拍',
            dataIndex: 'img',
            key: 'img',
            render: (text) => <img src={text} style={{ width: '4rem', height: '4rem' }} alt="" />,
        },
        {
            title: '同行方式',
            dataIndex: 'pass',
            key: 'pass',
        },
        {
            title: '进出方向',
            dataIndex: 'inOut',
            key: 'inOut',
            render: (text) => text ? '进场' : '出场'
        },
        {
            title: '通行时间',
            dataIndex: 'date',
            key: 'date',
            render: (text) => dayjs(text).format('YYYY-MM-DD HH:mm:ss')
        }
    ];
    const [current, setCurrent] = useState(1);
    const [pageSize, setPageSize] = useState(5);
    const [data, setData] = useState([]);
    const person_message = useSelector(state => state.person_message);
    const person_type = useSelector(state => state.person_type);
    const access = useSelector(state => state.access);
    const pass = useSelector(state => state.pass);
    const inOut = useSelector(state => state.inOut);
    const passStateTime = useSelector(state => state.passStateTime);
    const passEndTime = useSelector(state => state.passEndTime);
    //分页
    const handlePageChange = (page, pageSize) => {
        setCurrent(page);
        setPageSize(pageSize);
    };
    //获取数据
    let getData = async () => {
        let { data } = await axios.get(`/people/person?person_message=${person_message}&person_type=${person_type}&
        access=${access}&pass=${pass}&inOut=${inOut}&passStateTime=${passStateTime}&passEndTime=${passEndTime}`)
        setData(data)
    }
    useEffect(() => {
        getData()
    }, [person_message,
        person_type,
        access,
        pass,
        inOut,
        passStateTime,
        passEndTime])
    return (
        <div>
            <div className='body'>
                <div>
                    <Table columns={columns}
                        style={{ height: '100%' }}
                        pagination={{
                            current,
                            pageSize,
                            onChange: handlePageChange,
                            showSizeChanger: true,
                            pageSizeOptions: ['5', '10', '15'],
                        }}
                        scroll={{
                            y: 300
                        }}
                        dataSource={data.map((item) => {
                            return {
                                ...item,
                                key: item._id
                            }
                        })
                        } />
                </div>
            </div>
        </div>
    )
}
