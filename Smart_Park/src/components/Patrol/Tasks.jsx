import React, { useEffect, useState, useCallback } from 'react'
import style from "../../assets/patrolcss/task.module.css"
import { Input, Form, Space, Button, Drawer, Cascader } from "antd";
import Table from './components/table'
import Drawers from './components/drawer'
import request from '../../utils/request';
import { useDispatch, useSelector } from 'react-redux';
import { setpatrol } from '../../store/module/data';

export default function Task() {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const options = [
    {
      value: 1,
      label: '完成',
    },
    {
      value: 0,
      label: '未完成',
    },
  ];
  const [name, setName] = useState(""); // 任务名称
  const [person, setPerson] = useState(""); // 执行人员
  const [plan, setPlan] = useState(""); // 来源计划
  const [finish, setFinish] = useState(null); // 完成情况
  const [open, setOpen] = useState(false);
  const [tableData, setTableData] = useState([]);

  const onReset = async() => {
    form.resetFields();
    setName(''); // 清空状态
    setPerson('');
    setPlan('');
    setFinish(null);
    getdata()
  };
  const addQuest = () => {
    setOpen(true);
  }
  const onClose = () => {
    setOpen(false);
  };

  const gettabledata = async () => {
    // 获取巡查点
    let { data } = await request.get('/patrol/getpatrolSizeModel');
    dispatch(setpatrol(data))
  };

  const getdata = async () => {
    // 提交/获取
    const res1 = await request.post("/patrol/getpatrolTaskModel", {
      name: name, person: person, plan: plan, state: finish
    })
    setTableData(res1.data)
  };

  useEffect(() => {
    gettabledata()
    getdata()
  }, [])
  return (
    <div className={style.body}>
      <div className={style.big}>
        <Form form={form} style={{ display: "flex", justifyContent: "space-between", flexWrap: "wrap" }}>
          <Form.Item
            label="任务名称: "
            name="name"
          >
            <Input placeholder="请输入" style={{ width: "13rem" }} value={name} onChange={(e) => setName(e.target.value)} />
          </Form.Item>
          <Form.Item
            label="执行人员"
            name="person"
          >
            <Input placeholder="请输入" style={{ width: "13rem" }} value={person} onChange={(e) => setPerson(e.target.value)} />
          </Form.Item>
          <Form.Item
            label="来源计划: "
            name="plan"
          >
            <Input placeholder="请输入" style={{ width: "13rem" }} value={plan} onChange={(e) => setPlan(e.target.value)} />
          </Form.Item>
          <Form.Item
            label="完成情况: "
            name="finish"
          >
            <Cascader options={options} style={{ width: "13rem" }} onChange={(e) => setFinish(e[0])} placeholder="请输入" />
          </Form.Item>
        </Form>
        <Space style={{ marginLeft: "5rem" }}>
          <Button type="primary" htmlType="submit" onClick={getdata}>
            搜索
          </Button>
          <Button htmlType="button" onClick={onReset}>
            重置
          </Button>
        </Space>
      </div>
      <div className={style.simmll}>
        <Button type="primary" onClick={addQuest}>新增任务</Button>
        <Drawer size="large" title="新增任务" onClose={onClose} open={open}>
          <Drawers setonClose={onClose} getpatrol={getdata}></Drawers>
        </Drawer>
        <Table datas={tableData} getpatrol={getdata}></Table>
      </div>
    </div>
  )
}
