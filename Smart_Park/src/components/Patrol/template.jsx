import React, { useEffect, useState } from 'react'
import style from "../../assets/patrolcss/task.module.css"
import Frimt from "./components/templates/form";
import Table from "./components/templates/table";
import request from '../../utils/request';
// 引入redux
import { useDispatch, useSelector } from 'react-redux';
import { settemplate } from '../../store/module/data';

export default function template() {
  const dispatch = useDispatch();
  const data = useSelector(state => state.template)

  // 获取数据
  const gettabledata = async (item) => {
    const res = await request.post('/patrol/gettemplateModel', item)
    if (res.code == 200) {
      dispatch(settemplate(res.data))
    }
  }
  useEffect(() => {
    gettabledata()
  }, [])
  return (
    <div className={style.body}>
      <div className={style.big}>
        <Frimt getdata ={gettabledata}></Frimt>
      </div>
      <div className={style.simmll}>
        <Table data={data} getdata={gettabledata}></Table>
      </div>
    </div>
  )
}
