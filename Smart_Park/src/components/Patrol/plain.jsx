import React, { useEffect, useState } from 'react'
import style from "../../assets/patrolcss/task.module.css"
import Frimt from "./components/plain/form";
import Table from "./components/plain/table"
import request from '../../utils/request';
// 引入redux
import { useDispatch, useSelector } from 'react-redux';
import { setplain } from '../../store/module/data';

export default function plain() {
  const dispatch = useDispatch();
  const data = useSelector(state => state.plain)

  // 获取数据
  const gettabledata = async (item) => {
    const res = await request.post('/patrol/getpatrolPlanModel', item)
    if (res.code == 200) {
      dispatch(setplain(res.data))
    }
  }
  useEffect(() => {
    gettabledata()
  }, [])
  return (
    <div className={style.body}>
      <div className={style.big}>
        <Frimt getdata ={gettabledata}></Frimt>
      </div>
      <div className={style.simmll}>
        <Table data={data} getdatas={gettabledata}></Table>
      </div>
    </div>
  )
}
