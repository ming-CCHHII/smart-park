import React, { useEffect, useState } from 'react'
import style from "../../assets/patrolcss/task.module.css"
import Frimt from "./components/points/form";
import Tables from "./components/points/table"
export default function point() {
  const [searchdata, setSearchdata] = useState({})
  const getdata = (searchdata) =>{
    setSearchdata(searchdata)
  }
  return (
    <div className={style.body}>
      <div className={style.big}>
        <Frimt getdata={getdata}></Frimt>
      </div>
      <div className={style.simmll}>
        <Tables searchdata={searchdata}></Tables>
      </div>
    </div>
  )
}
