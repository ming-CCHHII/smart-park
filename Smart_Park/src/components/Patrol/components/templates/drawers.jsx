import React, { useState, useEffect } from 'react'
import { Button, Form, Input, Cascader, DatePicker, Space, message, Switch } from 'antd';
const { TextArea } = Input;
import request from '../../../../utils/request';
export default function drawers({ item, setonClose, open }) {
    const [form] = Form.useForm();
    const [name, setName] = useState(""); // 模板名称
    const [content, setContent] = useState([]); // 模板内容
    const [state, setState] = useState(false); // 是否启用

    // 表单验证/提交
    const onFinish = async () => {
        if (item) {
            // 修改
            let res = await request.put('/patrol/settemplateModel', {
                id: item._id,
                state: {
                    name: name,
                    content: content.split(','),
                    state: state ? 1 : 0
                }
            })
            if (res.code == 200) {
                message.success('修改成功')
                setonClose()
                form.resetFields()
            }
        } else {
            // 添加
            let res = await request.post('/patrol/addtemplateModel', {
                name: name,
                content: content.split(','),
                state: state ? 1 : 0

            })
            if (res.code == 200) {
                message.success('添加成功')
                setonClose()
                form.resetFields()
            }
        }
    }

    // 初始化
    useEffect(() => {
        if (item) {
            let con = item.content.join(',')
            setName(item.name)
            setContent(con)
            setState(item.state == 1 ? true : false)
            form.setFieldsValue({
                name: item.name, content: item.content.join(',')
            })
        } else {
            setName("")
            setContent("")
            setState(false)
        }
    }, [item])

    // 关闭抽屉
    useEffect(() => {
        if (!open) {
            form.resetFields();
        }
    }, [open]);
    return (
        <>
            <Form form={form}
                style={{
                    padding: '0px 40px',
                    boxSizing: 'border-box',
                    width: '100%',
                }}
                onFinish={onFinish}
            >
                <Form.Item
                    label="模板名称: "
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Input placeholder='请输入' onChange={(e) => setName(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="模板名称: "
                    name="content"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <TextArea rows={4} placeholder="请输入" onChange={(e) => setContent(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="是否有效: "
                    name="state"
                    rules={[
                        {
                            required: false,
                            message: 'Please input your username!',
                        }
                    ]}
                >
                    <Switch checked={state} onChange={(e) => { setState(e ? 1 : 0) }} />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">{item ? "修改" : "确定"}</Button>
                </Form.Item>
            </Form>
        </>
    )
}
