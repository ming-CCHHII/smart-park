import React, { useState } from 'react'
import { Space, Table, Tag, Drawer, Button, message } from 'antd';
import Drawers from "./drawers"
import dayjs from 'dayjs';
export default function table({ data, getdata }) {
    const columns = [
        {
            title: '编号',
            dataIndex: 'code',
            key: 'code',
            width: 100
        },
        {
            title: '模板名称',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: '检查项目',
            dataIndex: 'name',
            key: 'name',
            width: 300,
            render: (_, { content }) => {
                let content1 = content.join(", ");
                return <span>{content1}</span>
            }
        },
        {
            title: "创建时间",
            dataIndex: 'createtime',
            key: 'createtime',
            render: (_, { createtime }) => {
                return <span>{dayjs(createtime).format('YYYY-MM-DD HH:mm:ss')}</span>
            }
        },
        {
            title: "是否有效",
            dataIndex: 'state',
            key: 'state',
            render: (_, { state }) => {
                return <Tag color={state == 0 ? 'red' : 'blue'}>
                    <span>{state == 0 ? '无效' : '有效'}</span>
                </Tag>;
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            width: 200,
            render: (_, item) => (
                <Space size="middle">
                    <a onClick={() => setitem(item)}>编辑</a>
                    <a onClick={() => del(item._id)} style={{ color: "red" }}>删除</a>
                </Space>
            ),
        }
    ];
    const [messageApi, contextHolder] = message.useMessage(); // 消息
    const [current, setCurrent] = useState(1);
    const [pageSize, setPageSize] = useState(5);
    const [open, setOpen] = useState(false);
    const [item, setItems] = useState({});
    // 分页
    const handlePageChange = (page, pageSize) => {
        setCurrent(page);
        setPageSize(pageSize);
    };

    // 添加开启
    const addQuest = () => {
        setOpen(true);
        setItems()
    }

    // 关闭
    const onClose = () => {
        setOpen(false);
        getdata()
        setItems()
    }

    // 修改
    const setitem = (item) => {
        setItems(item);
        setOpen(true);
    }

    // 删除
    const del = (id)=>{
        
    }

    return (
        <>
            <Button type="primary" onClick={() => addQuest()}>新增任务</Button>
            <Table pagination={{
                current,
                pageSize,
                total: data.length,
                onChange: handlePageChange,
                showSizeChanger: true,
                pageSizeOptions: ['5', '10', '15'],
            }}
                scroll={{
                    y: 300
                }}
                columns={columns} dataSource={data.map(item => ({ ...item, key: item._id }))} />
            <Drawer size="default" title="编辑模板" onClose={onClose} open={open}>
                <Drawers setonClose={onClose} item={item} open={open}></Drawers>
            </Drawer>
        </>
    )
}
