import React from 'react'
import { Input, Form, Space, Button, Cascader } from "antd";
export default function form({ getdata }) {
  const [form] = Form.useForm();
  const options = [
    {
      value: 1,
      label: '有效',
    },
    {
      value: 0,
      label: '无效',
    },
  ];
  const [name, setName] = React.useState(""); // 模板名称
  const [state, setState] = React.useState(""); // 是否有效

  const onReset = () => {
    form.resetFields();
    setName("");
    setState("");
    getdata();
  }

  const onSubmit = () => {
    let data = {
      name: name,
      state: state
    }
    getdata(data)
  }
  return (
    <>
      <Form form={form} style={{ display: "flex" }}>
        <Form.Item
          label="模板名称: "
          name="name"
        >
          <Input placeholder="请输入" style={{ width: "13rem" }} value={name} onChange={(e) => setName(e.target.value)} />
        </Form.Item>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <Form.Item
          label="是否有效: "
          name="finish"
        >
          <Cascader options={options} style={{ width: "13rem" }} onChange={(e) => setState(e[0])} placeholder="请输入" />
        </Form.Item>
      </Form>
      <Space style={{ marginLeft: "5rem" }}>
        <Button type="primary" htmlType="submit" onClick={(e) => onSubmit()}>
          搜索
        </Button>
        <Button htmlType="button" onClick={() => onReset()}>
          重置
        </Button>
      </Space>
    </>
  )
}
