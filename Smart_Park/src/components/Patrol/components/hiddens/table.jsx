import React, { useState } from 'react'
import { Space, Table, Tag, Drawer, Button, message } from 'antd';
import Drawers from "./drawers"
export default function table({ data }) {
    const columns = [
        {
            title: '上报时间',
            dataIndex: 'createtime',
            key: 'createtime',
            width: 150
        },
        {
            title: '隐患描述',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: "隐患对象",
            dataIndex: 'object',
            key: 'object',
        },
        {
            title: "隐患等级",
            dataIndex: 'level',
            key: 'level',
            render: (_, { level }) => {
                return <Tag color={level == 1 ? 'red' : level == 2 ? 'orange' : 'blue'}>
                    <span>{level == 1 ? '严重' : level == 2 ? '提醒' : '一般'}</span>
                </Tag>;
            }
        },
        {
            title: '所在位置',
            dataIndex: 'position',
            key: 'position'
        },
        {
            title: '上报人',
            dataIndex: 'reporter',
            key: 'reporter'
        },
        {
            title: '隐患图片',
            dataIndex: 'troubleimg',
            key: 'troubleimg',
            render: (_, { troubleimg }) => {
                return <img src={troubleimg} />;
            }
        },
        {
            title: '整改人',
            dataIndex: 'rectify',
            key: 'rectify'
        },
        {
            title: '整改状态',
            dataIndex: 'rectifystate',
            key: 'rectifystate',
            render: (_, { rectifystate }) => {
                return <Tag color={rectifystate == 0 ? 'orange' : 'blue'}>
                    <span>{rectifystate == 0 ? '未整改' : '已整改'}</span>
                </Tag>;
            }
        },
        {
            title: '整改图片',
            dataIndex: 'rectifyimg',
            key: 'rectifyimg',
            render: (_, { rectifyimg }) => {
                return <img src={rectifyimg} />;
            }
        },
        {
            title: '整改时间',
            dataIndex: 'rectifytime',
            key: 'rectifytime',
            width: 150
        },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            width: 200,
            render: (_, item) => (
                <Space size="middle">
                    <a onClick={() => setitem(item)}>详情</a>
                </Space>
            ),
        },
    ];
    const [messageApi, contextHolder] = message.useMessage(); // 消息
    const [current, setCurrent] = useState(1);
    const [pageSize, setPageSize] = useState(5);
    const [open, setOpen] = useState(false);
    const [item, setItems] = useState({});
    // 分页
    const handlePageChange = (page, pageSize) => {
        setCurrent(page);
        setPageSize(pageSize);
    };

    // 关闭添加
    const onClose = () => {
        setOpen(false);
    }

    // 详情
    const setitem = (item) => {
        setItems(item);
        setOpen(true);
    }

    return (
        <>
            {contextHolder}
            <Table pagination={{
                current,
                pageSize,
                total: data.length,
                onChange: handlePageChange,
                showSizeChanger: true,
                pageSizeOptions: ['5', '10', '15'],
            }}
                scroll={{
                    y: 300,
                    x: 1800
                }}
                columns={columns} dataSource={data.map(item => ({ ...item, key: item._id }))} />
            <Drawer size="large" title="隐患详情" onClose={onClose} open={open}>
                <Drawers setonClose={onClose} item={item}></Drawers>
            </Drawer>
        </>
    )
}
