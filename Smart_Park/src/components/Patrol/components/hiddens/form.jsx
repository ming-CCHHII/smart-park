import React from 'react'
import { Input, Form, Space, Button, Cascader } from "antd";
export default function form({ getdata }) {
    const [form] = Form.useForm();
    const levels = [
        {
            value: 0,
            label: '一般',
        },
        {
            value: 1,
            label: '严重',
        },
        {
            value: 2,
            label: "提醒"
        }
    ];
    const rectifystates = [
        {
            value: 0,
            label: '未整改',
        },
        {
            value: 1,
            label: "已整改",
        }
    ]
    const [title, setTitle] = React.useState(""); // 隐患描述
    const [reporter, setReporter] = React.useState(""); // 上报人
    const [rectify, setRectify] = React.useState(""); // 整改人
    const [level, setLevel] = React.useState("") // 隐患等级
    const [rectifystate, setRectifystate] = React.useState(""); // 整改状态
    
    // 重置表单
    const onReset = () => {
        form.resetFields();
        setTitle("");
        setReporter("");
        setRectify("");
        setLevel("");
        setRectifystate("");
        getdata()
    }

    // 提交表单
    const onSubmit = () => {
        let data = {
            title: title,
            reporter: reporter,
            rectify: rectify,
            level: level,
            rectifystate: rectifystate
        }
        getdata(data)
    }
    return (
        <>
            <Form form={form} style={{ display: "flex", justifyContent: "space-between", flexWrap: "wrap" }}>
                <Form.Item
                    label="隐患描述: "
                    name="title"
                >
                    <Input placeholder="请输入" style={{ width: "12rem" }} value={title} onChange={(e) => setTitle(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="上报人: "
                    name="reporter"
                >
                    <Input placeholder="请输入" style={{ width: "12rem" }} value={reporter} onChange={(e) => setReporter(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="整改人: "
                    name="plan"
                >
                    <Input placeholder="请输入" style={{ width: "12rem" }} value={rectify} onChange={(e) => setRectify(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="隐患等级"
                    name="person"
                >
                    <Cascader options={levels} style={{ width: "12rem" }} onChange={(e) => setLevel(e[0])} placeholder="请选择" />
                </Form.Item>
                
                <Form.Item
                    label="整改状态: "
                    name="finish"
                >
                    <Cascader options={rectifystates} style={{ width: "12rem" }} onChange={(e) => setRectifystate(e[0])} placeholder="请选择" />
                </Form.Item>
            </Form>
            <Space style={{ marginLeft: "5rem" }}>
                <Button type="primary" htmlType="submit" onClick={(e) => onSubmit()}>
                    搜索
                </Button>
                <Button htmlType="button" onClick={() => onReset()}>
                    重置
                </Button>
            </Space>
        </>
    )
}
