import React, { useState, useEffect } from 'react';
import { Form } from 'antd';
export default function drawers({ item, setonClose }) {
    const [items, setitems] = useState({})
    useEffect(() => {
        setitems(item)
    }, [item])
    return (
        <>
            <div style={{ width: '100%', height: '100%',padding: '50px 100px', display: 'flex', justifyContent: 'space-between', alignItems: 'center', boxSizing: "border-box"}}>
                <div>
                    <Form>
                        <Form.Item label="隐患描述:">
                            <span>{items.title}</span>
                        </Form.Item>
                        <Form.Item label="隐患等级:">
                            <span>{items.level == 0 ? '一般隐患' : items.level == 1 ? '严重隐患' : '提醒'}</span>
                        </Form.Item>
                        <Form.Item label="上报人:">
                            <span>{items.reporter}</span>
                        </Form.Item>
                        <Form.Item label="隐患图片:">
                            <img src={items.troubleimg} alt="" />
                        </Form.Item>
                        <Form.Item label="整改人">
                            <span>{items.rectify}</span>
                        </Form.Item>
                        <Form.Item label="整改状态:">
                            <span>{items.rectifystate == 0 ? '未整改' : '已整改'}</span>
                        </Form.Item>
                        <Form.Item label="整改图片:">
                        <img src={items.rectifyimg} alt="" />
                        </Form.Item>
                    </Form>
                </div>
                <div>
                    <Form>
                        <Form.Item label="隐患对象:">
                            <span>{items.title}</span>
                        </Form.Item>
                        <Form.Item label="所在位置:">
                            <span>{item.position}</span>
                        </Form.Item>
                        <Form.Item label="联系方式:">
                            <span>{items.reporterphone}</span>
                        </Form.Item>
                        <Form.Item label="上报时间:">
                            <span>{items.createtime}</span>
                        </Form.Item>
                        <Form.Item label="联系方式:">
                            <span>{items.rectifyphone}</span>
                        </Form.Item>
                        <Form.Item label="整改描述:">
                            <span>{items.rectifytitle}</span>
                        </Form.Item>
                        <Form.Item label="整改时间:">
                            <span>{items.rectifytime}</span>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </>
    )
}
