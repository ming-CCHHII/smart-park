import React, { useEffect, useState } from 'react';
import { Button, Form, Input, Cascader, DatePicker, Space, message } from 'antd';
import request from '../../../utils/request';
import Transfers from './transfer';
// 导入redux
import { useDispatch, useSelector } from 'react-redux';
import { setoptions } from '../../../store/module/data';
export default function Drawer({ setonClose, getpatrol }) {
    const dispatch = useDispatch();
    const options = useSelector((state) => state.options);
    const [form] = Form.useForm()
    const [Taskname, setTaskname] = useState(null); // 任务名称
    const [Taskpersonnel, setTaskpersonnel] = useState(null); // 执行人员
    const [Starttime, setStarttime] = useState(null); // 开始时间
    const [Endtime, setEndtime] = useState(null); // 结束时间
    const [patrols, setPatrols] = useState(null); // 巡检点
    const [source, setSource] = useState([]); // 来源
    // 获取执行人员
    const getpersonnelTypeModel = async () => {
        const res = await request.get('/patrol/getpersonnelTypeModel')
        dispatch(setoptions(res.data))
    }
    // 子组件传值巡查点
    const onsetpatrol = (values, typ) => {
        setPatrols(values)
        setSource(typ)
    }

    // 提交表单
    const onFinish = async () => {
        if (source.length > 0) {
            const res = await request.post('/patrol/addpatrolTaskModel', {
                name: Taskname,
                Taskpersonnel: Taskpersonnel,
                patrols: patrols,
                startime: Starttime + "-" + Endtime,
                source: source
            })
            if (res.code === 200) {
                // messageApi.success(res.msg)
                setonClose()
                onReset()
                getpatrol()
            }
        } else {
            // messageApi.open({
            //     type: 'error',
            //     content: '添加失败',
            // });
        }

    };

    // 重置表单
    const onReset = () => {
        form.resetFields();
        // 清空
        setTaskname(null)
        setTaskpersonnel(null)
        setStarttime(null)
        setEndtime(null)
        setPatrols(null)
        setSource([])
    };
    const changes = (e)=>{
        let name = e[1].split('/')[0]
        setTaskpersonnel(name)
    }

    useEffect(() => {
        getpersonnelTypeModel()
    }, [])


    return (
        <>
            {/* {contextHolder} */}
            <Form
                form={form}
                name="basic"
                onFinish={onFinish}
                style={{
                    padding: '0px 40px',
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    boxSizing: 'border-box',
                    height: '500px'
                }}
            >
                <Form.Item
                    label="任务名称"
                    name="a"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Input placeholder='请输入' value={Taskname} onChange={(e) => setTaskname(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="执行人员"
                    name="b"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}

                >
                    <Cascader options={options} onChange={(e) => changes(e)} placeholder="请选择" />
                </Form.Item>
                <Form.Item
                    label="开始时间"
                    name="c"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <DatePicker
                        showTime
                        placeholder="请选择日期时间"
                        onChange={(value, dateString) => {
                            setStarttime(dateString)
                        }}
                    />
                </Form.Item>
                <Form.Item
                    label="结束时间"
                    name="d"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <DatePicker
                        showTime
                        placeholder="请选择日期时间"
                        onChange={(value, dateString) => {
                            setEndtime(dateString)
                        }}
                    />
                </Form.Item>
                <Form.Item
                    label="选择巡查点"
                    name="e"
                    layout="vertical"
                    style={{ height: '250px' }}
                >
                    <Transfers onsetpatrol={onsetpatrol}></Transfers>
                </Form.Item>
            </Form>
            <Space style={{ marginLeft: "5rem" }}>
                <Button type="primary" onClick={() => form.submit()} htmlType="submit">
                    Submit
                </Button>
                <Button htmlType="button" onClick={() => onReset()}>
                    重置
                </Button>
            </Space>
        </>
    );
}