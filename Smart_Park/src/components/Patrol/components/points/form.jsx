import React from 'react'
import { Input, Form, Space, Button, Cascader } from "antd";
export default function form({getdata}) {
    const [form] = Form.useForm();
    const options = [
        {
            value: 1,
            label: '有效',
        },
        {
            value: 0,
            label: '无效',
        },
    ];
    const [name, setName] = React.useState(""); // 巡查点名称
    const [object, setObject] = React.useState(""); // 巡查对象
    const [inspect, setInspect] = React.useState(""); // 检查模板
    const [state, setState] = React.useState(""); // 状态
    const onReset = () => {
        form.resetFields();
        setName("");
        setObject("");
        setInspect("");
        setState("");
        getdata({});
    }
    const onSubmit = () => {
        let data = {
            name: name,
            object: object,
            inspect: inspect,
            state: state
        }
        getdata(data)
    }
    return (
        <>
            <Form form={form} style={{ display: "flex", justifyContent: "space-between", flexWrap: "wrap" }}>
                <Form.Item
                    label="巡查点名称: "
                    name="name"
                >
                    <Input placeholder="请输入" style={{ width: "13rem" }} value={name} onChange={(e) => setName(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="巡查对象"
                    name="person"
                >
                    <Input placeholder="请输入" style={{ width: "13rem" }} value={object} onChange={(e) => setObject(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="检查模板: "
                    name="plan"
                >
                    <Input placeholder="请输入" style={{ width: "13rem" }} value={inspect} onChange={(e) => setInspect(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="是否有效: "
                    name="finish"
                >
                    <Cascader options={options} style={{ width: "13rem" }} onChange={(e) => setState(e[0])} placeholder="请输入" />
                </Form.Item>
            </Form>
            <Space style={{ marginLeft: "5rem" }}>
                <Button type="primary" htmlType="submit" onClick={(e) => onSubmit()}>
                    搜索
                </Button>
                <Button htmlType="button" onClick={() => onReset()}>
                    重置
                </Button>
            </Space>
        </>
    )
}
