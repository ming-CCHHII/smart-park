import React, { useState, useEffect } from 'react';
import { Form, Input, Switch, Button } from "antd"
import request from '../../../../utils/request';

export default function setadd({ feg, open, onClose }) {
    const [form] = Form.useForm(); // 表单
    const [name, setname] = useState('');
    const [position, setposition] = useState("");
    const [object, setobject] = useState("");
    const [inspect, setinspect] = useState("");
    const [state, setstate] = useState(false);

    useEffect(() => {
        if (feg) {
            setname(feg.name),
                setposition(feg.position),
                setobject(feg.object),
                setinspect(feg.inspect),
                setstate(feg.state == 1 ? true : false)
            form.setFieldsValue({ name: feg.name, position: feg.position, object: feg.object, inspect: feg.inspect }); // 设置初始值
        } else {
            setname(""),
                setposition(""),
                setobject(""),
                setinspect(""),
                setstate(false)
        }
    }, [feg]);
    useEffect(() => {
        if (!open) {
            form.resetFields();
        }
    }, [open]);

    // 提交
    const onFinish = async () => {
        let arr = {
            name: name,
            position: position,
            object: object,
            inspect: inspect,
            createtime: new Date(),
            state: state,
            img: "1"
        }
        // 修改
        if (feg) {
            const res = await request.put(`/patrol/point`, { id: feg._id, state: arr })
            if (res.code == 200) {
                onClose()
            }


        } else {
            // 新增
            const res = await request.post(`/patrol/pointadd`, arr)
            if (res.code == 200) {
                onClose()
            }
        }
    }

    return (
        <>
            <Form
                style={{ maxWidth: "100%" }}
                onFinish={onFinish}
                form={form}
                name="myForm"
            >
                <Form.Item
                    label="巡查点名"
                    name='name'
                    rules={[
                        {
                            required: true,
                            message: 'Please input!',
                        },
                    ]}
                >
                    <Input onChange={(e) => setname(e.target.value)}></Input>
                </Form.Item>
                <Form.Item
                    label="所在位置"
                    name='position'
                    rules={[
                        {
                            required: true,
                            message: 'Please input!',
                        },
                    ]}
                >
                    <Input onChange={(e) => setposition(e.target.value)}></Input>
                </Form.Item>
                <Form.Item
                    label="巡查对象"
                    name='object'
                    rules={[
                        {
                            required: true,
                            message: 'Please input!',
                        },
                    ]}
                >
                    <Input onChange={(e) => setobject(e.target.value)}></Input>
                </Form.Item>
                <Form.Item
                    label="检查模板"
                    name='inspect'
                    rules={[
                        {
                            required: true,
                            message: 'Please input!',
                        },
                    ]}
                >
                    <Input onChange={(e) => setinspect(e.target.value)}></Input>
                </Form.Item>
                <Form.Item label="是否有效">
                    <Switch checked={state} onChange={(e) => { setstate(e ? 1 : 0) }} />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">{feg ? "修改" : "确定"}</Button>
                </Form.Item>
            </Form>
        </>
    )
}
