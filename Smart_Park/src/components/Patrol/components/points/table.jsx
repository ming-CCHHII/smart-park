import React, { useState, useEffect } from 'react'
import { Space, Table, Tag, Drawer, Button } from 'antd';
import Setadd from './setadd'
// 引入redux
import { useDispatch, useSelector } from 'react-redux';
import { setpatrol } from '../../../../store/module/data';
import request from "../../../../utils/request"

export default function table({ searchdata }) {
  const columns = [
    {
      title: `编号`,
      dataIndex: 'code',
      key: 'code'
    },
    {
      title: '巡查点名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '所在位置',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: "巡查对象",
      dataIndex: 'object',
      key: 'object',
    },
    {
      title: "检查模板",
      dataIndex: 'inspect',
      key: 'inspect',
    },
    {
      title: '创建时间',
      dataIndex: 'createtime',
      key: 'createtime',
      render: (_, { createtime }) => {
        return <span>{createtime.slice(0, 10)}</span>;
      }
    },
    {
      title: '是否有效',
      dataIndex: 'state',
      key: 'state',
      render: (_, { state }) => {
        return <Tag>
          <span>{!state == 0 ? '有效' : '关闭'}</span>
        </Tag>;
      }
    },
    {
      title: '二维码',
      dataIndex: 'img',
      key: 'img',
      render: (_, { img }) => {
        return <img src='https://img0.baidu.com/it/u=4138289147,3487448798&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500'></img>;
      }
    },
    {
      title: '操作',
      dataIndex: 'action',
      key: 'action',
      width: 200,
      render: (_, item) => (
        <Space size="middle">
          <a onClick={() => console.log(_id)}>打印二维码</a>
          <a onClick={() => setitem(item)}>编辑</a>
          <a onClick={() => console.log(_id)} style={{ color: "red" }}>删除</a>
        </Space>
      ),
    },
  ];
  const dispatch = useDispatch();
  const patrol = useSelector(state => state.patrol)
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [open, setOpen] = useState(false);
  const [feg, setFeg] = useState(null);

  // 分页
  const handlePageChange = (page, pageSize) => {
    setCurrent(page);
    setPageSize(pageSize);
  };

  // 编辑
  const setitem = (item) => {
    setOpen(true)
    setFeg(item);
  }
  // 添加
  const addQuest = () => {
    setOpen(true)
    setFeg("")
  }

  // 关闭
  const onClose = () => {
    setOpen(false);
    getRedux();
  };

  const getRedux = async (item) => {
    if (item) {
      const { data } = await request.get('/patrol/getpatrolSizeModel', {
        params: {
          name: item.name,
          object: item.object,
          inspect: item.inspect,
          state: item.state
        }
      });
      dispatch(setpatrol(data))
    }else{
      const { data } = await request.get('/patrol/getpatrolSizeModel');
      dispatch(setpatrol(data))
    }
  }

  useEffect(() => {
    if (searchdata) {
      getRedux(searchdata)
    }
  }, [searchdata])

  return (
    <>
      <Button type="primary" onClick={() => addQuest()}>新增任务</Button>
      <Table pagination={{
        current,
        pageSize,
        total: patrol.length,
        onChange: handlePageChange,
        showSizeChanger: true,
        pageSizeOptions: ['5', '10', '15'],
      }}
        scroll={{
          y: 300
        }}
        columns={columns} dataSource={patrol.map(item => ({ ...item, key: item._id }))} />
      <Drawer size="default" title="巡查点编辑" onClose={onClose} open={open}>
        <div style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'column' }}>
          <Setadd feg={feg} open={open} onClose={onClose}></Setadd>
        </div>
      </Drawer>
    </>

  )
}
