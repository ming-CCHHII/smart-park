import React, {useEffect} from 'react'
import { Input, Form, Space, Button, Cascader } from "antd";
import request from '../../../../utils/request';
export default function form({ getdata }) {
  const [form] = Form.useForm();
  const [name, setName] = React.useState(""); // 巡查人
  const [typeid, setTypeid] = React.useState(""); // 部门
  const [positionid, setPositionid] = React.useState(""); // 职位
  const [address, setAddress] = React.useState(""); // 地址

  const [typs, setTypes] = React.useState([]); // 部门列表
  const [positions, setPositions] = React.useState([]); // 职位列表

  const onReset = () => {
    form.resetFields();
    setName("");
    setTypeid("");
    setPositionid("");
    setAddress("");
    getdata()
  }
  const onSubmit = () => {
    let data = {
      name: name,
      typeid: typeid,
      positionid: positionid,
      address: address
    }
    getdata(data)
  }

  const getTP = async ()=>{
    let type = await request.get('/patrol/getpersonnelTypeModel')
    let posi = await request.get("/patrol/getpositionModel")
    setTypes(type.data)
    setPositions(posi.data)
  }

  useEffect(() => {
    getTP()
  }, [])

  return (
    <>
      <Form form={form} style={{ display: "flex", justifyContent: "space-between", flexWrap: "wrap" }}>
        <Form.Item
          label="巡查人: "
          name="name"
        >
          <Input placeholder="请输入" style={{ width: "13rem" }} value={name} onChange={(e) => setName(e.target.value)} />
        </Form.Item>
        <Form.Item
          label="部门: "
          name="person"
        >
          <Cascader options={typs} style={{ width: "13rem" }} onChange={(e) => setTypeid(e[0])} placeholder="请选择" />
        </Form.Item>
        <Form.Item
          label="职位: "
          name="plan"
        >
          <Cascader options={positions} style={{ width: "13rem" }} onChange={(e) => setPositionid(e[0])} placeholder="请选择" />
        </Form.Item>
        <Form.Item
          label="地址: "
          name="finish"
        >
          <Input placeholder='请输入' style={{ width: "13rem" }} value={address} onChange={(e) => setAddress(e.target.value)}/>
        </Form.Item>
      </Form>
      <Space style={{ marginLeft: "5rem" }}>
        <Button type="primary" htmlType="submit" onClick={(e) => onSubmit()}>
          搜索
        </Button>
        <Button htmlType="button" onClick={() => onReset()}>
          重置
        </Button>
      </Space>
    </>
  )
}
