import React, { useState } from 'react'
import { Space, Table, Tag, Drawer, Button, message } from 'antd';
import request from '../../../../utils/request';
import Drawers from "./drawers"
export default function table({ data, getdatas }) {
    const columns = [
        {
            title: '巡查人',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '职位',
            dataIndex: 'positionid',
            key: 'positionid',
            render: (_, { positionid }) => {
                return <span>
                    {positionid.position}
                </span>
            }
        },
        {
            title: "联系方式",
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: "地址",
            dataIndex: 'address',
            key: 'address'
        },
        {
            title: "部门",
            dataIndex: 'typeid',
            key: 'typeid',
            render: (_, {typeid})=>{
                return <span>
                    {typeid.type}
                </span>
            }
        },
        // {
        //     title: '照片',
        //     dataIndex: 'time',
        //     key: 'time',
        //     render: (_, { time }) => {
        //         return <span>{time + "小时"}</span>;
        //     }
        // },
        // {
        //     title: '创建时间',
        //     dataIndex: 'state',
        //     key: 'state',
        //     render: (_, { state }) => {
        //         return <Tag>
        //             <span>{state == 1 ? '有效' : '关闭'}</span>
        //         </Tag>;
        //     }
        // },
        {
            title: '操作',
            dataIndex: 'action',
            key: 'action',
            width: 200,
            render: (_, item) => (
                <Space size="middle">
                    <a onClick={() => delitem(item._id)} style={{ color: "red" }}>删除</a>
                </Space>
            ),
        },
    ];
    const [messageApi, contextHolder] = message.useMessage(); // 消息
    const [current, setCurrent] = useState(1);
    const [pageSize, setPageSize] = useState(5);
    const [open, setOpen] = useState(false);
    const [item, setItems] = useState(null);
    // 分页
    const handlePageChange = (page, pageSize) => {
        setCurrent(page);
        setPageSize(pageSize);
    };

    // 开启添加
    const addQuest = () => {
        setOpen(true);
        setItems()
    }

    // 关闭添加
    const onClose = () => {
        setOpen(false);
    }

    // 重新获取数据
    const getdata = () => {
        getdatas();
    }

    // 删除
    const delitem = async (id) => {
        const res = await request.delete('/patrol/delpatrolPlanModel', {
            params: {
                _id: id
            }
        })
        if (res.code == 200) {
            getdatas();
            messageApi.success(res.msg)
        }
    }
    return (
        <>
            {contextHolder}
            <Button type="primary" onClick={() => addQuest()}>新增任务</Button>
            <Table pagination={{
                current,
                pageSize,
                total: data.length,
                onChange: handlePageChange,
                showSizeChanger: true,
                pageSizeOptions: ['5', '10', '15'],
            }}
                scroll={{
                    y: 300
                }}
                columns={columns} dataSource={data.map(item => ({ ...item, key: item._id }))} />
            <Drawer size="default" title="新增任务" onClose={onClose} open={open}>
                <Drawers setonClose={onClose} getplain={getdata} data={data}></Drawers>
            </Drawer>

        </>
    )
}
