import React, { useState, useEffect } from 'react'
import { Form, Input, Button, Cascader, message } from 'antd'
import { useSelector } from "react-redux"
export default function drawers({ data, setonClose, getplain }) {
  const [datas, setDatas] = useState([])
  const [arr, setArr] = useState({})
  const options = useSelector((state) => state.options);
  const changes = (e) => {
    let id = e[1].split('/')[1]
    let bm = e[0]
    datas.forEach(item => {
      if (item._id == id) {
        setArr({ ...item, bm: bm })
      }
    })
  }
  useEffect(() => {
    setDatas(data)
  }, [data])
  return (
    <>
      <Form>
        <Form.Item label="选择人员: ">
          <Cascader options={options} onChange={(e) => changes(e)} placeholder="请选择" />
        </Form.Item>
      </Form>
      {/* arr不为空进行渲染 */}
      {Object.keys(arr).length === 0 ?
        <p>No user </p>
        :
        <div style={{ width: "200px", height: "300px", margin: "0 auto", display: "flex", justifyContent: "space-between", flexDirection: "column", }}>
          <p>姓名: {arr.name}</p>
          <p>部门: {arr.bm}</p>
          <p>联系方式: {arr.phone}</p>
          <p>图片:</p>
          <img style={{ width: "100px", height: "100px"}} src="https://cdn7.axureshop.com/demo2024/2291721/images/%E8%AE%BF%E5%AE%A2%E7%94%B3%E8%AF%B7%E7%AE%A1%E7%90%86/u4970.svg"></img>
          <Button type="primary" onClick={() => {setonClose(); message.success("已发送");}}>添加</Button>
        </div>
      }
    </>
  )
}
