import React, { useState, useEffect } from 'react';
import { Transfer } from 'antd';
import request from '../../../../utils/request';

const App = ({onsetpatrol, source}) => {
    // console.log(source)
    const [mockData, setMockData] = useState([])
    const [targetKeys, setTargetKeys] = useState([]);
    const [selectedKeys, setSelectedKeys] = useState([]);
    const onChange = (nextTargetKeys, direction, moveKeys) => {
        const typ = mockData.filter((item) => item.key == moveKeys[0])[0].inspect
        onsetpatrol(moveKeys, typ)
        setTargetKeys(nextTargetKeys)
    };
    const onSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
        setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
    };
    // 获取数据
    const getData = async () => {
        const res = await request.get('/patrol/getpatrolSizeModel'); // 请求接口
        const arr = []
        res.data.forEach((item, index) => {
            arr.push({
                key: item._id,
                title: item.name,
                description: item.position,
                inspect: item.inspect,
            })
        })
        setMockData(arr)
    };
    useEffect(() => {
        getData();
    }, []);
    return (
        <Transfer
            dataSource={mockData} // 数据源
            titles={['所有', '已选择']} // 标题
            targetKeys={targetKeys} // 目标列表
            selectedKeys={selectedKeys}  // 选中项
            onChange={onChange} // 列表变化
            onSelectChange={onSelectChange} // 选中项变化
            render={(item) => item.title} // 自定义渲染
        />
    );
};
export default App;