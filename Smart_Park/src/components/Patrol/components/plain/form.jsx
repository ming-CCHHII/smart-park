import React from 'react'
import { Input, Form, Space, Button, Cascader } from "antd";
export default function form({ getdata }) {
    const [form] = Form.useForm();
    const options = [
        {
            value: 1,
            label: '有效',
        },
        {
            value: 0,
            label: '无效',
        },
    ];
    const cycles = [
        {
            value: "一天一次",
            label: '一天一次',
        },
        {
            value: "两天一次",
            label: "两天一次",
        }
    ]
    const [name, setName] = React.useState(""); // 计划名称
    const [cycle, setCycle] = React.useState(""); // 任务周期
    const [Taskpersonnel, setTaskpersonnel] = React.useState(""); // 执行人员
    const [state, setState] = React.useState(""); // 是否有效
    const onReset = () => {
        form.resetFields();
        setName("");
        setCycle("");
        setTaskpersonnel("");
        setState("");
        getdata()
    }
    const onSubmit = () => {
        let data = {
            name: name,
            cycle: cycle,
            Taskpersonnel: Taskpersonnel,
            state: state
        }
        getdata(data)
    }
    return (
        <>
            <Form form={form} style={{ display: "flex", justifyContent: "space-between", flexWrap: "wrap" }}>
                <Form.Item
                    label="计划名称: "
                    name="name"
                >
                    <Input placeholder="请输入" style={{ width: "13rem" }} value={name} onChange={(e) => setName(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="任务周期"
                    name="person"
                >
                    <Cascader options={cycles} style={{ width: "13rem" }} onChange={(e) => setCycle(e[0])} placeholder="请选择" />
                </Form.Item>
                <Form.Item
                    label="执行人员: "
                    name="plan"
                >
                    <Input placeholder="请输入" style={{ width: "13rem" }} value={Taskpersonnel} onChange={(e) => setTaskpersonnel(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="是否有效: "
                    name="finish"
                >
                    <Cascader options={options} style={{ width: "13rem" }} onChange={(e) => setState(e[0])} placeholder="请选择" />
                </Form.Item>
            </Form>
            <Space style={{ marginLeft: "5rem" }}>
                <Button type="primary" htmlType="submit" onClick={(e) => onSubmit()}>
                    搜索
                </Button>
                <Button htmlType="button" onClick={() => onReset()}>
                    重置
                </Button>
            </Space>
        </>
    )
}
