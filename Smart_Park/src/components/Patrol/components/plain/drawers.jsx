import React, { useEffect, useState } from 'react';
import { Button, Form, Input, Cascader, DatePicker, Space, message, Switch } from 'antd';
import request from '../../../../utils/request';
import Transfers from './transfer';
import dayjs from 'dayjs';
export default function Drawer({ setonClose, getplain, item, open }) {
    const { RangePicker } = DatePicker;
    const cycles = [
        {
            value: "一天一次",
            label: '一天一次',
        },
        {
            value: "两天一次",
            label: "两天一次",
        }
    ]
    const [options, setOptions] = useState([]);
    const [form] = Form.useForm()
    const [messageApi, contextHolder] = message.useMessage();
    const [name, setName] = useState(null); // 计划名称
    const [cycle, setCycle] = useState(null); // 任务周期
    const [Taskpersonnel, setTaskpersonnel] = useState(null); // 执行人员
    const [startimeout, setStartimeout] = useState(null); // 计划有效期
    const [time, setTime] = useState(null); // 任务执行时间
    const [state, setstate] = useState(0); // 是否有效
    const [source, setSource] = useState([]); // 巡查点

    // 获取执行人员
    const getpersonnelTypeModel = async () => {
        const res = await request.get('/patrol/getpersonnelTypeModel')
        setOptions(res.data)
    }

    // 子组件传值巡查点
    const onsetpatrol = (moveKeys, typ) => {
        // 巡查点
        setSource(moveKeys) // []
        // 巡查模板
        // console.log(typ) // string
    }

    // 提交表单
    const onFinish = async () => {
        const id = item?._id
        if (source.length > 0) {
            const res = await request.post('/patrol/addpatrolPlanModel', {
                id: id,
                arr: {
                    name: name,
                    cycle: cycle,
                    Taskpersonnel: Taskpersonnel,
                    startimeout: startimeout,
                    time: time,
                    state: state,
                    patrols: source
                }
            })
            if (res.code == 200) {
                messageApi.success(res.msg)
                setonClose()
                onReset()
                getplain()
            }
        } else {
            messageApi.open({
                type: 'error',
                content: '添加失败',
            });
        }

    };

    // 重置表单
    const onReset = () => {
        form.resetFields();
        // 清空表单
        setName(null)
        setCycle(null)
        setTaskpersonnel(null)
        setStartimeout(null)
        setTime(null)
        setstate(0)
        setSource([])
    };

    // 修改回显
    const setItem = (item) => {
        form.setFieldsValue({ name: item.name, cycle: item.cycle, startimeout: [dayjs(item.startimeout.split('-')[0]), dayjs(item.startimeout.split('-')[1])], Taskpersonnel: item.Taskpersonnel, time: item.time, state: item.state, source: item.patrols }); // 设置初始值
        setName(item.name)
        setCycle(item.cycle)
        setTaskpersonnel(item.Taskpersonnel)
        setStartimeout(item.startimeout)
        setTime(item.time)
        setstate(item.state)
        setSource(item.patrols)
    }

    useEffect(() => {
        getpersonnelTypeModel()

        // 修改回显
        if (item != null) {
            setItem(item)
        }

        // 关闭抽屉, 清空数据
        if (open == false) {
            onReset()
        }
    }, [item, open])


    return (
        <>
            {contextHolder}
            <Form
                form={form}
                name="basic"
                onFinish={onFinish}
                style={{
                    padding: '0px 40px',
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'space-between',
                    boxSizing: 'border-box',
                    height: '500px'
                }}
            >
                <Form.Item
                    label="计划名称"
                    name="name"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Input placeholder='请输入' value={name} onChange={(e) => setName(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="任务周期"
                    name="cycle"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Cascader options={cycles} style={{ width: "13rem" }} onChange={(e) => setCycle(e[0])} placeholder="请选择" />
                </Form.Item>
                <Form.Item
                    label="执行人员"
                    name="Taskpersonnel"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}

                >
                    <Cascader options={options} onChange={(e) => setTaskpersonnel(e[1])} placeholder="请选择" />
                </Form.Item>
                <Form.Item
                    label="计划有效期"
                    name="startimeout"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <RangePicker
                        showTime
                        placeholder="请选择日期时间"
                        onChange={(value, dateString) => {
                            setStartimeout(dateString[0] + '-' + dateString[1])
                        }}
                    />
                </Form.Item>
                <Form.Item
                    label="任务执行时间"
                    name="time"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Input placeholder='请输入小时' value={time} onChange={(e) => setTime(e.target.value)} />
                </Form.Item>
                <Form.Item
                    label="是否有效"
                    name="state"
                    layout="vertical"
                    style={{ width: '250px' }}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your username!',
                        },
                    ]}
                >
                    <Switch onChange={(e) => { setstate(e ? 1 : 0) }} />
                </Form.Item>
                <Form.Item
                    label="选择巡查点"
                    name="source"
                    layout="vertical"
                    style={{ height: '250px' }}
                >
                    <Transfers onsetpatrol={onsetpatrol} source={source}></Transfers>
                </Form.Item>
            </Form>
            <Space style={{ marginLeft: "5rem" }}>
                <Button type="primary" onClick={() => form.submit()} htmlType="submit">
                    Submit
                </Button>
                <Button htmlType="button" onClick={() => onReset()}>
                    重置
                </Button>
            </Space>
        </>
    );
}