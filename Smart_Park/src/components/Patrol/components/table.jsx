import React, { useState } from 'react';
import { Space, Table, Tag, ConfigProvider, Drawer } from 'antd';
import request from '../../../utils/request';

export default function Tables({ datas, getpatrol }) {
  const onClose = () => {
    setOpen(false);
  };

  const columns = [
    {
      title: `任务名称`,
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: '任务周期',
      dataIndex: 'cycle',
      key: 'cycle',
    },
    {
      title: '执行人员',
      dataIndex: 'Taskpersonnel',
      key: 'Taskpersonnel',
    },
    {
      title: "来源计划",
      dataIndex: 'source',
      key: 'source',
    },
    {
      title: '执行时间',
      dataIndex: 'startime',
      key: 'startime',
    },
    {
      title: '状态',
      dataIndex: 'state',
      key: 'state',
      render: (_, { state }) => {
        let color = state == 0 ? 'red' : 'green';
        return <Tag color={color}>
          <span>{!state == 0 ? '已完成' : '未完成'}</span>
        </Tag>;
      }
    },
    {
      title: '操作',
      dataIndex: 'action',
      key: 'action',
      render: (_, { _id }) => (
        <Space size="middle">
          <a onClick={() => xiangqing(_id)}>任务详情</a>
          <a onClick={() => delpatrols(_id)} style={{ color: "red" }}>删除</a>
        </Space>
      ),
    },
  ];
  
  const columns1 = [
    {
      title: '任务名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '巡查点',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: '巡查对象',
      dataIndex: 'object',
      key: 'object',
    },
    {
      title: '巡查项目',
      dataIndex: 'startime',
      key: 'startime',

    },
    {
      title: '巡查时间',
      dataIndex: 'createtime',
      key: 'createtime'
    },
    {
      title: '巡查结果',
      dataIndex: 'state',
      key: 'state',
      render: (_, { state }) => {
        let color = state == 0 ? 'red' : 'green';
        return <Tag color={color}>
          <span>{!state == 0 ? '已完成' : '未完成'}</span>
        </Tag>;
      }
    }

  ]
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [open, setOpen] = useState(false);
  const [data, setData] = useState({});
  const [patrols, setPatrols] = useState([]);

  const handlePageChange = (page, pageSize) => {
    setCurrent(page);
    setPageSize(pageSize);
  };
  
  const xiangqing = async (id) => {
    // 所有任务类型
    const { data } = await request.get('/patrol/getpatrolSizeModel'); // 请求接口
    
    // 根据id查找对应的任务
    let arr
    datas.forEach(item => {
      if (item._id == id) {
        arr = item
      }
    })

    // 任务id数组
    let { patrols } = arr;

    // 根据patrols的值去data中查找对应的任务
    let a = []
    patrols.forEach(item => {
      data.forEach(item1 => {
        if (item == item1._id) {
          a.push(item1)
        }
      })
    })
    setPatrols(a);
    setData(arr);
    setOpen(true);
  }

  const delpatrols = async (id) => {
    await request.delete(`/patrol/delpatrols/?_id=${id}`);
    getpatrol()
  }
  return (
    <ConfigProvider
      theme={{
        components: {
          Table: {
            headerBg: 'rgb(245, 246, 251)'
          },
        },
      }}
    >
      <Table pagination={{
        current,
        pageSize,
        total: datas.length,
        onChange: handlePageChange,
        showSizeChanger: true,
        pageSizeOptions: ['5', '10', '15'],
      }}
        scroll={{
          y: 300
        }}
        columns={columns} dataSource={datas.map(item => ({ ...item, key: item._id }))} />
      <Drawer size="large" title="任务详情" onClose={onClose} open={open}>
        <div style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'column' }}>
          <div style={{ display: 'flex', justifyContent: 'space-between', padding: '10px 90px', boxSizing: 'border-box', height: "30%" }}>
            <div style={{ height: "100%", display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
              <p>任务名称:&nbsp;&nbsp;&nbsp;&nbsp; {data.name}</p>
              <p>来源计划:&nbsp;&nbsp;&nbsp;&nbsp; {data.source}</p>
              <p>执行时间:&nbsp;&nbsp;&nbsp;&nbsp; {data.startime}</p>
            </div>
            <div style={{ height: "100%", display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
              <p>任务周期:&nbsp;&nbsp;&nbsp;&nbsp; {data.cycle}</p>
              <p>执行人员:&nbsp;&nbsp;&nbsp;&nbsp; {data.Taskpersonnel}</p>
              <p>完成情况:&nbsp;&nbsp;&nbsp;&nbsp; {data.state ? '已完成' : '未完成'}</p>
            </div>
          </div>
          <div style={{ height: "70%", overflowY: 'scroll' }}>
            <Table pagination={false} columns={columns1} dataSource={patrols.map(item => ({ ...item, key: item._id }))} />
          </div>
        </div>
      </Drawer>

    </ConfigProvider>)
}