import React, { useEffect, useState } from 'react'
import style from "../../assets/patrolcss/task.module.css"

import Frimt from "./components/hiddens/form";
import Table from "./components/hiddens/table";
import request from '../../utils/request';
// 引入redux
import { useDispatch, useSelector } from 'react-redux';
import { sethidden } from '../../store/module/data';

export default function hidden() {
  const dispatch = useDispatch();
  const data = useSelector(state => state.hidden)
  // 获取数据
  const gettabledata = async (item) => {
    const res = await request.post('/patrol/getpatrolHiddenModel', item)
    if (res.code == 200) {
      dispatch(sethidden(res.data))
    }
  }
  useEffect(() => {
    gettabledata()
  }, [])
  return (
    <div className={style.body}>
      <div className={style.big}>
        <Frimt getdata ={gettabledata}></Frimt>
      </div>
      <div className={style.simmll}>
        <Table data={data}></Table>
      </div>
    </div>
  )
}
