import React, { useEffect, useState } from 'react'
import Top from './Top'
import Footer from './Footer'
import EditModal from './EditModal'

export default function Index() {
  const [energyType, setEnergyType] = useState([]) //能耗类型
  const [energyCycle, setEnergyCycle] = useState([]);//能耗周期
  const [energyDevice, setEnergyDevice] = useState([]);//能耗设备
  const [upId, setUpId] = useState("")
  const [upItem, setUpItem] = useState([])
  //search
  const [sname, setSname] = useState('')
  const [setid, setSetid] = useState('')

  const [openEdit, setOpenEdit] = useState(false) //编辑弹窗

  const handleAction = (action) => {
    setEnergyType(action)
  }

  const receiveParams = (params) => {
    setSname(params.sname)
    setSetid(params.setid)
    console.log(params)
  }

  const getCycleLs = (action) => {
    setEnergyCycle(action)
  }

  const getDevicels = (action) => {
    setEnergyDevice(action)
  }

  const changeOpenEdit = (val, id, item) => {
    setOpenEdit(val)
    setUpId(id)
    // 深拷贝
    let itemCopy = JSON.parse(JSON.stringify(item))
    setUpItem(itemCopy)
  }


  const handleReset = () => {
    setSname('');
    setSetid('');
  };

  return (
    <div>

      <Top onAction={handleAction} receiveParams={receiveParams} onReset={handleReset} />
      <Footer actionmsg={energyType} getCycleLs={getCycleLs} getDevicels={getDevicels} changeOpenEdit={changeOpenEdit} sname={sname} setid={setid}/>
      <EditModal typels={energyType} cyclels={energyCycle} devicels={energyDevice} openEdit={openEdit} changeOpenEdit={changeOpenEdit} upId={upId} upItem={upItem} />
    </div>
  )
}
