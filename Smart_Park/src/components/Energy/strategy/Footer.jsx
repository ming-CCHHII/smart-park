import React, { useCallback, useEffect, useState } from 'react'
import style from '../../../assets/css/warn.module.css'
import { Input, Select, Drawer, Transfer, message, Pagination } from 'antd'
import request from '../../../utils/request'
import dayjs from 'dayjs'

const { TextArea } = Input;

export default function Footer({ actionmsg, getCycleLs, getDevicels, changeOpenEdit, setid, sname }) {
    const columns = ['预警名称', '描述', '能耗类型', '预警周期', '阈值', '创建时间', '操作']
    const [energyls, setEnergyls] = useState([]) // 能耗资源
    const [energyCycle, setEnergyCycle] = useState([]);//能耗周期
    const [energyDevice, setEnergyDevice] = useState([]);//能耗设备
    const [isLoading, setIsLoading] = useState(true);


    //drawer
    const [open, setOpen] = useState(false);
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };

    //select选择器
    const handleChangeType = (value) => {
        setEtId(value)
    }
    const handleChangeCycle = (value) => {
        setEcId(value)
    }

    //获取能耗周期
    const getEnergyCycle = async () => {
        let { data } = await request.get('/energy/getenergyCycle')
        setEnergyCycle(data)
        getCycleLs(data)
    }
    //获取能耗设备
    const getEnergyDevice = async () => {
        let { data } = await request.get('/energy/getenergyDevice')
        let arr = []
        data.forEach(item => {
            arr.push({
                key: item._id,
                title: item.name,
                image: item.image
            })
        })
        setEnergyDevice(arr)
        getDevicels(arr)
    }

    //add
    const [energyname, setEnergyname] = useState('');//预警名称
    const [describe, setDescribe] = useState('');//预警描述
    const [result, setResult] = useState('');//预警结果
    const [threshold, setThreshold] = useState(0);//阈值
    const [etid, setEtId] = useState('');//能耗类型id
    const [ecid, setEcId] = useState('');//能耗周期id

    //穿梭框
    //右侧框数据的key集合
    const [targetKeys, setTargetKeys] = useState([]);
    //选中的项
    const [selectedKeys, setSelectedKeys] = useState([]);

    const onChange1 = (nextTargetKeys, direction, moveKeys) => {
        setTargetKeys(nextTargetKeys);
    };
    const onSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
        setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
    };

    const render = (item) => (
        <>
            <p style={{ display: 'flex', alignItems: 'center' }}>
                <img src={item.image} style={{ width: '17px', height: '17px', marginRight: '10px' }} />
                <span>{item.title}</span>
            </p>

        </>
    );
    const [messageApi, contextHolder] = message.useMessage(); //提示框
    //添加能耗预警
    const addEnergyWarning = async () => {
        let obj = {
            name: energyname,
            describe: describe,
            time: Date.now(),
            result: result,
            threshold: threshold,
            etid: etid,
            ecid: ecid,
            deviceid: targetKeys
        }
        let { code } = await request.post('/energy/addenergyWarn', obj)
        if (code === 200) {

            messageApi.open({
                type: 'success',
                content: '新增成功',
            });
            getEnergyWarning();
            setEnergyname("")
            setDescribe("")
            setThreshold("")
            setEcId("")
            setEtId("")
            setTargetKeys([])
            setOpen(false);

        }

    }

    //获取能耗预警
    const getEnergyWarning = async () => {
        try {
            let { data } = await request.get(`/energy/getenergyWarn/?sname=${sname}&setid=${setid}`)
            setEnergyls(data)
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsLoading(false);
        }

    }


    //删除能耗预警
    const deleteEnergy = async (id) => {
        let { code } = await request.delete(`/energy/deleteenergyWarn?id=${id}`)
        if (code === 200) {
            messageApi.open({
                type: 'success',
                content: '删除成功',
            });
            getEnergyWarning();
        }
    }

    useEffect(() => {
        getEnergyCycle()
        getEnergyDevice()
        getEnergyWarning()
    }, [sname, setid])
    return (
        <>
            {contextHolder}
            <div className={style.footer_card}>

                <button className={style.add_btn} onClick={showDrawer}>新增策略</button>
                <Drawer title="新增策略" onClose={onClose} open={open} size="large">
                    <div className={style.drawer_box}>
                        <div className={style.drawer_item}>
                            <p style={{
                                marginBottom: '15px'
                            }}><span style={{
                                color: 'red',
                                marginRight: '5px'
                            }}>*</span>预警名称</p>
                            <Input placeholder="请输入" style={{ width: '260px' }} value={energyname} onChange={(e) => {
                                setEnergyname(e.target.value)
                            }} />
                        </div>
                        <div className={style.drawer_item}>
                            <p style={{
                                marginBottom: '15px'
                            }}><span style={{
                                color: 'red',
                                marginRight: '5px'
                            }}>*</span>能耗类型</p>
                            <Select
                                placeholder="请选择"
                                style={{
                                    width: '260px',
                                    marginRight: '5px'
                                }}
                                onChange={handleChangeType}
                                options={actionmsg}
                                fieldNames={{ label: 'name', value: '_id' }}
                            />
                        </div>
                        <div className={style.drawer_item}>
                            <p style={{
                                marginBottom: '15px',
                                paddingLeft: '5px',
                                boxSizing: 'border-box'
                            }}>描述</p>
                            <TextArea rows={5} cols={35} placeholder="请输入" value={describe} onChange={(e) => {
                                setDescribe(e.target.value)
                            }} />
                        </div>
                        <div className={style.drawer_item}>
                            <p style={{
                                marginTop: '5px',
                                marginBottom: '15px'
                            }}><span style={{
                                color: 'red',
                                marginRight: '5px'
                            }}>*</span>预警周期：</p>
                            <Select
                                placeholder="请选择"
                                style={{
                                    width: '260px',
                                    marginRight: '5px'
                                }}
                                onChange={handleChangeCycle}
                                options={energyCycle}
                                fieldNames={{ label: 'name', value: '_id' }}
                            />
                            <p style={{
                                marginTop: '20px',
                                marginBottom: '15px'
                            }}><span style={{
                                color: 'red',
                                marginRight: '5px'
                            }}>*</span>阈值：</p>
                            <Input placeholder="请输入" style={{ width: '260px' }} value={threshold} onChange={(e) => {
                                setThreshold(e.target.value)
                            }} />
                        </div>
                    </div>
                    <p className={style.drawer_p}><span style={{
                        color: 'red',
                        marginRight: '5px'
                    }}>*</span>选择能耗设备</p>
                    <Transfer
                        dataSource={energyDevice}
                        targetKeys={targetKeys}
                        selectedKeys={selectedKeys}
                        onChange={onChange1}
                        onSelectChange={onSelectChange}
                        render={render}
                        style={{
                            padding: '7px 20px',
                            boxSizing: 'border-box'
                        }}
                    />
                    <div className={style.drawer_bottom}>
                        <button className={style.drawer_sure} onClick={addEnergyWarning}>确认</button>
                        <button className={style.drawer_cancel} onClick={onClose}>取消</button>
                    </div>
                </Drawer>
                <table className={style.tb}>
                    <thead>
                        <tr style={{
                            backgroundColor: '#f5f6fb'
                        }}>
                            {columns.map((item, index) => {
                                return <td key={index}>{item}</td>
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {isLoading ? (
                            <tr>
                                <td colSpan={7} style={{ textAlign: 'center', color: 'gray' }}>Loading...</td>
                            </tr>
                        ) : (
                            Array.isArray(energyls) && energyls.length > 0 ? (
                                energyls.map((item) => (
                                    <tr key={item._id}>
                                        <td>{item.name}</td>
                                        <td>{item.describe}</td>
                                        <td>{item.etid.name}</td>
                                        <td>{item.ecid.name}</td>
                                        <td>{item.threshold}</td>
                                        <td>{dayjs(item.time).format('YYYY-MM-DD HH:mm:ss')}</td>
                                        <td>
                                            <button className={style.editbtn} onClick={() => { changeOpenEdit(true, item._id, item) }}>编辑</button>
                                            <button className={style.delbtn} onClick={() => { deleteEnergy(item._id) }}>删除</button>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan={7} style={{ textAlign: 'center', color: 'gray' }}>No data available</td>
                                </tr>
                            )
                        )}
                    </tbody>
                </table>
                <div className={style.pagebox}>
                    <Pagination
                        total={energyls.length}
                        showSizeChanger
                        showQuickJumper
                        showTotal={(total) => `共 ${total} 条`}
                    />
                </div>

            </div>
        </>

    )
}
