import React, { useEffect, useState } from 'react'
import style from '../../../assets/css/warn.module.css'
import { Input, Select } from 'antd'
import request from '../../../utils/request'

export default function Top({ onAction, receiveParams,onReset }) {
    const [energyType, setEnergyType] = useState([])

    //search
    const [sname, setSname] = useState('')
    const [setid, setSetid] = useState('')

    const handleChange = (value) => {
        setSetid(value)
    }

    const search = () => {
        receiveParams({ sname, setid })
    }


    //获取能耗类型
    const getEnergyType = async () => {
        let { data } = await request.get('/energy/getenergyType')
        setEnergyType(data)
        onAction(data)
    }
    useEffect(() => {
        getEnergyType();
    }, [])
    return (
        <div className={style.top_card}>
                <span>预警名称：</span>
                <Input placeholder="请输入" style={{
                    width: '230px',
                    marginRight: '100px'
                }} value={sname} onChange={(e) => setSname(e.target.value)} />
                <span>能耗类型：</span>
                <Select
                    placeholder="请选择"
                    style={{
                        width: '230px',
                        marginRight: '5px'
                    }}
                    onChange={handleChange}
                    options={energyType}
                    fieldNames={{ label: 'name', value: '_id' }}
                />
                <button className={style.search_btn} onClick={search}>搜索</button>
                <button className={style.reset_btn} onClick={()=>{
                    setSname('')
                    setSetid('')
                    onReset()
                }}>重置</button>
        </div>
    )
}
