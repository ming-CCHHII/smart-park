import React, { useState, useEffect } from 'react'
import { Drawer, Input, Select, Transfer } from 'antd';
import style from '../../../assets/css/warn.module.css'
import request from '../../../utils/request'
import dayjs from 'dayjs'

const { TextArea } = Input;
export default function EditModal({ openEdit, upId, upItem,typels,cyclels ,devicels,changeOpenEdit}) {
    //update
    const [editname, setEditname] = useState('');//预警名称
    const [editdescribe, setEditDescribe] = useState('');//预警描述
    const [editresult, setEditResult] = useState('');//预警结果
    const [editthreshold, setEditThreshold] = useState(0);//阈值
    const [upetid, setUpEtId] = useState('');//能耗类型id
    const [upecid, setUpEcId] = useState('');//能耗周期id
    const [updeviceid, setUpDeviceId] = useState([]);//设备id

    //select选择器
    const handleChangeType = (value) => {
        setUpEtId(value)
    }
    const handleChangeCycle = (value) => {
        setUpEcId(value)
    }

     //穿梭框
    //右侧框数据的key集合
    const [targetKeys, setTargetKeys] = useState([]);
    //选中的项
    const [selectedKeys, setSelectedKeys] = useState([]);

    const onChange1 = (nextTargetKeys, direction, moveKeys) => {
        setTargetKeys(nextTargetKeys);
    };
    const onSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
        setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
    };

    const render = (item) => (
        <>
            <p style={{ display: 'flex', alignItems: 'center' }}>
                <img src={item.image} style={{ width: '17px', height: '17px', marginRight: '10px' }} />
                <span>{item.title}</span>
            </p>

        </>
    );

    //编辑
    const edit = async()=>{
       let upObj = {
        name: editname,
        describe: editdescribe,
        time: Date.now(),
        result: editresult,
        threshold: editthreshold,
        etid: upetid,
        ecid: upecid,
        deviceid: targetKeys
       } 
        let res=await request.post(`/energy/updateenergyWarn?id=${upId}`,upObj)
        console.log(res)
    }

    useEffect(()=>{
        if(upItem){
            const initialValue = upItem.etid ? upItem.etid._id : null;
            const initialValue1 = upItem.ecid ? upItem.ecid._id : null;
            setEditname(upItem.name)
            setEditDescribe(upItem.describe)
            setEditThreshold(upItem.threshold)
            setTargetKeys(upItem.deviceid || [])
            setUpEtId(initialValue)
            setUpEcId(initialValue1)
        }
    },[upItem])


    return (
        <Drawer title="编辑策略" open={openEdit} onClose={()=>{changeOpenEdit(false)}} size="large">
            <div className={style.drawer_box}>
                <div className={style.drawer_item}>
                    <p style={{
                        marginBottom: '15px'
                    }}><span style={{
                        color: 'red',
                        marginRight: '5px'
                    }}>*</span>预警名称</p>
                    <Input placeholder="请输入" style={{ width: '260px' }} value={editname} onChange={(e) => {
                        setEditname(e.target.value)
                    }} />
                </div>
                <div className={style.drawer_item}>
                    <p style={{
                        marginBottom: '15px'
                    }}><span style={{
                        color: 'red',
                        marginRight: '5px'
                    }}>*</span>能耗类型</p>
                    <Select
                        placeholder="请选择"
                        style={{
                            width: '260px',
                            marginRight: '5px'
                        }}
                        onChange={handleChangeType}
                        options={typels}
                        fieldNames={{ label: 'name', value: '_id' }}
                        value={upetid}
                    />
                </div>
                <div className={style.drawer_item}>
                    <p style={{
                        marginBottom: '15px',
                        paddingLeft: '5px',
                        boxSizing: 'border-box'
                    }}>描述</p>
                    <TextArea rows={5} cols={35} placeholder="请输入" value={editdescribe} onChange={(e) => {
                        setEditDescribe(e.target.value)
                    }} />
                </div>
                <div className={style.drawer_item}>
                    <p style={{
                        marginTop: '5px',
                        marginBottom: '15px'
                    }}><span style={{
                        color: 'red',
                        marginRight: '5px'
                    }}>*</span>预警周期：</p>
                    <Select
                        placeholder="请选择"
                        style={{
                            width: '260px',
                            marginRight: '5px'
                        }}
                        onChange={handleChangeCycle}
                        options={cyclels}
                        fieldNames={{ label: 'name', value: '_id' }}
                        value={upecid}
                    />
                    <p style={{
                        marginTop: '20px',
                        marginBottom: '15px'
                    }}><span style={{
                        color: 'red',
                        marginRight: '5px'
                    }}>*</span>阈值：</p>
                    <Input placeholder="请输入" style={{ width: '260px' }} value={editthreshold} onChange={(e) => {
                        setEditThreshold(e.target.value)
                    }} />
                </div>
            </div>
            <p className={style.drawer_p}><span style={{
                color: 'red',
                marginRight: '5px'
            }}>*</span>选择能耗设备</p>
            <Transfer
                dataSource={devicels}
                targetKeys={targetKeys}
                selectedKeys={selectedKeys}
                onChange={onChange1}
                onSelectChange={onSelectChange}
                render={render}
                style={{
                    padding: '7px 20px',
                    boxSizing: 'border-box'
                }}
            />
            <div className={style.drawer_bottom}>
                <button className={style.drawer_sure} onClick={edit}>编辑</button>
                <button className={style.drawer_cancel}>取消</button>
            </div>
        </Drawer>
    )
}
