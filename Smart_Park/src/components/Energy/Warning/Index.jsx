import React, { useState } from 'react'
import Top from './Top'
import Footer from './Footer'

export default function Index() {
  //接收搜索参数
  const [sname, setSname] = useState('')
  const [state, setState] = useState('')
  const [date, setDate] = useState('')
  const getQuery = (sname, state, date) => {
    setSname(sname)
    setState(state)
    setDate(date)
  }
  const handleReset = () => {
    setSname("");
    setState("")
    setDate("")
  };
  return (
    <div>
      <Top getQuery={getQuery} onReset={handleReset}/>
      <Footer sname={sname} state={state} date={date} />
    </div>
  )
}
