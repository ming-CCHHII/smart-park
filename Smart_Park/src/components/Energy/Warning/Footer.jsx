import React, { useEffect, useState, useCallback } from 'react'
import style from '../../../assets/css/warn.module.css'
import request from '../../../utils/request'
import dayjs from 'dayjs'
import { Button, Drawer, Input } from 'antd';

const { TextArea } = Input;
export default function Footer({ sname, state, date }) {
    const [energyls, setEnergyls] = useState([])
    const columns = ['预警时间', '预警内容', '描述', '处理状态', '处理情况', '操作']
    const [info, setInfo] = useState({})
    const [value, setValue] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const time = Date.now()
    //darwer
    const [open, setOpen] = useState(false); //抽屉
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };

    const getEnergyWarning = async () => {
        try {
            let { data } = await request.get(`/energy/getenergyWarn/?sname=${sname}&state=${state}&time=${date}`)
            setEnergyls(data)
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsLoading(false);
        }

    }
    const changeWarnState = async () => {
        let res = await request.post(`/energy/changeWarnState?_id=${info._id}&result=${value}&time=${time}`)
        if (res.code === 200) {
            getEnergyWarning()
        }
    }
    useEffect(() => {
        getEnergyWarning()
    }, [sname, state, date])
    return (
        <>
            <div className={style.footer_card}>
                <table className={style.tb}>
                    <thead>
                        <tr style={{
                            backgroundColor: '#f5f6fb'
                        }}>
                            {columns.map((item, index) => {
                                return <td key={index}>{item}</td>
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {isLoading ? (
                            <tr>
                                <td colSpan={6} style={{ textAlign: 'center', color: 'gray' }}>Loading...</td>
                            </tr>
                        ) : (
                            energyls.length > 0 ? (
                                energyls.map((item, index) => (
                                    <tr key={item._id}>
                                        <td>{dayjs(item.time).format('YYYY-MM-DD HH:mm:ss')}</td>
                                        <td>{item.name}</td>
                                        <td>{item.describe}</td>
                                        <td>{item.state === 0 ? '未处理' : '已处理'}</td>
                                        <td>
                                            {item.result === "" ? '-' : item.result}<br />
                                            <span style={{
                                                color: '#999',
                                                fontSize: '12px'
                                            }}>
                                                {!item.resultTime ? "" : dayjs(item.resultTime).format('YYYY-MM-DD HH:mm:ss')}
                                            </span>
                                        </td>
                                        <td>
                                            <button className={style.editbtn} onClick={() => {
                                                showDrawer();
                                                setInfo(item);
                                            }}>前往处理</button>
                                        </td>
                                    </tr>
                                ))
                            ) : (
                                <tr>
                                    <td colSpan={6} style={{ textAlign: 'center', color: 'gray' }}>No data available</td>
                                </tr>
                            )
                        )}
                    </tbody>
                </table>
                <Drawer
                    title="预警处理"
                    placement="right"
                    width={420}
                    onClose={onClose}
                    open={open}
                >
                    <div className={style.drawer_box_warn}>
                        <p><span className={style.gray_span}>预警时间：</span>{dayjs(info.time).format('YYYY-MM-DD HH:mm:ss')}</p>
                        <p><span className={style.gray_span}>预警内容：</span>{info.name}</p>
                        <p><span className={style.gray_span}>描述：</span>{info.describe}</p>
                        <p><span className={style.gray_span}>处理状态：</span>{info.state === 0 ? '未处理' : '已处理'}</p>
                        <p style={{
                            display: 'flex'
                        }}>
                            <span style={{ color: 'red', marginRight: '5px' }}>*</span><span>处理结果：</span>
                            <TextArea
                                value={value}
                                onChange={(e) => setValue(e.target.value)}
                                placeholder="请输入"
                                autoSize={{
                                    minRows: 4,
                                    maxRows: 5,
                                }}
                                style={{
                                    width: '60%'
                                }}
                            />
                        </p>
                        <div className={style.drawer_bottom1}>
                            <Button onClick={onClose} style={{
                                marginRight: '10px'
                            }}>取消</Button>
                            <Button type="primary" onClick={() => {
                                onClose()
                                changeWarnState()
                            }}>
                                确认
                            </Button>
                        </div>
                    </div>
                </Drawer>
            </div>
        </>
    )
}
