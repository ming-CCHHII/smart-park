
import style from '../../assets/css/analysis.module.css'
import React from 'react'

export default function TableCard({title,columns,data,width}) {
  return (
    <div className={style.table_card}>
            <p style={{
                marginBottom: '15px',
                display:title ? 'block':'none'
            }}>{title}</p>
            <table className={style.tb}>
                <thead>
                    <tr style={{
                        backgroundColor: '#f5f6fb'
                    }}>
                        {columns.map((item,index)=>{
                            return <td key={index}>{item}</td>
                        })}
                    </tr>
                </thead>
                <tbody>
                {data.map((row, rowIndex) => (
                        <tr key={rowIndex}>
                            {Object.keys(row).map((key, cellIndex) => (
                                <td key={cellIndex}>{row[key]}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
  )
}

