import { useEffect, useState, useMemo } from 'react'
import style from '../../assets/css/warn.module.css'
import { Select, Cascader, Form } from "antd";
import TableCard from './TableCard';
import request from '../../utils/request'

export default function Monthly() {
  const [energyType, setEnergyType] = useState([])
  const [month, setMonth] = useState("")
  const [type, setType] = useState("")
  const [vmonth, setVmonth] = useState("")
  const [vtype, setVtype] = useState("")
  const [form] = Form.useForm();
  const search = () => {
    setType(vtype)
    setMonth(vmonth)
  }
  const reset = () => {
    form.resetFields()
    setVmonth('')
    setVtype('')
    setType('')
    setMonth('')
  }
  //级联选择器
  const onChange = (value) => {
    setVmonth(value[1]);
  };
  //select选择器
  const handleChange = (value) => {
    setVtype(value)
  };
  const options1 = [
    {
      value: '2024年',
      label: '2024年',
      children: [
        {
          value: '5月',
          label: '5月',
        },
        {
          value: '4月',
          label: '4月',
        },
        {
          value: '3月',
          label: '3月',
        },
        {
          value: '2月',
          label: '2月',
        },
        {
          value: '1月',
          label: '1月',
        }
      ],
    },
    {
      value: '2023年',
      label: '2023年',
      children: [
        {
          value: '12月',
          label: '12月',
        },
        {
          value: '11月',
          label: '11月',
        },
        {
          value: '10月',
          label: '10月',
        },
        {
          value: '9月',
          label: '9月',
        },
        {
          value: '8月',
          label: '8月',
        },
      ],
    },
  ];
  const columns = ['账单', '用量总计', '费用总计', '生成时间']
  const data = [
    {
      orderName: '2024年-5月（用电账单）',
      cost: '12120kw',
      total: 8749.21,
      time: '2024/11/05 14:50:12'
    },
    {
      orderName: '2024年-5月（用水账单）',
      cost: '2200kw',
      total: 679.83,
      time: '2024/11/05 09:01:00'
    },
    {
      orderName: '2024年-4月（用电账单）',
      cost: '14899kw',
      total: 10021.91,
      time: '2024/10/31 13:22:05'
    },
    {
      orderName: '2024年-4月（用水账单）',
      cost: '1903kw',
      total: 578.01,
      time: '2024/10/31 09:41:23'
    }
  ]

  //获取能耗类型
  const getEnergyType = async () => {
    let res = await request.get('/energy/getenergyType')
    if (res.code == 200) {
      setEnergyType(res.data)
    }
  }

  const newData = useMemo(() => {
    return data.filter(item => {
      return item.orderName.includes(month)
    })
  }, [data, month])

  const newData1 = useMemo(() => {
    return [...newData].filter(item => {
      return item.orderName.includes(type)
    })
  }, [newData, type])

  useEffect(() => {
    getEnergyType()
  }, [])
  return (
    <div>
      <div className={style.top_card}>
        <Form form={form} style={{display:'flex',alignItems:'center'}}>
          <Form.Item label="选择月份" name="month">
            <Cascader placeholder="请选择" options={options1} onChange={onChange} style={{
              width: '230px',
              marginRight: '100px'
            }} />
          </Form.Item>
          <Form.Item label="能耗类型" name="type">
            <Select
              placeholder="请选择"
              style={{
                width: '230px',
                marginRight: '5px'
              }}
              onChange={handleChange}
              options={energyType}
              fieldNames={{ label: 'name', value: 'name' }}
            />
          </Form.Item>
          <Form.Item>
          <button className={style.search_btn} onClick={search}>搜索</button>
          <button className={style.reset_btn} onClick={reset}>重置</button>
          </Form.Item>
        </Form>
        
        
      </div>
      <div className={style.footer_card}>
        {/* 表格卡片 */}
        <TableCard columns={columns} data={newData1}></TableCard>
      </div>
    </div>
  )
}
