
import style from '../../../assets/css/analysis.module.css'
import Side from './Side'
import Main from './Main'
import { useState } from 'react'

export default function Index() {
  const [sideid,setSideid] = useState("672d8543b276d6182ee5acdd")
  const getSideid = (id) => {
    setSideid(id)
  }
  return (
    <div className={style.parsel}>
        <Side getSideid={getSideid}/>
        <Main sideid={sideid}/>
    </div>
  )
}
