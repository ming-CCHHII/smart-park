import{ useEffect, useState } from 'react'
import style from '../../../assets/css/analysis.module.css'
import { Drawer, Input, InputNumber, Transfer, message } from "antd";
import request from '../../../utils/request'


export default function Side({getSideid}) {
    const [num, setNum] = useState(0)
    const [spanls, setSpanls] = useState([])
    const [name, setName] = useState('')//分组名称
    const [weight, setWeight] = useState(0)//分组权重
    const [typeid, setTypeid] = useState("")//分组类型
    const [flag, setFlag] = useState(false)//切换穿梭框
    const [energyDevice, setEnergyDevice] = useState([]);//电表设备
    const [waterDevice, setWaterDevice] = useState([]);//水表设备
    const [realTime, setRealTime] = useState("");
    const [groupls, setGroupls] = useState([])//用途分组列表
    const [pid, setPid] = useState("")

    //drawer
    const [open, setOpen] = useState(false);
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };
    //inpNumber
    const onChangeWeight = (value) => {
        setWeight(value)
    };
    //穿梭框1
    //右侧框数据的key集合
    const [targetKeys, setTargetKeys] = useState([]);
    //选中的项
    const [selectedKeys, setSelectedKeys] = useState([]);
    const onChange1 = (nextTargetKeys) => {
        setTargetKeys(nextTargetKeys);
    };
    const onSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
        setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
    };
    const render = (item) => (
        <>
            <p style={{ display: 'flex', alignItems: 'center' }}>
                <img src={item.image} alt={item.title} style={{ width: '17px', height: '17px', marginRight: '10px' }} />
                <span>{item.title}</span>
            </p>

        </>
    );
    //穿梭框2
    //右侧框数据的key集合
    const [targetKeys1, setTargetKeys1] = useState([]);
    //选中的项
    const [selectedKeys1, setSelectedKeys1] = useState([]);
    const onChange2 = (nextTargetKeys) => {
        setTargetKeys1(nextTargetKeys);
    };
    const onSelectChange1 = (sourceSelectedKeys, targetSelectedKeys) => {
        setSelectedKeys1([...sourceSelectedKeys, ...targetSelectedKeys]);
    };
    const render1 = (item) => (
        <>
            <p style={{ display: 'flex', alignItems: 'center' }}>
                <img src={item.image} alt={item.title} style={{ width: '17px', height: '17px', marginRight: '10px' }} />
                <span>{item.title}</span>
            </p>

        </>
    );

    //获取电表设备
    const getEnergyDevice = async () => {
        let { data } = await request.get('/energy/getenergyDevice')
        let arr = []
        data.forEach(item => {
            arr.push({
                key: item._id,
                title: item.name,
                image: item.image
            })
        })
        setEnergyDevice(arr)
    }

    //获取水表设备
    const getWaterDevice = async () => {
        let { data } = await request.get('/energy/getwaterDevice')
        let arr = []
        data.forEach(item => {
            arr.push({
                key: item._id,
                title: item.name,
                image: item.image
            })
        })
        setWaterDevice(arr)
    }

    //获取用途类型
    const getType = async () => {
        let res = await request.get('/energy/getpurposeType')
        if (res.code == 200) {
            setSpanls(res.data)
        }
    }

    const [messageApi, contextHolder] = message.useMessage(); //提示框

    //添加用途分组
    const addGroup = async () => {
        if (pid == '') {
            let obj = {
                name: name,
                weight: weight,
                deviceid: targetKeys,
                waterid: targetKeys1,
                typeid: typeid,
                realTime: realTime,
                recordTime: Date.now(),
            }
            let res = await request.post('/energy/addpurposeGroup', obj)
            if (res.code == 200) {
                messageApi.open({
                    type: 'success',
                    content: '添加成功',
                });
                getType()
                setName('')
                setWeight('')
                setTargetKeys([])
                setTargetKeys1([])
                setTypeid('')
                setRealTime('')
                setOpen(false);
                getGroup()
            }
        } else {
            let obj = {
                name: name,
                weight: weight,
                deviceid: targetKeys,
                waterid: targetKeys1,
                typeid: typeid,
                realTime: realTime,
                recordTime: Date.now(),
                pid: pid
            }
            let res = await request.post('/energy/addpurposeGroup', obj)
            if (res.code == 200) {
                messageApi.open({
                    type: 'success',
                    content: '添加成功',
                });
                getType()
                setName('')
                setWeight('')
                setTargetKeys([])
                setTargetKeys1([])
                setTypeid('')
                setRealTime('')
                setOpen(false);
                setPid('')
                getGroup()
            }
        }

    }

    //获取用途分组
    const getGroup = async () => {
        let res = await request.get('/energy/getpurposeGroup')
        if (res.code == 200) {
            setGroupls(res.result)
            // console.log(res.result)
        }
    }

    const changeShowFlag = async(id)=>{
        let res = await request.post(`/energy/changeshowFlag/?_id=${id}`)
        if(res.code == 200){
            getGroup()
        }
    }

    useEffect(() => {
        getType()
        getEnergyDevice()
        getWaterDevice()
        getGroup()
    }, [])
    return (
        <>
            {contextHolder}
            {/* 侧边栏卡片 */}
            <div className={style.side_card}>
                <button className={style.addbtn} onClick={showDrawer}>添加用途分组</button>
                <p style={{
                    marginTop: '1rem',
                    height: 'auto'
                }}>
                    {spanls.map((item, index) => {
                        return <span key={item._id} className={num === index ? style.spanActive : style.side_span} onClick={() => {
                            setNum(index)
                        }}>{item.name}</span>
                    }
                    )}

                </p>
                <div className={style.side_menu}>
                    {groupls.filter(item => !item.pid).map((item) => {
                        return <div style={{ height: 'auto', width: '100%' }} key={item._id}>
                            <div className={style.side_menu_item} onClick={() => {
                                setPid(item._id)
                                getSideid(item._id)
                            }}>
                                <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E8%83%BD%E8%80%97%E5%88%86%E6%9E%90/u6936.png" alt="" style={{ width: '15%', height: '30px' }} className={style.downOutline} />
                                <p style={{
                                    width: '60%',
                                    textAlign: 'left',
                                    paddingLeft: '10px',
                                    boxSizing: 'border-box'
                                }}>{item.name}</p>
                                <p style={{
                                    width: '25%',
                                    display: !item.showFlag ? 'block' : 'none'
                                }} onClick={()=>{
                                    changeShowFlag(item._id)
                                }} ><img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E8%A7%86%E9%A2%91%E5%9B%9E%E6%94%BE/u1048.svg" alt="" /></p>
                                <p style={{
                                    width: '25%',
                                    display: item.showFlag ? 'block' : 'none'
                                }} onClick={()=>{
                                    changeShowFlag(item._id)
                                }}><img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E8%A7%86%E9%A2%91%E5%9B%9E%E6%94%BE/u1048.svg" alt="" style={{
                                    transform: 'rotate(270deg)'
                                }} /></p>
                            </div>
                            {item.children && item.children.length > 0 && (
                                <div style={{
                                    display: item.showFlag ? 'block' : 'none'
                                }}>
                                    {item.children.map((ele) => (
                                        <div className={style.side_menu_child} key={ele._id} onClick={()=>{
                                            getSideid(ele._id)
                                        }}>
                                            <span></span>
                                            <p style={{ color: 'gray' }}>{ele.name}</p>
                                        </div>
                                    ))}
                                </div>
                            )}
                        </div>
                    })}
                </div>
            </div>
            <Drawer title="添加分组" onClose={onClose} open={open} size="large">
                <div className={style.drawer_box}>
                    <div className={style.drawer_item}>
                        <p style={{
                            marginBottom: '15px'
                        }}><span style={{
                            color: 'red',
                            marginRight: '5px'
                        }}>*</span>分组名称</p>
                        <Input placeholder="请输入" style={{ width: '260px' }} value={name} onChange={e => setName(e.target.value)} />
                    </div>
                    <div className={style.drawer_item}>
                        <p style={{
                            marginBottom: '15px'
                        }}><span style={{
                            color: 'red',
                            marginRight: '5px'
                        }}>*</span>排序权重</p>
                        <InputNumber min={1} max={10} defaultValue={0} onChange={onChangeWeight} style={{ width: '260px' }} />
                    </div>
                    <div className={style.drawer_item}>
                        <p style={{
                            marginBottom: '15px'
                        }}><span style={{
                            color: 'red',
                            marginRight: '5px'
                        }}>*</span>类型选择</p>
                        {spanls.map(item => {
                            return <button className={style.drawer_btn} key={item._id} onClick={() => {
                                setTypeid(item._id)
                                setFlag(!flag)
                            }}>{item.name}</button>
                        })}

                    </div>
                </div>
                <p className={style.drawer_p}>选择设备</p>
                <div style={{
                    width: '100%',
                    height: 'auto',
                    display: flag ? 'block' : 'none'

                }}>
                    <Transfer
                        dataSource={energyDevice}
                        targetKeys={targetKeys}
                        selectedKeys={selectedKeys}
                        onChange={onChange1}
                        onSelectChange={onSelectChange}
                        render={render}
                        style={{
                            padding: '7px 20px',
                            boxSizing: 'border-box'
                        }}
                    />
                </div>
                <div style={{
                    width: '100%',
                    height: 'auto',
                    display: !flag ? 'block' : 'none'
                }}>
                    <Transfer
                        dataSource={waterDevice}
                        targetKeys={targetKeys1}
                        selectedKeys={selectedKeys1}
                        onChange={onChange2}
                        onSelectChange={onSelectChange1}
                        render={render1}
                        style={{
                            padding: '7px 20px',
                            boxSizing: 'border-box'
                        }}
                    />
                </div>

                <div className={style.drawer_bottom}>
                    <button className={style.drawer_sure} onClick={addGroup}>确认</button>
                    <button className={style.drawer_cancel} onClick={onClose}>取消</button>
                </div>
            </Drawer>
        </>
    )
}
