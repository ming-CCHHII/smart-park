import React, { useState, useEffect } from 'react'
import EChartsReact from 'echarts-for-react';

export default function Line() {
    const [option, setOption] = useState({
        tooltip: {},
        xAxis: {
            type: 'category',
            data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月'],
            
        },
        yAxis: {
            type: 'value',
            name:'用电量:（kw）'
           
        },
        series: [
            {
                data: [60, 90, 80, 110, 80, 85, 70],
                type: 'line',
                lineStyle: {
                    color: '#409eff',
                    width: 2
                },
                symbol: 'circle', // 设置数据点的形状为实心圆
                symbolSize: 8, // 设置数据点的大小
                itemStyle: {
                    color: '#409eff' // 设置数据点的颜色
                },
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [
                            { offset: 0, color: 'rgba(64, 158, 255, 0.5)' }, // 0% 处的颜色
                            { offset: 1, color: 'rgba(84, 112, 198, 0)' } // 100% 处的颜色
                        ],
                        global: false // 缺省为 false
                    }
                }
            }
        ]
    });
    return (
        <>
            <EChartsReact option={option} style={{ height: '300px', width: '100%' }} />
        </>
    )
}
