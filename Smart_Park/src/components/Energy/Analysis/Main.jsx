import React, { useEffect, useState ,useCallback} from 'react'
import { DatePicker } from "antd";
import TableCard from '../TableCard';
import style from '../../../assets/css/analysis.module.css'
import request from '../../../utils/request';
import Line from './Line';

const { RangePicker } = DatePicker;
export default function Main({ sideid }) {
    const tbtit = "数据详情";
    const columns = ['记录时间', '实时读数', '与上次记录增加量']
    const data = [
        {
            recordTime: '2023-03-01 10:00:00',
            realTime: '100',
            addNum: '10',
        },
        {
            recordTime: '2023-03-01 11:00:00',
            realTime: '110',
            addNum: '5',
        }
    ]

    const [info, setInfo] = useState({})

    const getInfo = useCallback(async()=>{
        let res = await request.post(`/energy/getInfo/?_id=${sideid}`)
        if (res.code == 200) {
            setInfo(res.data)
        }
    },[sideid])

    useEffect(() => {
        getInfo()
    },[sideid])

    return (
        <>
            <div className={style.main_card}>
                {/* 顶部卡片 */}
                <div className={style.top_card}>
                    <div style={{ display: 'flex', width: "40%", alignItems: 'center' }}>
                        <p style={{ width: '15%' }}>
                            <img src='https://cdn7.axureshop.com/demo2024/2291721/images/%E8%83%BD%E8%80%97%E5%88%86%E6%9E%90/u6624.svg' style={{ width: '45px', height: '45px' }} />
                        </p>
                        <div style={{ width: '65%' }}>
                            <p>{info.name ? info.name : 'xxx'}</p>
                            <p style={{ color: '#429eff', marginTop: '0.5rem', fontSize: '0.8rem' }}>实时读数：{info.realTime ? info.realTime : 1239.12}KW</p>
                        </div>
                        <button className={style.editbtn} >编辑分组</button>
                    </div>
                    <div className={style.hr}></div>
                    <div style={{ display: "flex", width: "35%", alignItems: 'center' }}>
                        <p style={{ fontSize: "14px", marginLeft: '17px' }}>自动分析时段 : </p>

                        <RangePicker placeholder={['开始时间', '结束时间']} style={{
                            width: '65%',
                            padding: '10px 10px',
                            boxSizing: ' border-box',
                            marginRight: '5px',
                            marginLeft: '15px'
                        }} />

                    </div>
                    <div style={{ display: "flex", justifyContent: "space-around", width: "25%", alignItems: 'center' }}>
                        <button className={style.defaultbtn}>近7天</button>
                        <button className={style.defaultbtn}>近30天</button>
                        <button className={style.defaultbtn}>近半年</button>
                        <button className={style.defaultbtn}>今年至今</button>
                    </div>
                </div>
                {/* numData */}
                <div className={style.numData_card}>
                    <div>
                        <p>累积能耗（kw）</p>
                        <p >9241</p>
                    </div>
                    <div>
                        <p>日均值（kw）</p>
                        <p >123</p>
                    </div>
                    <div>
                        <p>极大值（kw）</p>
                        <p >190</p>
                    </div>
                    <div>
                        <p>极大值（kw）</p>
                        <p >2</p>
                    </div>
                    <div>
                        <p>同比升降</p>
                        <p >+12%</p>
                    </div>
                    <div>
                        <p>同比升降</p>
                        <p >-12%</p>
                    </div>
                    <div>
                        <p>全年占比</p>
                        <p >30%</p>
                    </div>
                </div>
                {/* echarts */}
                <div className={style.echarts_card}>
                    <div style={{
                        width: '100%',
                        height: 'auto',
                        display: 'flex',
                        justifyContent: 'space-between',
                    }}>
                        <p>趋势分析</p>
                        <div>
                            <button>按月</button>
                            <button>按周</button>
                            <button>按日</button>
                        </div>
                    </div>
                    <br />
                    <Line></Line>
                </div>
                {/* 表格卡片 */}
                <TableCard title={tbtit} columns={columns} data={data}></TableCard>
            </div>
        </>
    )
}
