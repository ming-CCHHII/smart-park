import React, { useState, useEffect } from 'react'
import style from '../../assets/css/warn.module.css'
import request from '../../utils/request'
import { Input, Select, message } from "antd";
import dayjs from 'dayjs';

export default function Price() {
  const columns = ['能耗类型', '阶梯档位', '阶梯档位', '阶梯阈值', '阈值', '条件', '更新时间']
  const [list, setlist] = useState([])
  const [pid, setPid] = useState("")

  const numberToChinese = (num) => {
    const chineseNums = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
    const units = ['', '十', '百', '千'];

    let result = '';
    let unitIndex = 0;

    while (num > 0) {
      let digit = num % 10;
      if (digit !== 0 || unitIndex === 0) {
        result = chineseNums[digit] + units[unitIndex] + result;
      }
      num = Math.floor(num / 10);
      unitIndex++;
    }

    // 特殊情况处理
    if (result.startsWith('一十')) {
      result = result.replace('一十', '十');
    }

    return result || '零';
  }

  const getlength = () => {
    let ress = list.filter(item => {
      return item._id == pid
    })
    console.log(ress)
    return ress[0].children ? (ress[0].children.length - 1)+3: 2
  }

  const addChild = async (typeid) => {
    if (pid && typeid) {
      let obj = {
        typeid: typeid,
        stairs: `第${numberToChinese(getlength())}阶梯`,
        stairsPrice: undefined,
        stairsThreshold: undefined,
        stairsCondition: 0,
        up_time: Date.now(),
        pid: pid
      }
      let res = await request.post('/energy/addenergyPrice', obj)
      if (res.code == 200) {
        message.success(res.msg)
        getEnergyPrice()
        setPid("")
      }
    }

  }

  //获取能源价格
  const getEnergyPrice = async () => {
    let res = await request.get('/energy/getenergyPrice')
    if (res.code == 200) {
      setlist(res.result)
    }
  }

  //select
  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  //删除阶梯
  const delStairs = async (id) => {
    let res = await request.delete(`/energy/delstairs/?_id=${id}`)
    if (res.code == 200) {
      message.success("删除成功")
      getEnergyPrice()
    }
  }
  useEffect(() => {
    getEnergyPrice()
  }, [pid])

  return (

    <div className={style.price_card}>
      <table className={style.tb}>
        <thead>
          <tr style={{
            backgroundColor: '#f5f6fb'
          }}>
            {columns.map((item, index) => {
              return <td key={index}>{item}</td>
            })}
          </tr>
        </thead>
        <tbody>
          {list.map((item) => {
            return (
              <React.Fragment key={item._id}>
                <tr key={item._id}>
                  <td>{item.typeid.name}</td>
                  <td>{item.stairs}</td>
                  <td>
                    <Input
                      placeholder="请输入"
                      value={item.stairsPrice}
                      style={{ width: '70px' }}
                    />&nbsp; 元 / kw
                  </td>
                  <td>
                    <Input
                      placeholder="请输入"
                      value={item.stairsThreshold}
                      style={{ width: '70px' }}
                    /> &nbsp;kw
                  </td>
                  <td>
                    <Select
                      defaultValue={item.stairsCondition}
                      style={{ width: 100 }}
                      onChange={handleChange}
                      options={[
                        {
                          value: 0,
                          label: "小于阈值"
                        },
                        {
                          value: 1,
                          label: "大于阈值"
                        }
                      ]}
                    />
                  </td>
                  <td>{dayjs(item.up_time).format('YYYY-MM-DD HH:mm:ss')}</td>
                  <td>
                    <button
                      className={style.addstairs}
                      onClick={() => {
                        setPid(item._id);
                        addChild(item.typeid._id,item._id);
                      }}
                    >
                      增加阶梯
                    </button>
                    <button className={style.upstairs}>更新</button>
                    <button
                      className={style.delstairs}
                      style={{ display: item.children && item.children.length > 0 ? 'block' : 'none' }}
                    >
                      删除阶梯
                    </button>
                  </td>
                </tr>
                {item.children && item.children.length > 0 && (
                  <>
                    {item.children.map((ele) => (
                      <tr key={ele._id}>
                        <td></td>
                        <td>{ele.stairs}</td>
                        <td>
                          <Input
                            placeholder="请输入"
                            value={ele.stairsPrice}
                            style={{ width: '70px' }}
                          />&nbsp; 元 / kw
                        </td>
                        <td>
                          <Input
                            placeholder="请输入"
                            value={ele.stairsThreshold}
                            style={{ width: '70px' }}
                          />&nbsp;kw
                        </td>
                        <td>
                          <Select
                            defaultValue={ele.stairsCondition}
                            style={{ width: 100 }}
                            onChange={handleChange}
                            options={[
                              {
                                value: 0,
                                label: "小于阈值"
                              },
                              {
                                value: 1,
                                label: "大于阈值"
                              }
                            ]}
                          />
                        </td>
                        <td>{dayjs(ele.up_time).format('YYYY-MM-DD HH:mm:ss')}</td>
                        <td>
                          <button
                            className={style.delstairs}
                            style={{ display: ele.pid ? 'block' : 'none' }}
                            onClick={() => delStairs(ele._id)}
                          >
                            删除阶梯
                          </button>
                        </td>
                      </tr>
                    ))}
                  </>
                )}
              </React.Fragment>
            );
          })}
        </tbody>

      </table>
    </div>
  )
}
