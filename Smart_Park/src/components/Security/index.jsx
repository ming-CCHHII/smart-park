import React, { useEffect, useState} from 'react'
import { Outlet } from 'react-router-dom'

export default function index() {
  return (
    <Outlet></Outlet>
  )
}
