import React, { useEffect, useRef } from 'react'
import style from '../../assets/SecurityCss/video.module.css'
import img from '../../assets/image/监控.png'
import EZUIKit from 'ezuikit-js'
import { Button } from "antd";

export default function Video() {
  const playerRef = useRef(null); // 用于存储播放器实例
  const videoContainerRef = useRef(null);

  // useEffect(() => {
  //   if (window.player) {
  //     window.player.play(); // 播放
  //     return;
  //   }

  //   // 初始化加载播放
  //   playerRef.current = new EZUIKit.EZUIKitPlayer({
  //     id: "video-container",
  //     accessToken: "at.8ut5mb1m1nkodnend0vgs0rebj0m0e1m-8s3c0xhvu5-12g722o-ody66wpcj",
  //     url: "ezopen://open.ys7.com/BD5336750/1.hd.live",
  //     template: "pcLive",
  //     plugin: ["talk"],
  //     handleFirstFrameDisplay: () => {
  //       if (window.player.jSPlugin) {
  //         console.log(
  //           "-------------------res",
  //           window.player.jSPlugin.player.getFrameInfo()
  //         );
  //       }
  //     },
  //     handleError: (error) => {
  //       console.log("handleError", error);
  //     },
  //     // 结束录像方法
  //     stopSaveCallBack: (res) => {
  //       console.log("结束了录像", res);
  //     }
  //   });

  //   window.player = playerRef.current;


  //   // Cleanup function to handle component unmount
  //   return () => {
  //     window.player = null
  //   };
    
  // }, [])

  useEffect(() => {
    if (window.player) {
      return;
    }

    playerRef.current = new EZUIKit.EZUIKitPlayer({
      id: "video-container",
      accessToken: "at.8ut5mb1m1nkodnend0vgs0rebj0m0e1m-8s3c0xhvu5-12g722o-ody66wpcj",
      url: "ezopen://open.ys7.com/BD5336750/1.hd.live",
      template: "pcLive",
      plugin: ["talk"],
      // handleFirstFrameDisplay: (res) => {
      //   if (window.player.jSPlugin) {
      //     console.log(
      //       "-------------------res",
      //       window.player.jSPlugin.player.getFrameInfo()
      //     );
      //   }
      // },
      // handleError: (error) => {
      //   console.log("handleError", error);
      // },
    });

    window.player = playerRef.current;

    // Cleanup function to handle component unmount
    return () => {
      window.player = null;
    };
  }, []);


  const play = () => {
    playerRef.current.play().then((data) => {
      console.log("promise 获取 数据", data);
    });
  };

  const stop = () => {
    playerRef.current.stop().then((data) => {
      console.log("promise 获取 数据", data);
    });
  };



  
  return (
    <div className={style.box}>

        {/* 左侧分组 */}
        <div className={style.left}>
          <div className={style.left_top}>
            <p>添加分组</p>
          </div>
          {/* 分类 */}
          <div className={style.left_box}>
            <div className={style.left_title}>
              <img src={img} alt="" />
              <p>A区-1楼大厅</p>
            </div>
            <div className={style.left_title}>
              <img src={img} alt="" />
              <p>A区-办公区域</p>
            </div>
            <div className={style.left_title}>
              <img src={img} alt="" />
              <p>B区办公区域</p>
            </div>
            <div className={style.left_title}>
              <img src={img} alt="" />
              <p>园区消防监控</p>
            </div>
          </div>
        </div>

        {/* 监控视频 */}
        <div className={style.middle}>
          <div className={style.middle_header}>
            <p>1楼大厅</p>
            <p>
              <Button type="primary" onClick={stop}>全屏编辑</Button>
              &nbsp;
              <Button type="primary" onClick={play}>查看</Button>
            </p>
          </div>
        <div className={style.middle_top} ref={videoContainerRef} id="video-container"></div>
          <div className={style.middle_bottom}>
          <img src="https://cdn7.axureshop.com/demo2024/2291721/images/%E8%A7%86%E9%A2%91%E7%9B%91%E6%8E%A7/u320.svg" alt="" />
          </div>
        </div>

        {/* 右侧预警记录 */}
        <div className={style.right}>
          
        </div>


    </div>
  )
}
