import React from 'react'
import Warning from './warning'
import Warning_header from './warning_header'
import style from '../../assets/SecurityCss/earlywarning.module.css'

export default function warning() {
  return (
    <div className={style.box}>
      <Warning_header/>
      <Warning/>
    </div>
  )
}
