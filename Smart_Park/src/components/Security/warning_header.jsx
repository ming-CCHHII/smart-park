import React, { useState } from 'react'
import style from '../../assets/SecurityCss/warning_header.module.css'
import { Input, Select, DatePicker } from "antd";

export default function warning_header() {
    const [date, setDate] = useState("")
    const [sname, setSname] = useState("")
    const [state, setState] = useState("")
    const options = [
        {
            value: 1,
            label: "已处理"
        },
        {
            value: 0,
            label: "未处理"
        }
    ]
    //select选择器
    const handleChangeState = (value) => {
        setState(value)
    };
    //日期选择器
    const handleChangeDate = (date, dateString) => {
        setDate(dateString);
    };


  return (
    <div>
        <div className={style.header}>
            <span>预警类型：</span>
            <Input placeholder="请输入" style={{
                width: '230px',
                marginRight: '100px'
            }} value={sname} onChange={(e) => setSname(e.target.value)} />
            <span>状态：</span>
            <Select
                placeholder="请选择"
                style={{
                    width: '230px',
                    marginRight: '100px'
                }}
                onChange={handleChangeState}
                options={options}
            />
            <span>选择日期：</span>
            <DatePicker placeholder="请选择日期" onChange={handleChangeDate} style={{
                width: '230px'
            }} />
            <button className={style.search_btn} onClick={() => { getQuery(sname, state, date) }}>搜索</button>
            <button className={style.reset_btn} onClick={() => {
                setSname('')
                setState('')
                setDate('')
                onReset()
            }}>重置</button>
        </div>
    </div>
  )
}
