import React, { useRef, useEffect} from 'react'
import style from '../../assets/SecurityCss/playback.module.css'
import img from '../../assets/image/监控.png'
import axios from 'axios'
import { DatePicker } from 'antd';
const { RangePicker } = DatePicker;

export default function playback() {
  const playerRef = useRef(null); // 用于存储播放器实例
  const videoContainerRef = useRef(null);
  // 选择回访时间段
  const dataTimes = async (dates, dateStrings) => {
    console.log('From: ', dateStrings[0], ', to: ', dateStrings[1]);
    // 将时间字符串转换为 Date 对象
    const startDate = new Date(dateStrings[0].replace(' ', 'T'));
    const endDate = new Date(dateStrings[1].replace(' ', 'T'));

    // 获取时间戳（毫秒）
    const startTimestamp = startDate.getTime();
    const endTimestamp = endDate.getTime();

    // 调用 getFile 函数获取文件录像
    getFile(startTimestamp, endTimestamp);
  }

  // 根据时间获取文件录像
  const getFile = async (startTime, endTime) => {
    console.log(startTime, endTime);
    // 新token   at.0eye9uui67c8q7cv12qq8256316tjvve-1e8mgxwe0q-0iu2n5g-ia6yrnvsz
    // 旧token   at.c18y5hm2a5egu5kv7xdlax2204m6kz6i-1ovg3xb3ex-10p8lmy-nsmjqhsdl
    let res = await axios.post('https://open.ys7.com/api/lapp/video/by/time', {
      accessToken: 'at.0eye9uui67c8q7cv12qq8256316tjvve-1e8mgxwe0q-0iu2n5g-ia6yrnvsz',
      deviceSerial: 'BD5336750',
      startTime: startTime,
      endTime: endTime
    }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    console.log(res)
  }


  useEffect(() => {
    if (window.player) {
      window.player.play(); // 播放
      return;
    }

    // 初始化加载播放
    playerRef.current = new EZUIKit.EZUIKitPlayer({
      id: "video-container",
      accessToken: "at.8ut5mb1m1nkodnend0vgs0rebj0m0e1m-8s3c0xhvu5-12g722o-ody66wpcj",
      url: "ezopen://open.ys7.com/BD5336750/1.cloud.rec?begin=20241112110000&end=20241112125959",
      template: "pcLive",
      plugin: ["talk"],
      width: '600',
      height: '400',
      handleFirstFrameDisplay: (res) => {
        if (window.player.jSPlugin) {
          console.log(
            "-------------------res",
            window.player.jSPlugin.player.getFrameInfo()
          );
        }
      },
      handleError: (error) => {
        console.log("handleError", error);
      },
    });

    window.player = playerRef.current;

    // Cleanup function to handle component unmount
    return () => {
      window.player = null
    };

  }, [playerRef])

  

  return (
    <div className={style.box}>


      {/* 左侧分组 */}
      <div className={style.left}>
        <div className={style.left_top}>
          <input type="text" placeholder='输入设备名称' />
        </div>
        {/* 分类 */}
        <div className={style.left_box}>
          <div className={style.left_title}>
            <img src={img} alt="" />
            <p>A区-1楼大厅</p>
          </div>
          <div className={style.left_title}>
            <img src={img} alt="" />
            <p>A区-办公区域</p>
          </div>
          <div className={style.left_title}>
            <img src={img} alt="" />
            <p>B区办公区域</p>
          </div>
          <div className={style.left_title}>
            <img src={img} alt="" />
            <p>园区消防监控</p>
          </div>
        </div>
      </div>


      <div className={style.middle}>
        <div className={style.middle_header}>
          选择时间段：<RangePicker showTime onChange={dataTimes} />
        </div>
        <div id='video-container'></div>
      </div>
      <div className={style.right}></div>
    </div>
  )
}
