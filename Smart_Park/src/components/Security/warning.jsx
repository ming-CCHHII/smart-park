import React, { useState, useEffect } from 'react'
import style from '../../assets/SecurityCss/warnings.module.css'
import dayjs from 'dayjs'
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3001'
export default function warning() {

    const [energyls, setEnergyls] = useState([])
    const columns = ['预警时间', '级别', '预警类型', '描述', '设备名称', '所在区域', '处理状态', '处理情况', '操作']
    const [info, setInfo] = useState({})
    const [value, setValue] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const time = Date.now()
    //darwer
    const [open, setOpen] = useState(false); //抽屉
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };

    const getEnergyWarning = async () => {
        try {
            let { data } = await axios.get(`/security/getSecurity`)
            setEnergyls(data.data)
            console.log(data.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsLoading(false);
        }

    }
    useEffect(() => {
        getEnergyWarning()
    }, [])

  return (
    <div>
        <div className={style.boxs}>
            <table className={style.tb}>
                <thead>
                    <tr style={{
                        backgroundColor: '#f5f6fb'
                    }}>
                        {columns.map((item, index) => {
                            return <td key={index}>{item}</td>
                        })}
                    </tr>
                </thead>
                <tbody>
                    {isLoading ? (
                        <tr>
                            <td colSpan={6} style={{ textAlign: 'center', color: 'gray' }}>Loading...</td>
                        </tr>
                    ) : (
                        energyls.length > 0 ? (
                            energyls.map((item, index) => (
                                <tr key={item._id}>
                                    <td>{dayjs(item.createTime).format('YYYY-MM-DD HH:mm:ss')}</td>
                                    <td>{item.securityLevelId.level}</td>
                                    <td>{item.securityLevelId.cate}</td>
                                    <td>{item.securityLevelId.desc}</td>
                                    <td>{item.securityId.name}</td>
                                    <td>{item.areaId.name}</td>
                                    <td>{item.state === false ? '未处理' : '已处理'}</td>
                                    <td>{item.result}</td>
                                    <td>
                                        <button className={style.editbtn} onClick={() => { }}>{ item.state === false ? '前往处理' : '查看详情'}</button>
                                    </td>
                                </tr>
                            ))
                        ) : (
                            <tr>
                                <td colSpan={6} style={{ textAlign: 'center', color: 'gray' }}>No data available</td>
                            </tr>
                        )
                    )}
                </tbody>
            </table>
        </div>
    </div>
  )
}
