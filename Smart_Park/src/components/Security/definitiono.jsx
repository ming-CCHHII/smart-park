import React from 'react'
import DefineHeader from './define_header'
import Define from './define'
import style from '../../assets/SecurityCss/define.module.css'

export default function definitiono() {
  return (
    <div className={style.box}>
      <DefineHeader></DefineHeader>
      <Define></Define>
    </div>
  )
}
