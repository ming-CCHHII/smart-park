import React, { useState, useEffect } from 'react'
import style from '../../assets/SecurityCss/defines.module.css'
import dayjs from 'dayjs'
import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:3001'

export default function define() {

    const [energyls, setEnergyls] = useState([])
    const columns = ['编号', '预警类型', '描述', '级别', '创建时间', '状态', '操作']
    const [info, setInfo] = useState({})
    const [value, setValue] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const time = Date.now()
    //darwer
    const [open, setOpen] = useState(false); //抽屉
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };

    const getEnergyWarning = async () => {
        try {
            let { data } = await axios.get(`/security/getSecuritys`)
            setEnergyls(data.data)
            console.log(data.data);
            
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setIsLoading(false);
        }

    }
    useEffect(() => {
        getEnergyWarning()
    }, [])

  return (
    <div>
        <div className={style.boxs}>
              <table className={style.tb}>
                  <thead>
                      <tr style={{
                          backgroundColor: '#f5f6fb'
                      }}>
                          {columns.map((item, index) => {
                              return <td key={index}>{item}</td>
                          })}
                      </tr>
                  </thead>
                  <tbody>
                      {isLoading ? (
                          <tr>
                              <td colSpan={6} style={{ textAlign: 'center', color: 'gray' }}>Loading...</td>
                          </tr>
                      ) : (
                          energyls.length > 0 ? (
                              energyls.map((item, index) => (
                                  <tr key={item._id}>
                                      <td>{item.name}</td>
                                      <td>{item.cate}</td>
                                      <td>{item.desc}</td>
                                      <td>{item.level}</td>
                                      <td>{dayjs(item.createTime).format('YYYY-MM-DD HH:mm:ss')}</td>
                                      <td>{item.state === false ? '关闭' : '开启'}</td>
                                      <td>
                                          <button className={style.editbtn} onClick={() => {}}>前往处理</button>
                                      </td>
                                  </tr>
                              ))
                          ) : (
                              <tr>
                                  <td colSpan={6} style={{ textAlign: 'center', color: 'gray' }}>No data available</td>
                              </tr>
                          )
                      )}
                  </tbody>
              </table>
        </div>
    </div>
  )
}
