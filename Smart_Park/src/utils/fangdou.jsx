import { useEffect, useState, useRef } from 'react';
export default function fangdou(value, delay) {
    const [debouncedValue, setDebouncedValue] = useState(value);
  const handler = useRef();
 
  useEffect(() => {
    // 清除之前的定时器
    if (handler.current) {
      clearTimeout(handler.current);
    }
 
    // 设置新的定时器
    handler.current = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);
 
    // 清理定时器
    return () => {
      if (handler.current) {
        clearTimeout(handler.current);
      }
    };
  }, [value, delay]); // 依赖value和delay，当它们变化时重新设置定时器
 

  return (
    <div>
      {debouncedValue}



    </div>
  )
}
